﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BattleshipAPI.Model
{
    public class ShipModel
    {
        public int intShipId;
        public int intShipSize;
        public string strName;
        public List<string> strShipPos;
    }
}