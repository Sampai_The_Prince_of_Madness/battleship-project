﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Windows.Forms;
using BattleshipAPI.Model;

namespace BattleshipAPI.Controllers
{
    public class GameController : ApiController
    {
        /*------------*/
        /*Event Quiers*/
        /*------------*/

        //Persist Security Info=False;User ID=*****;Password=*****;Initial Catalog=AdventureWorks;Server=MySqlServer
        //user id=Tmooo;" +"password=Battleship01;server=tmooo.database.windows.net;" + "Trusted_Connection=yes;" + "database=database; " + "connection timeout=30

        SqlConnection myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");

        SqlCommand sqlCommand = new SqlCommand();

        SqlDataReader myReader = null;

        public GameController()
        {
            //OpenConnection();
        }

        [HttpGet]
        //This is a test for the connection to the server and grabbing
        public List<string> GetPlayerNameTest()
        {
            List<string> testList = new List<string>();
            myReader = null;

            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 1; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("SELECT PlayerName FROM Player", myConnection);

                //myReader.Close();

                myReader = sqlCommand.ExecuteReader();

                while (myReader.Read())
                {
                    testList.Add(myReader[0].ToString());
                }

                //myReader.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 2; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 3; " + e.ToString());
            }

            return testList;
        }

        [HttpGet]
        /*Trying to check to see if the turn packages have been uploaded*/
        public string GetCoordinates(int intPlayerID)
        {
            string strReturn = "";
            myReader = null;

            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 4; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("SELECT Coordinate FROM Events WHERE Player = '" + intPlayerID + "'", myConnection);

                myReader = sqlCommand.ExecuteReader();

                while (myReader.Read())
                {
                    strReturn = myReader[0].ToString();
                }

                myReader.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 5; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 6; " + e.ToString());
            }

            return strReturn;
        }

        [HttpGet]
        /*Trying to check to see if the game has been won*/
        public bool GetGameStatisByPlayerID(int intPlayerID)
        {
            bool boolStatis = false;
            myReader = null;

            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 7; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("SELECT GameResult FROM Events WHERE Player = (" + intPlayerID + ")", myConnection);

                //myReader.Close();

                myReader = sqlCommand.ExecuteReader();

                while (myReader.Read())
                {
                    if (myReader[0].ToString() != null || myReader[0].ToString() != "")
                    {
                        boolStatis = (Convert.ToBoolean(myReader[0].ToString()));
                    }

                    else
                    {
                        boolStatis = false;
                    }
                }

                myReader.Close();
            }

            catch(Exception e)
            {
                MessageBox.Show("ERROR: 8; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 9; " + e.ToString());
            }

            return boolStatis;
        }

        //[HttpGet]
        //public int GetPlayerIDByGame(int intNumOfPlayer)
        //{
        //    int intPlayerID = 0;

        //    myReader = null;

        //    try
        //    {
        //        sqlCommand = new SqlCommand("SELECT Player FROM Events WHERE Player = '" + intNumOfPlayer + "'", myConnection);

        //        //myReader.Close();

        //        myReader = sqlCommand.ExecuteReader();

        //        while (myReader.Read())
        //        {
        //            if (myReader[0].ToString() != "")
        //            {
        //                intPlayerID = Int32.Parse(myReader[0].ToString());
        //            }
        //        }

        //        myReader.Close();
        //    }

        //    catch (Exception e)
        //    {
        //        //CloseConnection();
        //        MessageBox.Show(e.ToString());
        //    }

        //    return intPlayerID;
        //}

        [HttpPost]
        public void PostPlayerIDIntoGame(int intPlayerID)
        {
            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 10; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("INSERT INTO Events(Player) VALUES (" + intPlayerID + ")", myConnection);

                sqlCommand.ExecuteReader();
            }
            catch (Exception e)
            {
                MessageBox.Show("ERROR: 11; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 12; " + e.ToString());
            }
        }

        [HttpDelete]
        /*Trying to delete turn packages*/
        public void DeleteCoordinates(int intPlayerID)
        {
            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 13; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("DELETE FROM Events(Coordinate) WHERE Player = " + intPlayerID, myConnection);

                sqlCommand.ExecuteReader();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 14; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 15; " + e.ToString());
            }
        }

        [HttpDelete]
        public void DeleteEventGameByPlayer(int intPlayer)
        {
            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 16; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("DELETE FROM Events WHERE Player = (" + intPlayer + ")", myConnection);

                sqlCommand.ExecuteReader();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 17; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 18; " + e.ToString());
            }
        }

        [HttpPut]
        public void RemoveChallenge(int intPlayer)
        {
            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 19; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("UPDATE Player SET InGame = 'FALSE' WHERE PlayerID = "+ intPlayer + "; UPDATE Player SET Online = 'FALSE' WHERE PlayerID = " + intPlayer + "; UPDATE Player SET ChallengedPlayer = '' WHERE PlayerID = " + intPlayer + ";", myConnection);

                sqlCommand.ExecuteReader();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 20; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 21; " + e.ToString());
            }
        }

        [HttpPut]
        /*Trying to increment Round/Turn*/
        public void PutTurnIncrement()
        {
            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 22; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("UPDATE Events SET Turn = Turn + 1", myConnection);

                sqlCommand.ExecuteReader();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 23; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 24; " + e.ToString());
            }
        }

        [HttpPut]
        /*Trying to update GameWon*/
        public void PutGameStatis(bool boolStatis, string strPlayer)
        {
            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 25; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("UPDATE Events SET GameResult = '" + boolStatis + "' WHERE Player = (" + strPlayer + ")", myConnection);

                sqlCommand.ExecuteReader();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 26; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 27; " + e.ToString());
            }
        }

        [HttpPut]
        /*Trying to insert turn packages*/
        public void PutCoordinates(string strPlayer, string strCoordinates)
        {
            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 28; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("UPDATE Events SET Coordinate = ('" + strCoordinates + "') WHERE Player = (" + strPlayer + ")", myConnection);

                sqlCommand.ExecuteReader();
            }
            catch (Exception e)
            {
                MessageBox.Show("ERROR: 29; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 30; " + e.ToString());
            }
        }

        /*-------------*/
        /*Player Quiers*/
        /*-------------*/

        [HttpGet]
        /*Trying to grab player name by ID*/
        public string GetPlayerByID(int intIDFinder)
        {
            string strName = "";
            myReader = null;

            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 31; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("SELECT PlayerName FROM Player WHERE PlayerID = (" + intIDFinder + ")", myConnection);

                //myReader.Close();

                myReader = sqlCommand.ExecuteReader();

                while (myReader.Read())
                {
                    strName = myReader[0].ToString();
                }

                myReader.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 32; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 33; " + e.ToString());
            }

            return strName;
        }

        [HttpGet]
        /*Trying to grab different ship IDs for a player*/
        public List<int> GetPlayerShipIDs(int intIDFinder)
        {
            List<int> intList = new List<int>();
            myReader = null;

            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 34; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("SELECT Ship1, Ship2, Ship3, Ship4, Ship5 FROM Player WHERE PlayerID = " + intIDFinder, myConnection);

                //myReader.Close();

                myReader = sqlCommand.ExecuteReader();

                while (myReader.Read())
                {
                    intList.Add(Int32.Parse(myReader[0].ToString()));
                }

                myReader.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 35; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 36; " + e.ToString());
            }

            return intList;
        }

        [HttpGet]
        /*Trying to check if a player is online*/
        public bool GetPlayerStatusByID(int intIDFinder)
        {
            bool boolConnected = false;
            myReader = null;

            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 37; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("SELECT Online FROM Player WHERE PlayerID = (" + intIDFinder + ")", myConnection);

                //myReader.Close();

                myReader = sqlCommand.ExecuteReader();

                while (myReader.Read())
                {
                    boolConnected = Convert.ToBoolean(myReader[0]);
                }

                myReader.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 38; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 39; " + e.ToString());
            }

            return boolConnected;
        }

        [HttpGet]
        //Trying to get the latest player added to the database
        public int GetPlayerIDByDate(string strName)
        {
            int intID = 0;
            myReader = null;

            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 40; " + e.ToString());
            }

            try
            {
                //SELECT Max(PlayerID) FROM Player WHERE PlayerName = 'Steve'
                sqlCommand = new SqlCommand("SELECT Max(PlayerID) FROM Player WHERE PlayerName = '" + strName + "'", myConnection);

                //myReader.Close();

                myReader = sqlCommand.ExecuteReader();

                while (myReader.Read())
                {
                    strName = myReader[0].ToString();
                    intID = Int32.Parse(strName);
                }

                myReader.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 41; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 42; " + e.ToString());
            }

            return intID;
        }

        [HttpGet]
        public int GetPlayerIDByChallenge(int intPlayerID)
        {
            int intID = 0;

            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 44; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("SELECT ChallengedPlayer FROM Player WHERE PlayerID = (" + intPlayerID + ")", myConnection);

                //myReader.Close();

                myReader = sqlCommand.ExecuteReader();

                while (myReader.Read())
                {
                    intID = Int32.Parse(myReader[0].ToString());
                }

                myReader.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 45; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 46; " + e.ToString());
            }

            return intID;
        }

        [HttpGet]
        //Getting all the ID's of all offline players
        public List<int> GetPlayerIDByOffline()
        {
            List<int> intList = new List<int>();
            myReader = null;

            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 47; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("SELECT PlayerID FROM Player WHERE Online = 'FALSE'", myConnection);

                //myReader.Close();

                myReader = sqlCommand.ExecuteReader();

                while (myReader.Read())
                {
                    intList.Add(Int32.Parse(myReader[0].ToString()));
                }

                myReader.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 48; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 49; " + e.ToString());
            }

            return intList;
        }

        [HttpGet]
        //Getting all the ID's of all online players
        public List<int> GetPlayerIDByOnline()
        {
            List<int> intList = new List<int>();
            myReader = null;

            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 50; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("SELECT PlayerID FROM Player WHERE Online = 'TRUE'", myConnection);

                //myReader.Close();

                myReader = sqlCommand.ExecuteReader();

                while (myReader.Read())
                { 
                    if(myReader[0].ToString() != "")
                    {
                        //MessageBox.Show(myReader[0].ToString());
                        intList.Add(Int32.Parse(myReader[0].ToString()));
                    }      
                }

                myReader.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 51; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 52; " + e.ToString());
            }

            return intList;
        }

        [HttpGet]
        public bool GetInGameStatus(int intPlayer)
        { 
            bool boolConnected = false;
            myReader = null;

            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 53; " + e.ToString());
            }

            try
            {
                //OpenConnection();
                //myConnection.Open();

                sqlCommand = new SqlCommand("SELECT InGame FROM Player WHERE PlayerID = (" + intPlayer + ")", myConnection);

                //myReader.Close();

                myReader = sqlCommand.ExecuteReader();

                while (myReader.Read())
                {
                    boolConnected = Convert.ToBoolean(myReader[0]);
                }

                myReader.Close();

                //CloseConnection();
                //myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 54; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 55; " + e.ToString());
            }

            return boolConnected;
        }

        [HttpPost]
        /*Trying to insert a new player*/
        public void PostPlayer(string strPlayerName)
        {
            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 57; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("INSERT INTO Player(PlayerName) VALUES ('" + strPlayerName + "')", myConnection);

                sqlCommand.ExecuteReader();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 58; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 59; " + e.ToString());
            }
        }

        [HttpPost]
        public void PostPlayerIDinPlayer(int intPlayerID)
        {
            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 60; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("INSERT INTO Events(Player) VALUES ('" + intPlayerID + "');", myConnection);

                sqlCommand.ExecuteReader();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 61; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 62; " + e.ToString());
            }
        }

        [HttpPut]
        public void PutPlayerStatusOnline(int intPlayerID, bool boolStatus)
        {
            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 63; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("UPDATE Player SET Online = '" + boolStatus + "' WHERE PlayerID = " + intPlayerID, myConnection);

                sqlCommand.ExecuteReader();   
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 64; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 65; " + e.ToString());
            }
        }

        [HttpPut]
        public void PutPlayerIDInChallenge(int intPlayerTargetID, int intPlayerSenderID)
        {
            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 66; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("UPDATE Player SET ChallengedPlayer = (" + intPlayerSenderID + ") WHERE PlayerID = (" + intPlayerTargetID + ")", myConnection);

                sqlCommand.ExecuteReader();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 67; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 68; " + e.ToString());
            }
        }

        [HttpPut]
        public void PutInGameStatus(int intPlayerID, bool boolStatus)
        {
            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 69; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("UPDATE Player SET InGame = '" + boolStatus + "' WHERE PlayerID = (" + intPlayerID + ")", myConnection);

                sqlCommand.ExecuteReader();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 70; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 71; " + e.ToString());
            }
        }

        [HttpDelete]
        /*Trying to delete a player*/
        public void DeletePlayer(int intPlayerID)
        {
            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 72; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("DELETE FROM Player WHERE PlayerID = (" + intPlayerID + ")", myConnection);

                sqlCommand.ExecuteReader();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 73; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 74; " + e.ToString());
            }
        }

        [HttpGet]
        /*Trying to fill UserSelection List*/
        public Dictionary<int, string> GetPlayerNamesByOffline()
        {
            List<int> intList = new List<int>();
            Dictionary<int, string> dicPlayerList = new Dictionary<int, string>();

            try
            {
                myConnection = new SqlConnection("user id=Tmooo; password=Battleships01; server=tmooo.database.windows.net; Trusted_Connection=false; Encrypt=True; database=Battleships; connection timeout=30; MultipleActiveResultSets=true");
                myConnection.Open();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 75; " + e.ToString());
            }

            try
            {
                sqlCommand = new SqlCommand("SELECT PlayerName, PlayerID FROM Player WHERE Online = 'FALSE'", myConnection);

                myReader = sqlCommand.ExecuteReader();

                while (myReader.Read())
                {                    
                    dicPlayerList.Add(Int32.Parse(myReader[1].ToString()), myReader[0].ToString());
                }
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 76; " + e.ToString());
            }

            try
            {
                myConnection.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show("ERROR: 77; " + e.ToString());
            }

            return dicPlayerList;
        }

        public void OpenConnection()
        {

            try
            {
                myConnection.Open();
            }

            catch (Exception e)
            {

            }
            
        }

        public void CloseConnection()
        {
            try
            {
                myConnection.Close();
            }

            catch(Exception e)
            {

            }  
        }
    }
}