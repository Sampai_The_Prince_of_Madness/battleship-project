﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Battleships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Tests
{
    [TestClass()]
    public class GameTests
    {
        [TestMethod()]
        public void ReturnShipDisplayTest()
        {
            //Arrange
            Game game = new Game();
            List<string> strListShipCoord = new List<string>();

            //Act
            strListShipCoord = game.ReturnShipDisplay("E2", 3, false);

            //Assert
            Assert.AreEqual("E3", strListShipCoord[1]);
            Assert.AreEqual("E1", strListShipCoord[2]);
        }

        [TestMethod()]
        public void ReturnShipDisplayTest2()
        {
            //Arrange
            Game game = new Game();
            List<string> strListShipCoord = new List<string>();

            //Act
            strListShipCoord = game.ReturnShipDisplay("D1", 3, true);

            //Assert
            Assert.AreEqual("E1", strListShipCoord[1]);
            Assert.AreEqual("C1", strListShipCoord[2]);
        }

        [TestMethod()]
        public void ReturnShipDisplayTest3()
        {
            //Arrange
            Game game = new Game();
            List<string> strListShipCoord = new List<string>();

            //Act
            strListShipCoord = game.ReturnShipDisplay("J1", 3, true);

            //Assert
            Assert.AreEqual("I1", strListShipCoord[1]);
            Assert.AreEqual("H1", strListShipCoord[2]);
        }

        [TestMethod()]
        public void ReturnShipDisplayTest4()
        {
            //Arrange
            Game game = new Game();
            List<string> strListShipCoord = new List<string>();

            //Act
            strListShipCoord = game.ReturnShipDisplay("J10", 3, false);

            //Assert
            Assert.AreEqual("J9", strListShipCoord[1]);
            Assert.AreEqual("J8", strListShipCoord[2]);
        }

        [TestMethod()]
        public void GameTest()
        {
            //Arrange
            Game game = new Game();

            //Act

            //Assert
            Assert.AreEqual(game, game);
        }

        [TestMethod()]
        public void registerHitTest()
        {
            //arrange
            Game game = new Game();
            game.PopulateGameBoard(2);
            //List<string> strList = new List<string>();
            //Dictionary<string, int> gameBoard = new Dictionary<string, int>();

            //act            
            var test = game.registerHit("A1", 1);
            
            //assert
            Assert.AreEqual(test, 0);
        }

        [TestMethod()]
        public void PopulateGameBoardTest()
        {
            //arrange
            Game game = new Game();

            //act
            game.PopulateGameBoard(1);
            
            //assert
            Assert.IsNotNull(game.gameBoard1);
        }

        [TestMethod()]
        public void RandomPlacementGeneratorTest()
        {
            //arrange
            Game game = new Game();
            game.PopulateGameBoard(1);

            //act
            List<string> test = game.RandomPlacementGenerator(5);
            // = game.gameBoard1.FirstOrDefault(x => x.Value == 1).Key;

            //assert
            Assert.IsNotNull(test);
        }

        [TestMethod()]
        public void RandomFireTest()
        {
            //arrange
            Game game = new Game();
            Random rnd = new Random();
            List<string> strListAlreadyFiredUpon = new List<string>() { "A1" };


            //act
            var Fired = game.RandomAiFire(true, true, true, "A1", "Destroyer", "A1", "A2", 1, "A1", "Easy");


            //assert
            Assert.IsNotNull(Fired);
        }

        [TestMethod()]
        public void RandomAiFireTest()
        {
            //arrange            
            Game game = new Game();
            Random rnd = new Random();
            game.PopulateGameBoard(1);
            game.PopulateGameBoard(2);
            List<string> strListAlreadyFiredUpon = new List<string>();

            //act
            var Fired = game.RandomAiFire(false, false, false, null, "NA", null, null, 0, null, "Hard");

            //assert
            Assert.IsNotNull(Fired);
        }

        [TestMethod()]
        public void AiOrientationSwitch()
        {
            //arrange            
            Game game = new Game();
            Random rnd = new Random();
            game.PopulateGameBoard(1);
            game.PopulateGameBoard(2);
            List<string> strListAlreadyFiredUpon = new List<string>();

            //act
            var Fired = game.RandomAiFire(false, false, true, null, "Ship1", "651", "651", 0, null, "Hard");

            //assert
            Assert.IsNotNull(Fired);
        }


        [TestMethod()]
        public void ClearTakenListTest()
        {
            //arrange
            Game game = new Game();
            List<string> strListInvalidCoord = new List<string>();


            //act
            game.ClearTakenList();


            //assert
            Assert.AreEqual(strListInvalidCoord.Count, 0);
        }

        [TestMethod()]
        public void PlaceShipsOnBoardTest()
        {
            //arrange
            Game game = new Game();
            List<Ship> test = new List<Ship>();

            //act
            game.PopulateGameBoard(1);
            Ship Carrier = new Ship(1, game.RandomPlacementGenerator(5), 5, "Carrier");
            Ship Battleship = new Ship(2, game.RandomPlacementGenerator(4), 4, "Batleship");
            Ship Crusier = new Ship(3, game.RandomPlacementGenerator(3), 3, "Crusier");
            Ship Submarine = new Ship(4, game.RandomPlacementGenerator(3), 3, "Submarine");
            Ship Destroyer = new Ship(5, game.RandomPlacementGenerator(2), 2, "Destoywer");
            test.Add(Carrier);
            test.Add(Battleship);
            test.Add(Crusier);
            test.Add(Submarine);
            test.Add(Destroyer);
            game.PlaceShipsOnBoard(test, 1);

            //assert
            Assert.IsNotNull(game.gameBoard1.FirstOrDefault(x => x.Value == 5).Key);
        }

        [TestMethod()]
        public void ClearListsTest()
        {
            //arrange
            Game game = new Game();
            List<string> validateString = new List<string>()
                {
                    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J"
                };

            Dictionary<string, int> gameBoard1 = new Dictionary<string, int>();
            Dictionary<string, int> gameBoard2 = new Dictionary<string, int>();
            List<string> strListHitNMissCoords1 = new List<string>();
            List<string> strListHitNMissCoords2 = new List<string>();
            List<string> strListInvalidCoord = new List<string>();
            List<string> strListAlreadyFiredUpon = new List<string>();
            
            //act

            game.ClearTakenList();
            //assert
            Assert.AreEqual(validateString[0], "A");
            Assert.AreEqual(gameBoard1.Count, 0);
            Assert.AreEqual(gameBoard2.Count, 0);
            Assert.AreEqual(strListHitNMissCoords1.Count, 0);
            Assert.AreEqual(strListHitNMissCoords2.Count, 0);
            Assert.AreEqual(strListInvalidCoord.Count, 0);
            Assert.AreEqual(strListAlreadyFiredUpon.Count, 0);
        }

        [TestMethod()]
        public void ReturnHitNMissListTest()
        {
            //arrange
            Game game = new Game();
            List<string> strListHitNMissCoords1 = new List<string>() { "A1" };
            List<string> strListHitNMissCoords2 = new List<string>() { "A2" };


            //act
            game.ReturnHitNMissList(1);
            game.ReturnHitNMissList(2);
            //assert
            Assert.AreEqual(strListHitNMissCoords1[0], "A1" );
            Assert.AreEqual(strListHitNMissCoords2[0], "A2" );
        }
    }
}