﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Battleships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Tests
{
    [TestClass()]
    public class PlaceShipTests
    {
        [TestMethod()]
        public void PlaceShipTest()
        {
            //aRrAnGe
            PlaceShip shipPlacer = new PlaceShip("Samuel", "Easy");
            //aCt

            //aSsErT
            Assert.AreEqual(0, shipPlacer.intShipIDGen);
        }

        [TestMethod()]
        public void SetIdsTest()
        {
            //aRrAnGe
            PlaceShip shipPlacer = new PlaceShip("Samuel", "Easy");
            Game game = new Game();
            List<int> intPlayerIDs = new List<int>();
            //aCt
            shipPlacer.SetIds(intPlayerIDs);
            //aSsErT
            Assert.AreEqual(intPlayerIDs, shipPlacer.intPlayerIDs);
        }

        [TestMethod()]
        public void PlaceShipsTest()
        {
            //aRrAnGe
            PlaceShip shipPlacer = new PlaceShip("Samuel", "Easy");
            Game game = new Game();
            List<int> intPlayerIDs = new List<int>();
            //aCt
            shipPlacer.PlaceShips(shipPlacer.GetPciture());
            //aSsErT
            Assert.AreEqual(0, shipPlacer.intShipSize);
        }

        [TestMethod()]
        public void RemoveShipHandlersTest()
        {
            //aRrAnGe
            PlaceShip shipPlacer = new PlaceShip("Samuel", "Easy");
            Game game = new Game();
            List<int> intPlayerIDs = new List<int>();
            //aCt
            shipPlacer.RemoveShipHandlers();
            //aSsErT
            Assert.AreEqual(true, shipPlacer.boolShowShip);
        }

        [TestMethod()]
        public void AddShipHandlersTest()
        {
            //aRrAnGe
            PlaceShip shipPlacer = new PlaceShip("Samuel", "Easy");
            Game game = new Game();
            List<int> intPlayerIDs = new List<int>();
            //aCt
            shipPlacer.AddShipHandlers();
            //aSsErT
            Assert.AreEqual(false, shipPlacer.boolShowShip);
        }

        [TestMethod()]
        public void HideShipChoiceTest()
        {
            //aRrAnGe
            PlaceShip shipPlacer = new PlaceShip("Samuel", "Easy");
            Game game = new Game();
            List<int> intPlayerIDs = new List<int>();
            //aCt
            shipPlacer.HideShipChoice();
            //aSsErT
            Assert.AreEqual(42, shipPlacer.sorryNotSorry);
        }

        [TestMethod()]
        public void ShowShipChoiceTest()
        {
            //aRrAnGe
            PlaceShip shipPlacer = new PlaceShip("Samuel", "Easy");
            Game game = new Game();
            List<int> intPlayerIDs = new List<int>();
            //aCt
            shipPlacer.ShowShipChoice();
            //aSsErT
            Assert.AreEqual(69, shipPlacer.sorryNotSorry);
        }

        [TestMethod()]
        public void ClearBoardTest()
        {
            //aRrAnGe
            PlaceShip shipPlacer = new PlaceShip("Samuel", "Easy");
            Game game = new Game();
            List<int> intPlayerIDs = new List<int>();
            //aCt
            shipPlacer.ClearBoard();
            //aSsErT
            Assert.AreEqual(55, shipPlacer.sorryNotSorry);
        }
    }
}