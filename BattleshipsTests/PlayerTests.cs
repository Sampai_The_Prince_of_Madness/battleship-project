﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Battleships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Tests
{
    [TestClass()]
    public class PlayerTests
    {
        [TestMethod()]
        public void PlayerTest()
        {
            //arrange
            Player player = new Player("Bob", true);
            //act

            //assert
            Assert.AreEqual("Bob", player.strName);
            Assert.AreEqual(true, player.isAI);
        }
    }
}