﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Battleships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Tests
{
    [TestClass()]
    public class ShipTests
    {
        [TestMethod()]
        public void ShipTest()
        {
            //Arrange
            List<Ship> shipList = new List<Ship>();
            List<string> strList = new List<string>() {"A1", "A2", "A3", "A4", "A5"};
            Ship ship5 = new Ship(1, strList, 5, "carrier");
            
            strList = new List<string>() {"B1", "C1", "D1", "E1"};
            Ship ship4 = new Ship(2, strList, 4, "battleship");
            
            strList = new List<string>() {"J9", "J10", "J8"};
            Ship ship3A = new Ship(3, strList, 3, "crusier");
            
            strList = new List<string>() {"I5", "I6", "I7"};
            Ship ship3B = new Ship(4, strList, 3, "submarine");
            
            strList = new List<string>() {"E7", "F7"};
            Ship ship2 = new Ship(5, strList, 2, "destroyer");

            //Act
            shipList.Add(ship5);
            shipList.Add(ship4);
            shipList.Add(ship3A);
            shipList.Add(ship3B);
            shipList.Add(ship2);

            //Assert
            Assert.AreEqual(5, shipList.Count);
        }

        [TestMethod()]
        public void ShipTest2()
        {
            //Arrange
            List<Ship> shipList = new List<Ship>();
            List<string> strList = new List<string>() { "A1", "A2", "A3", "A4", "A5" };
            Ship ship5 = new Ship(1, strList, 5, "carrier");

            //Act
            shipList.Add(ship5);

            //Assert
            Assert.AreEqual(ship5.strShipPos, shipList[0].strShipPos);
        }

        [TestMethod()]
        public void IsHitTest()
        {
            //Arrange
            List<Ship> shipList = new List<Ship>();
            List<string> strList = new List<string>() { "A1", "A2", "A3", "A4", "A5" };
            Ship ship5 = new Ship(1, strList, 5, "carrier");

            //Act
            shipList.Add(ship5);
            shipList[0].IsHit();
            int intHolder = shipList[0].intShipHits;

            //Assert
            Assert.AreEqual(1, intHolder);
        }

        [TestMethod()]
        public void CalcSinkTest()
        {
            //Arrange
            List<Ship> shipList = new List<Ship>();
            List<string> strList = new List<string>() { "A1", "A2", "A3", "A4", "A5" };
            Ship ship5 = new Ship(1, strList, 5, "carrier");

            //Act
            shipList.Add(ship5);
            shipList[0].IsHit();

            //Assert
            Assert.AreEqual(true, shipList[0].CalcSink());
        }

        [TestMethod()]
        public void CalcSinkTest2()
        {
            //Arrange
            List<Ship> shipList = new List<Ship>();
            List<string> strList = new List<string>() { "A1", "A2", "A3", "A4", "A5" };
            Ship ship5 = new Ship(1, strList, 5, "carrier");

            //Act
            shipList.Add(ship5);
            shipList[0].IsHit();
            shipList[0].IsHit();
            shipList[0].IsHit();
            shipList[0].IsHit();
            shipList[0].IsHit();

            //Assert
            Assert.AreEqual(false, shipList[0].CalcSink());
        }

        [TestMethod()]
        public void CalcSinkTest3()
        {
            //Arrange
            List<Ship> shipList = new List<Ship>();
            List<string> strList = new List<string>() { "A1", "A2", "A3", "A4", "A5" };
            Ship ship5 = new Ship(1, strList, 5, "carrier");

            //Act
            shipList.Add(ship5);
            shipList[0].IsHit();
            shipList[0].IsHit();
            shipList[0].IsHit();

            //Assert
            Assert.AreEqual(true, shipList[0].CalcSink());
        }
    }
}