﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Battleships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Tests
{
    [TestClass()]
    public class GameHandlerTests
    {
        [TestMethod()]
        public void CanThereBeShipTest()
        {
            //arrange
            GameHandler gameH = new GameHandler();

            //act
            List<string> test = gameH.CanThereBeShip("A1", 3, true);

            //assert
            Assert.IsNotNull(test);
        }

        [TestMethod()]
        public void RandomTest()
        {
            //arrange
            GameHandler gameH = new GameHandler();

            //act
            List<string> test = gameH.Random(3);

            //assert
            Assert.IsNotNull(test);
        }

        [TestMethod()]
        public void RandomAITest()
        {
            //arrange
            GameHandler gameH = new GameHandler();            
            Random rnd = new Random();
            List<string> strListAlreadyFiredUpon = new List<string>() { "A1" };

            //act
            var Fired = gameH.RandomAI(true, true, true, "A1", "Destroyer", "A1", "A2", 1, "A1" , "Easy");

            //assert
            Assert.AreEqual(strListAlreadyFiredUpon[0], "A1");
           
        }

        [TestMethod()]
        public void PopulateGBTest()
        {
            //arrange
            GameHandler gameH = new GameHandler();

            //act
            gameH.PopulateGB(1);
            var test = gameH.returnGameboard("A1");

            //assert
            Assert.IsNotNull(test);

        }

        [TestMethod()]
        public void ClearTakenListTest()
        {
            //arrange
            GameHandler gameH = new GameHandler();
            List<string> strListInvalidCoord = new List<string>();

            //act
            gameH.ClearTakenList();

            //assert
            Assert.AreEqual(strListInvalidCoord.Count, 0);
        }

        [TestMethod()]
        public void HitTest()
        {
            //Arrange
            GameHandler gameH = new GameHandler();
            //gameH.PopulateGB(1);
            gameH.PopulateGB(2);

            //act
            var test = gameH.Hit("A1", 1);

            //assert
            Assert.AreEqual(test, 0);
        }

        [TestMethod()]
        public void PlaceShipTest()
        {
            //arrange
            GameHandler gameH = new GameHandler();
            Game game = new Game();
            List<Ship> test = new List<Ship>();

            //act
            gameH.PopulateGB(2);
            List<string> strListCoords = new List<string>() { "A1", "B1" };
            //Ship Carrier = new Ship(1, game.RandomPlacementGenerator(5), 5, "Carrier");
            //Ship Battleship = new Ship(2, game.RandomPlacementGenerator(4), 4, "Batleship");
            //Ship Crusier = new Ship(3, game.RandomPlacementGenerator(3), 3, "Crusier");
            //Ship Submarine = new Ship(4, game.RandomPlacementGenerator(3), 3, "Submarine");
            Ship Destroyer = new Ship(5, strListCoords, 2, "Destoyer");
            //test.Add(Carrier);
            //test.Add(Battleship);
            //test.Add(Crusier);
            //test.Add(Submarine);
            test.Add(Destroyer);
            gameH.PlaceShip(test, 2);
            var testy = gameH.Hit("A1", 1);

            //assert
            Assert.AreEqual(testy, 5);
        }

        [TestMethod()]
        public void ClearListsTest()
        {
            //arrange
            GameHandler gameH = new GameHandler();
            List<string> validateString = new List<string>()
                {
                    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J"
                };

            Dictionary<string, int> gameBoard1 = new Dictionary<string, int>();
            Dictionary<string, int> gameBoard2 = new Dictionary<string, int>();
            List<string> strListHitNMissCoords1 = new List<string>();
            List<string> strListHitNMissCoords2 = new List<string>();
            List<string> strListInvalidCoord = new List<string>();
            List<string> strListAlreadyFiredUpon = new List<string>();

            //act

            gameH.ClearLists();
            //assert
            Assert.AreEqual(validateString[0], "A");
            Assert.AreEqual(gameBoard1.Count, 0);
            Assert.AreEqual(gameBoard2.Count, 0);
            Assert.AreEqual(strListHitNMissCoords1.Count, 0);
            Assert.AreEqual(strListHitNMissCoords2.Count, 0);
            Assert.AreEqual(strListInvalidCoord.Count, 0);
            Assert.AreEqual(strListAlreadyFiredUpon.Count, 0);
        }

        [TestMethod()]
        public void ReturnHitNMissTest()
        {
            //arrange
            GameHandler gameH = new GameHandler();
            List<string> strListHitNMissCoords1 = new List<string>() { "A1" };
            List<string> strListHitNMissCoords2 = new List<string>() { "A2" };

            //act
            gameH.ReturnHitNMiss(1);
            gameH.ReturnHitNMiss(2);
            //assert
            Assert.AreEqual(strListHitNMissCoords1[0], "A1");
            Assert.AreEqual(strListHitNMissCoords2[0], "A2");
        }

        [TestMethod()]
        public void returnGameboardTest()
        {
            //arrange
            GameHandler gameH = new GameHandler();
            Game game = new Game();

            //act
            game.PopulateGameBoard(1);
            var Test = game.gameBoard1["A1"];
            //assert            
            Assert.AreEqual(Test, 0);
        }

        [TestMethod()]
        public void TestListReturnTest()
        {
            //arrange

            //act

            //assert            
            Assert.Fail();
        }

        [TestMethod()]
        public void GetCoordinatesTest()
        {
            //arrange

            //act

            //assert            
            Assert.Fail();
        }

        [TestMethod()]
        public void GetGameStatusTest()
        {
            //arrange

            //act

            //assert            
            Assert.Fail();
        }

        [TestMethod()]
        public void PostCoordinatesTest()
        {
            //arrange

            //act

            //assert            
            Assert.Fail();
        }

        [TestMethod()]
        public void DeleteCoordinatesTest()
        {
            //arrange

            //act

            //assert            
            Assert.Fail();
        }

        [TestMethod()]
        public void PutTurnIncrementTest()
        {
            //arrange

            //act

            //assert            
            Assert.Fail();
        }

        [TestMethod()]
        public void PutGameStatusTest()
        {
            //arrange

            //act

            //assert            
            Assert.Fail();
        }

        [TestMethod()]
        public void GetPlayerStatusTest()
        {
            //arrange

            //act

            //assert            
            Assert.Fail();
        }

        [TestMethod()]
        public void GetPlayerByIDTest()
        {
            //arrange

            //act

            //assert            
            Assert.Fail();
        }

        [TestMethod()]
        public void GetPlayerShipIDsTest()
        {
            //arrange

            //act

            //assert            
            Assert.Fail();
        }

        [TestMethod()]
        public void GetPlayerByOfflineTest()
        {
            //arrange

            //act

            //assert            
            Assert.Fail();
        }

        [TestMethod()]
        public void GetPlayerIDByDateTest()
        {
            //arrange

            //act

            //assert            
            Assert.Fail();
        }

        [TestMethod()]
        public void GetPlayerIDByOfflineTest()
        {
            //arrange

            //act

            //assert            
            Assert.Fail();
        }

        [TestMethod()]
        public void PostPlayerTest()
        {
            //arrange

            //act

            //assert            
            Assert.Fail();
        }

        [TestMethod()]
        public void DeletePlayerTest()
        {
            //arrange

            //act

            //assert            
            Assert.Fail();
        }
           
    }
}