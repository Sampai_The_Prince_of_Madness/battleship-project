﻿namespace Battleships
{
    partial class Battleships
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblPlayer = new System.Windows.Forms.Label();
            this.btnMove = new System.Windows.Forms.Button();
            this.btnSonar = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.pJ10 = new System.Windows.Forms.PictureBox();
            this.pJ9 = new System.Windows.Forms.PictureBox();
            this.pJ8 = new System.Windows.Forms.PictureBox();
            this.pJ7 = new System.Windows.Forms.PictureBox();
            this.pJ6 = new System.Windows.Forms.PictureBox();
            this.pJ5 = new System.Windows.Forms.PictureBox();
            this.pJ4 = new System.Windows.Forms.PictureBox();
            this.pJ3 = new System.Windows.Forms.PictureBox();
            this.pJ2 = new System.Windows.Forms.PictureBox();
            this.pJ1 = new System.Windows.Forms.PictureBox();
            this.pC10 = new System.Windows.Forms.PictureBox();
            this.pC9 = new System.Windows.Forms.PictureBox();
            this.pC8 = new System.Windows.Forms.PictureBox();
            this.pC7 = new System.Windows.Forms.PictureBox();
            this.pC6 = new System.Windows.Forms.PictureBox();
            this.pC5 = new System.Windows.Forms.PictureBox();
            this.pC4 = new System.Windows.Forms.PictureBox();
            this.pC3 = new System.Windows.Forms.PictureBox();
            this.pC2 = new System.Windows.Forms.PictureBox();
            this.pC1 = new System.Windows.Forms.PictureBox();
            this.pB10 = new System.Windows.Forms.PictureBox();
            this.pB9 = new System.Windows.Forms.PictureBox();
            this.pB8 = new System.Windows.Forms.PictureBox();
            this.pB7 = new System.Windows.Forms.PictureBox();
            this.pB6 = new System.Windows.Forms.PictureBox();
            this.pB5 = new System.Windows.Forms.PictureBox();
            this.pB4 = new System.Windows.Forms.PictureBox();
            this.pB3 = new System.Windows.Forms.PictureBox();
            this.pB2 = new System.Windows.Forms.PictureBox();
            this.pB1 = new System.Windows.Forms.PictureBox();
            this.pA10 = new System.Windows.Forms.PictureBox();
            this.pA9 = new System.Windows.Forms.PictureBox();
            this.pA8 = new System.Windows.Forms.PictureBox();
            this.pA7 = new System.Windows.Forms.PictureBox();
            this.pA6 = new System.Windows.Forms.PictureBox();
            this.pA5 = new System.Windows.Forms.PictureBox();
            this.pA4 = new System.Windows.Forms.PictureBox();
            this.pA3 = new System.Windows.Forms.PictureBox();
            this.pA2 = new System.Windows.Forms.PictureBox();
            this.pA1 = new System.Windows.Forms.PictureBox();
            this.pF10 = new System.Windows.Forms.PictureBox();
            this.pF9 = new System.Windows.Forms.PictureBox();
            this.pF8 = new System.Windows.Forms.PictureBox();
            this.pF7 = new System.Windows.Forms.PictureBox();
            this.pF6 = new System.Windows.Forms.PictureBox();
            this.pF5 = new System.Windows.Forms.PictureBox();
            this.pF4 = new System.Windows.Forms.PictureBox();
            this.pF3 = new System.Windows.Forms.PictureBox();
            this.pF2 = new System.Windows.Forms.PictureBox();
            this.pF1 = new System.Windows.Forms.PictureBox();
            this.pE10 = new System.Windows.Forms.PictureBox();
            this.pE9 = new System.Windows.Forms.PictureBox();
            this.pE8 = new System.Windows.Forms.PictureBox();
            this.pE7 = new System.Windows.Forms.PictureBox();
            this.pE6 = new System.Windows.Forms.PictureBox();
            this.pE5 = new System.Windows.Forms.PictureBox();
            this.pE4 = new System.Windows.Forms.PictureBox();
            this.pE3 = new System.Windows.Forms.PictureBox();
            this.pE2 = new System.Windows.Forms.PictureBox();
            this.pE1 = new System.Windows.Forms.PictureBox();
            this.pD10 = new System.Windows.Forms.PictureBox();
            this.pD9 = new System.Windows.Forms.PictureBox();
            this.pD8 = new System.Windows.Forms.PictureBox();
            this.pD7 = new System.Windows.Forms.PictureBox();
            this.pD6 = new System.Windows.Forms.PictureBox();
            this.pD5 = new System.Windows.Forms.PictureBox();
            this.pD4 = new System.Windows.Forms.PictureBox();
            this.pD3 = new System.Windows.Forms.PictureBox();
            this.pD2 = new System.Windows.Forms.PictureBox();
            this.pD1 = new System.Windows.Forms.PictureBox();
            this.pI10 = new System.Windows.Forms.PictureBox();
            this.pI9 = new System.Windows.Forms.PictureBox();
            this.pI8 = new System.Windows.Forms.PictureBox();
            this.pI7 = new System.Windows.Forms.PictureBox();
            this.pI6 = new System.Windows.Forms.PictureBox();
            this.pI5 = new System.Windows.Forms.PictureBox();
            this.pI4 = new System.Windows.Forms.PictureBox();
            this.pI3 = new System.Windows.Forms.PictureBox();
            this.pI2 = new System.Windows.Forms.PictureBox();
            this.pI1 = new System.Windows.Forms.PictureBox();
            this.pH10 = new System.Windows.Forms.PictureBox();
            this.pH9 = new System.Windows.Forms.PictureBox();
            this.pH8 = new System.Windows.Forms.PictureBox();
            this.pH7 = new System.Windows.Forms.PictureBox();
            this.pH6 = new System.Windows.Forms.PictureBox();
            this.pH5 = new System.Windows.Forms.PictureBox();
            this.pH4 = new System.Windows.Forms.PictureBox();
            this.pH3 = new System.Windows.Forms.PictureBox();
            this.pH2 = new System.Windows.Forms.PictureBox();
            this.pH1 = new System.Windows.Forms.PictureBox();
            this.A9 = new System.Windows.Forms.PictureBox();
            this.A8 = new System.Windows.Forms.PictureBox();
            this.A7 = new System.Windows.Forms.PictureBox();
            this.A6 = new System.Windows.Forms.PictureBox();
            this.A5 = new System.Windows.Forms.PictureBox();
            this.A10 = new System.Windows.Forms.PictureBox();
            this.A4 = new System.Windows.Forms.PictureBox();
            this.A3 = new System.Windows.Forms.PictureBox();
            this.A2 = new System.Windows.Forms.PictureBox();
            this.A1 = new System.Windows.Forms.PictureBox();
            this.B9 = new System.Windows.Forms.PictureBox();
            this.B8 = new System.Windows.Forms.PictureBox();
            this.B7 = new System.Windows.Forms.PictureBox();
            this.B6 = new System.Windows.Forms.PictureBox();
            this.B5 = new System.Windows.Forms.PictureBox();
            this.B10 = new System.Windows.Forms.PictureBox();
            this.B4 = new System.Windows.Forms.PictureBox();
            this.B3 = new System.Windows.Forms.PictureBox();
            this.B2 = new System.Windows.Forms.PictureBox();
            this.B1 = new System.Windows.Forms.PictureBox();
            this.J9 = new System.Windows.Forms.PictureBox();
            this.J8 = new System.Windows.Forms.PictureBox();
            this.J7 = new System.Windows.Forms.PictureBox();
            this.J6 = new System.Windows.Forms.PictureBox();
            this.J5 = new System.Windows.Forms.PictureBox();
            this.J10 = new System.Windows.Forms.PictureBox();
            this.J4 = new System.Windows.Forms.PictureBox();
            this.J3 = new System.Windows.Forms.PictureBox();
            this.J2 = new System.Windows.Forms.PictureBox();
            this.J1 = new System.Windows.Forms.PictureBox();
            this.C9 = new System.Windows.Forms.PictureBox();
            this.C8 = new System.Windows.Forms.PictureBox();
            this.C7 = new System.Windows.Forms.PictureBox();
            this.C6 = new System.Windows.Forms.PictureBox();
            this.C5 = new System.Windows.Forms.PictureBox();
            this.C10 = new System.Windows.Forms.PictureBox();
            this.C4 = new System.Windows.Forms.PictureBox();
            this.C3 = new System.Windows.Forms.PictureBox();
            this.C2 = new System.Windows.Forms.PictureBox();
            this.C1 = new System.Windows.Forms.PictureBox();
            this.I9 = new System.Windows.Forms.PictureBox();
            this.I8 = new System.Windows.Forms.PictureBox();
            this.I7 = new System.Windows.Forms.PictureBox();
            this.I6 = new System.Windows.Forms.PictureBox();
            this.I5 = new System.Windows.Forms.PictureBox();
            this.I10 = new System.Windows.Forms.PictureBox();
            this.I4 = new System.Windows.Forms.PictureBox();
            this.I3 = new System.Windows.Forms.PictureBox();
            this.I2 = new System.Windows.Forms.PictureBox();
            this.I1 = new System.Windows.Forms.PictureBox();
            this.H9 = new System.Windows.Forms.PictureBox();
            this.H8 = new System.Windows.Forms.PictureBox();
            this.H7 = new System.Windows.Forms.PictureBox();
            this.H6 = new System.Windows.Forms.PictureBox();
            this.H5 = new System.Windows.Forms.PictureBox();
            this.H10 = new System.Windows.Forms.PictureBox();
            this.H4 = new System.Windows.Forms.PictureBox();
            this.H3 = new System.Windows.Forms.PictureBox();
            this.H2 = new System.Windows.Forms.PictureBox();
            this.H1 = new System.Windows.Forms.PictureBox();
            this.G9 = new System.Windows.Forms.PictureBox();
            this.G8 = new System.Windows.Forms.PictureBox();
            this.G7 = new System.Windows.Forms.PictureBox();
            this.G6 = new System.Windows.Forms.PictureBox();
            this.G5 = new System.Windows.Forms.PictureBox();
            this.G10 = new System.Windows.Forms.PictureBox();
            this.G4 = new System.Windows.Forms.PictureBox();
            this.G3 = new System.Windows.Forms.PictureBox();
            this.G2 = new System.Windows.Forms.PictureBox();
            this.G1 = new System.Windows.Forms.PictureBox();
            this.F9 = new System.Windows.Forms.PictureBox();
            this.F8 = new System.Windows.Forms.PictureBox();
            this.F7 = new System.Windows.Forms.PictureBox();
            this.F6 = new System.Windows.Forms.PictureBox();
            this.F5 = new System.Windows.Forms.PictureBox();
            this.F10 = new System.Windows.Forms.PictureBox();
            this.F4 = new System.Windows.Forms.PictureBox();
            this.F3 = new System.Windows.Forms.PictureBox();
            this.F2 = new System.Windows.Forms.PictureBox();
            this.F1 = new System.Windows.Forms.PictureBox();
            this.E9 = new System.Windows.Forms.PictureBox();
            this.E8 = new System.Windows.Forms.PictureBox();
            this.E7 = new System.Windows.Forms.PictureBox();
            this.E6 = new System.Windows.Forms.PictureBox();
            this.E5 = new System.Windows.Forms.PictureBox();
            this.E10 = new System.Windows.Forms.PictureBox();
            this.E4 = new System.Windows.Forms.PictureBox();
            this.E3 = new System.Windows.Forms.PictureBox();
            this.E2 = new System.Windows.Forms.PictureBox();
            this.E1 = new System.Windows.Forms.PictureBox();
            this.pG10 = new System.Windows.Forms.PictureBox();
            this.pG9 = new System.Windows.Forms.PictureBox();
            this.pG8 = new System.Windows.Forms.PictureBox();
            this.pG7 = new System.Windows.Forms.PictureBox();
            this.pG6 = new System.Windows.Forms.PictureBox();
            this.pG5 = new System.Windows.Forms.PictureBox();
            this.pG4 = new System.Windows.Forms.PictureBox();
            this.pG3 = new System.Windows.Forms.PictureBox();
            this.pG2 = new System.Windows.Forms.PictureBox();
            this.pG1 = new System.Windows.Forms.PictureBox();
            this.D9 = new System.Windows.Forms.PictureBox();
            this.D8 = new System.Windows.Forms.PictureBox();
            this.D7 = new System.Windows.Forms.PictureBox();
            this.D6 = new System.Windows.Forms.PictureBox();
            this.D5 = new System.Windows.Forms.PictureBox();
            this.D10 = new System.Windows.Forms.PictureBox();
            this.D4 = new System.Windows.Forms.PictureBox();
            this.D3 = new System.Windows.Forms.PictureBox();
            this.D2 = new System.Windows.Forms.PictureBox();
            this.D1 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.picVolume = new System.Windows.Forms.PictureBox();
            this.btnHit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pJ10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVolume)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 29.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(76, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(375, 46);
            this.label1.TabIndex = 350;
            this.label1.Text = "Opponent\'s Board";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 29.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(457, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(274, 46);
            this.label2.TabIndex = 351;
            this.label2.Text = " Your Board";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(382, 642);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 27);
            this.label3.TabIndex = 352;
            this.label3.Text = "HIT";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(383, 683);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 27);
            this.label4.TabIndex = 353;
            this.label4.Text = "SHIP";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(384, 725);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 25);
            this.label5.TabIndex = 354;
            this.label5.Text = "MISS";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Location = new System.Drawing.Point(216, 570);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(384, 199);
            this.label6.TabIndex = 355;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(348, 642);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 27);
            this.label7.TabIndex = 356;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Aqua;
            this.label8.Location = new System.Drawing.Point(348, 683);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 27);
            this.label8.TabIndex = 357;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Gray;
            this.label9.Location = new System.Drawing.Point(348, 725);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 27);
            this.label9.TabIndex = 358;
            // 
            // lblPlayer
            // 
            this.lblPlayer.BackColor = System.Drawing.Color.White;
            this.lblPlayer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayer.Location = new System.Drawing.Point(339, 570);
            this.lblPlayer.Name = "lblPlayer";
            this.lblPlayer.Size = new System.Drawing.Size(131, 49);
            this.lblPlayer.TabIndex = 359;
            this.lblPlayer.Text = "Player 1";
            this.lblPlayer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMove
            // 
            this.btnMove.BackColor = System.Drawing.Color.Silver;
            this.btnMove.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMove.Location = new System.Drawing.Point(493, 182);
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(202, 23);
            this.btnMove.TabIndex = 363;
            this.btnMove.Text = "Move";
            this.btnMove.UseVisualStyleBackColor = false;
            // 
            // btnSonar
            // 
            this.btnSonar.BackColor = System.Drawing.Color.Silver;
            this.btnSonar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSonar.Location = new System.Drawing.Point(493, 211);
            this.btnSonar.Name = "btnSonar";
            this.btnSonar.Size = new System.Drawing.Size(202, 23);
            this.btnSonar.TabIndex = 364;
            this.btnSonar.Text = "Sonar";
            this.btnSonar.UseVisualStyleBackColor = false;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Black;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.Location = new System.Drawing.Point(457, 243);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(274, 280);
            this.label12.TabIndex = 365;
            // 
            // pJ10
            // 
            this.pJ10.BackColor = System.Drawing.Color.White;
            this.pJ10.Location = new System.Drawing.Point(701, 494);
            this.pJ10.Name = "pJ10";
            this.pJ10.Size = new System.Drawing.Size(20, 20);
            this.pJ10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pJ10.TabIndex = 349;
            this.pJ10.TabStop = false;
            // 
            // pJ9
            // 
            this.pJ9.BackColor = System.Drawing.Color.White;
            this.pJ9.Location = new System.Drawing.Point(675, 494);
            this.pJ9.Name = "pJ9";
            this.pJ9.Size = new System.Drawing.Size(20, 20);
            this.pJ9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pJ9.TabIndex = 348;
            this.pJ9.TabStop = false;
            // 
            // pJ8
            // 
            this.pJ8.BackColor = System.Drawing.Color.White;
            this.pJ8.Location = new System.Drawing.Point(649, 494);
            this.pJ8.Name = "pJ8";
            this.pJ8.Size = new System.Drawing.Size(20, 20);
            this.pJ8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pJ8.TabIndex = 347;
            this.pJ8.TabStop = false;
            // 
            // pJ7
            // 
            this.pJ7.BackColor = System.Drawing.Color.White;
            this.pJ7.Location = new System.Drawing.Point(623, 494);
            this.pJ7.Name = "pJ7";
            this.pJ7.Size = new System.Drawing.Size(20, 20);
            this.pJ7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pJ7.TabIndex = 346;
            this.pJ7.TabStop = false;
            // 
            // pJ6
            // 
            this.pJ6.BackColor = System.Drawing.Color.White;
            this.pJ6.Location = new System.Drawing.Point(597, 494);
            this.pJ6.Name = "pJ6";
            this.pJ6.Size = new System.Drawing.Size(20, 20);
            this.pJ6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pJ6.TabIndex = 345;
            this.pJ6.TabStop = false;
            // 
            // pJ5
            // 
            this.pJ5.BackColor = System.Drawing.Color.White;
            this.pJ5.Location = new System.Drawing.Point(571, 494);
            this.pJ5.Name = "pJ5";
            this.pJ5.Size = new System.Drawing.Size(20, 20);
            this.pJ5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pJ5.TabIndex = 344;
            this.pJ5.TabStop = false;
            // 
            // pJ4
            // 
            this.pJ4.BackColor = System.Drawing.Color.White;
            this.pJ4.Location = new System.Drawing.Point(545, 494);
            this.pJ4.Name = "pJ4";
            this.pJ4.Size = new System.Drawing.Size(20, 20);
            this.pJ4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pJ4.TabIndex = 343;
            this.pJ4.TabStop = false;
            // 
            // pJ3
            // 
            this.pJ3.BackColor = System.Drawing.Color.White;
            this.pJ3.Location = new System.Drawing.Point(519, 494);
            this.pJ3.Name = "pJ3";
            this.pJ3.Size = new System.Drawing.Size(20, 20);
            this.pJ3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pJ3.TabIndex = 342;
            this.pJ3.TabStop = false;
            // 
            // pJ2
            // 
            this.pJ2.BackColor = System.Drawing.Color.White;
            this.pJ2.Location = new System.Drawing.Point(493, 494);
            this.pJ2.Name = "pJ2";
            this.pJ2.Size = new System.Drawing.Size(20, 20);
            this.pJ2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pJ2.TabIndex = 341;
            this.pJ2.TabStop = false;
            // 
            // pJ1
            // 
            this.pJ1.BackColor = System.Drawing.Color.White;
            this.pJ1.Location = new System.Drawing.Point(467, 494);
            this.pJ1.Name = "pJ1";
            this.pJ1.Size = new System.Drawing.Size(20, 20);
            this.pJ1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pJ1.TabIndex = 340;
            this.pJ1.TabStop = false;
            // 
            // pC10
            // 
            this.pC10.BackColor = System.Drawing.Color.White;
            this.pC10.Location = new System.Drawing.Point(701, 305);
            this.pC10.Name = "pC10";
            this.pC10.Size = new System.Drawing.Size(20, 20);
            this.pC10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pC10.TabIndex = 339;
            this.pC10.TabStop = false;
            // 
            // pC9
            // 
            this.pC9.BackColor = System.Drawing.Color.White;
            this.pC9.Location = new System.Drawing.Point(675, 305);
            this.pC9.Name = "pC9";
            this.pC9.Size = new System.Drawing.Size(20, 20);
            this.pC9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pC9.TabIndex = 338;
            this.pC9.TabStop = false;
            // 
            // pC8
            // 
            this.pC8.BackColor = System.Drawing.Color.White;
            this.pC8.Location = new System.Drawing.Point(649, 305);
            this.pC8.Name = "pC8";
            this.pC8.Size = new System.Drawing.Size(20, 20);
            this.pC8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pC8.TabIndex = 337;
            this.pC8.TabStop = false;
            // 
            // pC7
            // 
            this.pC7.BackColor = System.Drawing.Color.White;
            this.pC7.Location = new System.Drawing.Point(623, 305);
            this.pC7.Name = "pC7";
            this.pC7.Size = new System.Drawing.Size(20, 20);
            this.pC7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pC7.TabIndex = 336;
            this.pC7.TabStop = false;
            // 
            // pC6
            // 
            this.pC6.BackColor = System.Drawing.Color.White;
            this.pC6.Location = new System.Drawing.Point(597, 305);
            this.pC6.Name = "pC6";
            this.pC6.Size = new System.Drawing.Size(20, 20);
            this.pC6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pC6.TabIndex = 335;
            this.pC6.TabStop = false;
            // 
            // pC5
            // 
            this.pC5.BackColor = System.Drawing.Color.White;
            this.pC5.Location = new System.Drawing.Point(571, 305);
            this.pC5.Name = "pC5";
            this.pC5.Size = new System.Drawing.Size(20, 20);
            this.pC5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pC5.TabIndex = 334;
            this.pC5.TabStop = false;
            // 
            // pC4
            // 
            this.pC4.BackColor = System.Drawing.Color.White;
            this.pC4.Location = new System.Drawing.Point(545, 305);
            this.pC4.Name = "pC4";
            this.pC4.Size = new System.Drawing.Size(20, 20);
            this.pC4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pC4.TabIndex = 333;
            this.pC4.TabStop = false;
            // 
            // pC3
            // 
            this.pC3.BackColor = System.Drawing.Color.White;
            this.pC3.Location = new System.Drawing.Point(519, 305);
            this.pC3.Name = "pC3";
            this.pC3.Size = new System.Drawing.Size(20, 20);
            this.pC3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pC3.TabIndex = 332;
            this.pC3.TabStop = false;
            // 
            // pC2
            // 
            this.pC2.BackColor = System.Drawing.Color.White;
            this.pC2.Location = new System.Drawing.Point(493, 305);
            this.pC2.Name = "pC2";
            this.pC2.Size = new System.Drawing.Size(20, 20);
            this.pC2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pC2.TabIndex = 331;
            this.pC2.TabStop = false;
            // 
            // pC1
            // 
            this.pC1.BackColor = System.Drawing.Color.White;
            this.pC1.Location = new System.Drawing.Point(467, 305);
            this.pC1.Name = "pC1";
            this.pC1.Size = new System.Drawing.Size(20, 20);
            this.pC1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pC1.TabIndex = 330;
            this.pC1.TabStop = false;
            // 
            // pB10
            // 
            this.pB10.BackColor = System.Drawing.Color.White;
            this.pB10.Location = new System.Drawing.Point(701, 279);
            this.pB10.Name = "pB10";
            this.pB10.Size = new System.Drawing.Size(20, 20);
            this.pB10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pB10.TabIndex = 329;
            this.pB10.TabStop = false;
            // 
            // pB9
            // 
            this.pB9.BackColor = System.Drawing.Color.White;
            this.pB9.Location = new System.Drawing.Point(675, 279);
            this.pB9.Name = "pB9";
            this.pB9.Size = new System.Drawing.Size(20, 20);
            this.pB9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pB9.TabIndex = 328;
            this.pB9.TabStop = false;
            // 
            // pB8
            // 
            this.pB8.BackColor = System.Drawing.Color.White;
            this.pB8.Location = new System.Drawing.Point(649, 279);
            this.pB8.Name = "pB8";
            this.pB8.Size = new System.Drawing.Size(20, 20);
            this.pB8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pB8.TabIndex = 327;
            this.pB8.TabStop = false;
            // 
            // pB7
            // 
            this.pB7.BackColor = System.Drawing.Color.White;
            this.pB7.Location = new System.Drawing.Point(623, 279);
            this.pB7.Name = "pB7";
            this.pB7.Size = new System.Drawing.Size(20, 20);
            this.pB7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pB7.TabIndex = 326;
            this.pB7.TabStop = false;
            // 
            // pB6
            // 
            this.pB6.BackColor = System.Drawing.Color.White;
            this.pB6.Location = new System.Drawing.Point(597, 279);
            this.pB6.Name = "pB6";
            this.pB6.Size = new System.Drawing.Size(20, 20);
            this.pB6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pB6.TabIndex = 325;
            this.pB6.TabStop = false;
            // 
            // pB5
            // 
            this.pB5.BackColor = System.Drawing.Color.White;
            this.pB5.Location = new System.Drawing.Point(571, 279);
            this.pB5.Name = "pB5";
            this.pB5.Size = new System.Drawing.Size(20, 20);
            this.pB5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pB5.TabIndex = 324;
            this.pB5.TabStop = false;
            // 
            // pB4
            // 
            this.pB4.BackColor = System.Drawing.Color.White;
            this.pB4.Location = new System.Drawing.Point(545, 279);
            this.pB4.Name = "pB4";
            this.pB4.Size = new System.Drawing.Size(20, 20);
            this.pB4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pB4.TabIndex = 323;
            this.pB4.TabStop = false;
            // 
            // pB3
            // 
            this.pB3.BackColor = System.Drawing.Color.White;
            this.pB3.Location = new System.Drawing.Point(519, 279);
            this.pB3.Name = "pB3";
            this.pB3.Size = new System.Drawing.Size(20, 20);
            this.pB3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pB3.TabIndex = 322;
            this.pB3.TabStop = false;
            // 
            // pB2
            // 
            this.pB2.BackColor = System.Drawing.Color.White;
            this.pB2.Location = new System.Drawing.Point(493, 279);
            this.pB2.Name = "pB2";
            this.pB2.Size = new System.Drawing.Size(20, 20);
            this.pB2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pB2.TabIndex = 321;
            this.pB2.TabStop = false;
            // 
            // pB1
            // 
            this.pB1.BackColor = System.Drawing.Color.White;
            this.pB1.Location = new System.Drawing.Point(467, 279);
            this.pB1.Name = "pB1";
            this.pB1.Size = new System.Drawing.Size(20, 20);
            this.pB1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pB1.TabIndex = 320;
            this.pB1.TabStop = false;
            // 
            // pA10
            // 
            this.pA10.BackColor = System.Drawing.Color.White;
            this.pA10.Location = new System.Drawing.Point(701, 251);
            this.pA10.Name = "pA10";
            this.pA10.Size = new System.Drawing.Size(20, 20);
            this.pA10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pA10.TabIndex = 319;
            this.pA10.TabStop = false;
            // 
            // pA9
            // 
            this.pA9.BackColor = System.Drawing.Color.White;
            this.pA9.Location = new System.Drawing.Point(675, 251);
            this.pA9.Name = "pA9";
            this.pA9.Size = new System.Drawing.Size(20, 20);
            this.pA9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pA9.TabIndex = 318;
            this.pA9.TabStop = false;
            // 
            // pA8
            // 
            this.pA8.BackColor = System.Drawing.Color.White;
            this.pA8.Location = new System.Drawing.Point(649, 251);
            this.pA8.Name = "pA8";
            this.pA8.Size = new System.Drawing.Size(20, 20);
            this.pA8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pA8.TabIndex = 317;
            this.pA8.TabStop = false;
            // 
            // pA7
            // 
            this.pA7.BackColor = System.Drawing.Color.White;
            this.pA7.Location = new System.Drawing.Point(623, 251);
            this.pA7.Name = "pA7";
            this.pA7.Size = new System.Drawing.Size(20, 20);
            this.pA7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pA7.TabIndex = 316;
            this.pA7.TabStop = false;
            // 
            // pA6
            // 
            this.pA6.BackColor = System.Drawing.Color.White;
            this.pA6.Location = new System.Drawing.Point(597, 251);
            this.pA6.Name = "pA6";
            this.pA6.Size = new System.Drawing.Size(20, 20);
            this.pA6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pA6.TabIndex = 315;
            this.pA6.TabStop = false;
            // 
            // pA5
            // 
            this.pA5.BackColor = System.Drawing.Color.White;
            this.pA5.Location = new System.Drawing.Point(571, 251);
            this.pA5.Name = "pA5";
            this.pA5.Size = new System.Drawing.Size(20, 20);
            this.pA5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pA5.TabIndex = 314;
            this.pA5.TabStop = false;
            // 
            // pA4
            // 
            this.pA4.BackColor = System.Drawing.Color.White;
            this.pA4.Location = new System.Drawing.Point(545, 251);
            this.pA4.Name = "pA4";
            this.pA4.Size = new System.Drawing.Size(20, 20);
            this.pA4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pA4.TabIndex = 313;
            this.pA4.TabStop = false;
            // 
            // pA3
            // 
            this.pA3.BackColor = System.Drawing.Color.White;
            this.pA3.Location = new System.Drawing.Point(519, 251);
            this.pA3.Name = "pA3";
            this.pA3.Size = new System.Drawing.Size(20, 20);
            this.pA3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pA3.TabIndex = 312;
            this.pA3.TabStop = false;
            // 
            // pA2
            // 
            this.pA2.BackColor = System.Drawing.Color.White;
            this.pA2.Location = new System.Drawing.Point(493, 251);
            this.pA2.Name = "pA2";
            this.pA2.Size = new System.Drawing.Size(20, 20);
            this.pA2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pA2.TabIndex = 311;
            this.pA2.TabStop = false;
            // 
            // pA1
            // 
            this.pA1.BackColor = System.Drawing.Color.White;
            this.pA1.Location = new System.Drawing.Point(467, 251);
            this.pA1.Name = "pA1";
            this.pA1.Size = new System.Drawing.Size(20, 20);
            this.pA1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pA1.TabIndex = 310;
            this.pA1.TabStop = false;
            // 
            // pF10
            // 
            this.pF10.BackColor = System.Drawing.Color.White;
            this.pF10.Location = new System.Drawing.Point(701, 386);
            this.pF10.Name = "pF10";
            this.pF10.Size = new System.Drawing.Size(20, 20);
            this.pF10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pF10.TabIndex = 309;
            this.pF10.TabStop = false;
            // 
            // pF9
            // 
            this.pF9.BackColor = System.Drawing.Color.White;
            this.pF9.Location = new System.Drawing.Point(675, 386);
            this.pF9.Name = "pF9";
            this.pF9.Size = new System.Drawing.Size(20, 20);
            this.pF9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pF9.TabIndex = 308;
            this.pF9.TabStop = false;
            // 
            // pF8
            // 
            this.pF8.BackColor = System.Drawing.Color.White;
            this.pF8.Location = new System.Drawing.Point(649, 386);
            this.pF8.Name = "pF8";
            this.pF8.Size = new System.Drawing.Size(20, 20);
            this.pF8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pF8.TabIndex = 307;
            this.pF8.TabStop = false;
            // 
            // pF7
            // 
            this.pF7.BackColor = System.Drawing.Color.White;
            this.pF7.Location = new System.Drawing.Point(623, 386);
            this.pF7.Name = "pF7";
            this.pF7.Size = new System.Drawing.Size(20, 20);
            this.pF7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pF7.TabIndex = 306;
            this.pF7.TabStop = false;
            // 
            // pF6
            // 
            this.pF6.BackColor = System.Drawing.Color.White;
            this.pF6.Location = new System.Drawing.Point(597, 386);
            this.pF6.Name = "pF6";
            this.pF6.Size = new System.Drawing.Size(20, 20);
            this.pF6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pF6.TabIndex = 305;
            this.pF6.TabStop = false;
            // 
            // pF5
            // 
            this.pF5.BackColor = System.Drawing.Color.White;
            this.pF5.Location = new System.Drawing.Point(571, 386);
            this.pF5.Name = "pF5";
            this.pF5.Size = new System.Drawing.Size(20, 20);
            this.pF5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pF5.TabIndex = 304;
            this.pF5.TabStop = false;
            // 
            // pF4
            // 
            this.pF4.BackColor = System.Drawing.Color.White;
            this.pF4.Location = new System.Drawing.Point(545, 386);
            this.pF4.Name = "pF4";
            this.pF4.Size = new System.Drawing.Size(20, 20);
            this.pF4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pF4.TabIndex = 303;
            this.pF4.TabStop = false;
            // 
            // pF3
            // 
            this.pF3.BackColor = System.Drawing.Color.White;
            this.pF3.Location = new System.Drawing.Point(519, 386);
            this.pF3.Name = "pF3";
            this.pF3.Size = new System.Drawing.Size(20, 20);
            this.pF3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pF3.TabIndex = 302;
            this.pF3.TabStop = false;
            // 
            // pF2
            // 
            this.pF2.BackColor = System.Drawing.Color.White;
            this.pF2.Location = new System.Drawing.Point(493, 386);
            this.pF2.Name = "pF2";
            this.pF2.Size = new System.Drawing.Size(20, 20);
            this.pF2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pF2.TabIndex = 301;
            this.pF2.TabStop = false;
            // 
            // pF1
            // 
            this.pF1.BackColor = System.Drawing.Color.White;
            this.pF1.Location = new System.Drawing.Point(467, 386);
            this.pF1.Name = "pF1";
            this.pF1.Size = new System.Drawing.Size(20, 20);
            this.pF1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pF1.TabIndex = 300;
            this.pF1.TabStop = false;
            // 
            // pE10
            // 
            this.pE10.BackColor = System.Drawing.Color.White;
            this.pE10.Location = new System.Drawing.Point(701, 360);
            this.pE10.Name = "pE10";
            this.pE10.Size = new System.Drawing.Size(20, 20);
            this.pE10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pE10.TabIndex = 299;
            this.pE10.TabStop = false;
            // 
            // pE9
            // 
            this.pE9.BackColor = System.Drawing.Color.White;
            this.pE9.Location = new System.Drawing.Point(675, 360);
            this.pE9.Name = "pE9";
            this.pE9.Size = new System.Drawing.Size(20, 20);
            this.pE9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pE9.TabIndex = 298;
            this.pE9.TabStop = false;
            // 
            // pE8
            // 
            this.pE8.BackColor = System.Drawing.Color.White;
            this.pE8.Location = new System.Drawing.Point(649, 360);
            this.pE8.Name = "pE8";
            this.pE8.Size = new System.Drawing.Size(20, 20);
            this.pE8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pE8.TabIndex = 297;
            this.pE8.TabStop = false;
            // 
            // pE7
            // 
            this.pE7.BackColor = System.Drawing.Color.White;
            this.pE7.Location = new System.Drawing.Point(623, 360);
            this.pE7.Name = "pE7";
            this.pE7.Size = new System.Drawing.Size(20, 20);
            this.pE7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pE7.TabIndex = 296;
            this.pE7.TabStop = false;
            // 
            // pE6
            // 
            this.pE6.BackColor = System.Drawing.Color.White;
            this.pE6.Location = new System.Drawing.Point(597, 360);
            this.pE6.Name = "pE6";
            this.pE6.Size = new System.Drawing.Size(20, 20);
            this.pE6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pE6.TabIndex = 295;
            this.pE6.TabStop = false;
            // 
            // pE5
            // 
            this.pE5.BackColor = System.Drawing.Color.White;
            this.pE5.Location = new System.Drawing.Point(571, 360);
            this.pE5.Name = "pE5";
            this.pE5.Size = new System.Drawing.Size(20, 20);
            this.pE5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pE5.TabIndex = 294;
            this.pE5.TabStop = false;
            // 
            // pE4
            // 
            this.pE4.BackColor = System.Drawing.Color.White;
            this.pE4.Location = new System.Drawing.Point(545, 360);
            this.pE4.Name = "pE4";
            this.pE4.Size = new System.Drawing.Size(20, 20);
            this.pE4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pE4.TabIndex = 293;
            this.pE4.TabStop = false;
            // 
            // pE3
            // 
            this.pE3.BackColor = System.Drawing.Color.White;
            this.pE3.Location = new System.Drawing.Point(519, 360);
            this.pE3.Name = "pE3";
            this.pE3.Size = new System.Drawing.Size(20, 20);
            this.pE3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pE3.TabIndex = 292;
            this.pE3.TabStop = false;
            // 
            // pE2
            // 
            this.pE2.BackColor = System.Drawing.Color.White;
            this.pE2.Location = new System.Drawing.Point(493, 360);
            this.pE2.Name = "pE2";
            this.pE2.Size = new System.Drawing.Size(20, 20);
            this.pE2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pE2.TabIndex = 291;
            this.pE2.TabStop = false;
            // 
            // pE1
            // 
            this.pE1.BackColor = System.Drawing.Color.White;
            this.pE1.Location = new System.Drawing.Point(467, 360);
            this.pE1.Name = "pE1";
            this.pE1.Size = new System.Drawing.Size(20, 20);
            this.pE1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pE1.TabIndex = 290;
            this.pE1.TabStop = false;
            // 
            // pD10
            // 
            this.pD10.BackColor = System.Drawing.Color.White;
            this.pD10.Location = new System.Drawing.Point(701, 332);
            this.pD10.Name = "pD10";
            this.pD10.Size = new System.Drawing.Size(20, 20);
            this.pD10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pD10.TabIndex = 289;
            this.pD10.TabStop = false;
            // 
            // pD9
            // 
            this.pD9.BackColor = System.Drawing.Color.White;
            this.pD9.Location = new System.Drawing.Point(675, 332);
            this.pD9.Name = "pD9";
            this.pD9.Size = new System.Drawing.Size(20, 20);
            this.pD9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pD9.TabIndex = 288;
            this.pD9.TabStop = false;
            // 
            // pD8
            // 
            this.pD8.BackColor = System.Drawing.Color.White;
            this.pD8.Location = new System.Drawing.Point(649, 332);
            this.pD8.Name = "pD8";
            this.pD8.Size = new System.Drawing.Size(20, 20);
            this.pD8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pD8.TabIndex = 287;
            this.pD8.TabStop = false;
            // 
            // pD7
            // 
            this.pD7.BackColor = System.Drawing.Color.White;
            this.pD7.Location = new System.Drawing.Point(623, 332);
            this.pD7.Name = "pD7";
            this.pD7.Size = new System.Drawing.Size(20, 20);
            this.pD7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pD7.TabIndex = 286;
            this.pD7.TabStop = false;
            // 
            // pD6
            // 
            this.pD6.BackColor = System.Drawing.Color.White;
            this.pD6.Location = new System.Drawing.Point(597, 332);
            this.pD6.Name = "pD6";
            this.pD6.Size = new System.Drawing.Size(20, 20);
            this.pD6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pD6.TabIndex = 285;
            this.pD6.TabStop = false;
            // 
            // pD5
            // 
            this.pD5.BackColor = System.Drawing.Color.White;
            this.pD5.Location = new System.Drawing.Point(571, 332);
            this.pD5.Name = "pD5";
            this.pD5.Size = new System.Drawing.Size(20, 20);
            this.pD5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pD5.TabIndex = 284;
            this.pD5.TabStop = false;
            // 
            // pD4
            // 
            this.pD4.BackColor = System.Drawing.Color.White;
            this.pD4.Location = new System.Drawing.Point(545, 332);
            this.pD4.Name = "pD4";
            this.pD4.Size = new System.Drawing.Size(20, 20);
            this.pD4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pD4.TabIndex = 283;
            this.pD4.TabStop = false;
            // 
            // pD3
            // 
            this.pD3.BackColor = System.Drawing.Color.White;
            this.pD3.Location = new System.Drawing.Point(519, 332);
            this.pD3.Name = "pD3";
            this.pD3.Size = new System.Drawing.Size(20, 20);
            this.pD3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pD3.TabIndex = 282;
            this.pD3.TabStop = false;
            // 
            // pD2
            // 
            this.pD2.BackColor = System.Drawing.Color.White;
            this.pD2.Location = new System.Drawing.Point(493, 332);
            this.pD2.Name = "pD2";
            this.pD2.Size = new System.Drawing.Size(20, 20);
            this.pD2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pD2.TabIndex = 281;
            this.pD2.TabStop = false;
            // 
            // pD1
            // 
            this.pD1.BackColor = System.Drawing.Color.White;
            this.pD1.Location = new System.Drawing.Point(467, 332);
            this.pD1.Name = "pD1";
            this.pD1.Size = new System.Drawing.Size(20, 20);
            this.pD1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pD1.TabIndex = 280;
            this.pD1.TabStop = false;
            // 
            // pI10
            // 
            this.pI10.BackColor = System.Drawing.Color.White;
            this.pI10.Location = new System.Drawing.Point(701, 468);
            this.pI10.Name = "pI10";
            this.pI10.Size = new System.Drawing.Size(20, 20);
            this.pI10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pI10.TabIndex = 279;
            this.pI10.TabStop = false;
            // 
            // pI9
            // 
            this.pI9.BackColor = System.Drawing.Color.White;
            this.pI9.Location = new System.Drawing.Point(675, 468);
            this.pI9.Name = "pI9";
            this.pI9.Size = new System.Drawing.Size(20, 20);
            this.pI9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pI9.TabIndex = 278;
            this.pI9.TabStop = false;
            // 
            // pI8
            // 
            this.pI8.BackColor = System.Drawing.Color.White;
            this.pI8.Location = new System.Drawing.Point(649, 468);
            this.pI8.Name = "pI8";
            this.pI8.Size = new System.Drawing.Size(20, 20);
            this.pI8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pI8.TabIndex = 277;
            this.pI8.TabStop = false;
            // 
            // pI7
            // 
            this.pI7.BackColor = System.Drawing.Color.White;
            this.pI7.Location = new System.Drawing.Point(623, 468);
            this.pI7.Name = "pI7";
            this.pI7.Size = new System.Drawing.Size(20, 20);
            this.pI7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pI7.TabIndex = 276;
            this.pI7.TabStop = false;
            // 
            // pI6
            // 
            this.pI6.BackColor = System.Drawing.Color.White;
            this.pI6.Location = new System.Drawing.Point(597, 468);
            this.pI6.Name = "pI6";
            this.pI6.Size = new System.Drawing.Size(20, 20);
            this.pI6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pI6.TabIndex = 275;
            this.pI6.TabStop = false;
            // 
            // pI5
            // 
            this.pI5.BackColor = System.Drawing.Color.White;
            this.pI5.Location = new System.Drawing.Point(571, 468);
            this.pI5.Name = "pI5";
            this.pI5.Size = new System.Drawing.Size(20, 20);
            this.pI5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pI5.TabIndex = 274;
            this.pI5.TabStop = false;
            // 
            // pI4
            // 
            this.pI4.BackColor = System.Drawing.Color.White;
            this.pI4.Location = new System.Drawing.Point(545, 468);
            this.pI4.Name = "pI4";
            this.pI4.Size = new System.Drawing.Size(20, 20);
            this.pI4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pI4.TabIndex = 273;
            this.pI4.TabStop = false;
            // 
            // pI3
            // 
            this.pI3.BackColor = System.Drawing.Color.White;
            this.pI3.Location = new System.Drawing.Point(519, 468);
            this.pI3.Name = "pI3";
            this.pI3.Size = new System.Drawing.Size(20, 20);
            this.pI3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pI3.TabIndex = 272;
            this.pI3.TabStop = false;
            // 
            // pI2
            // 
            this.pI2.BackColor = System.Drawing.Color.White;
            this.pI2.Location = new System.Drawing.Point(493, 468);
            this.pI2.Name = "pI2";
            this.pI2.Size = new System.Drawing.Size(20, 20);
            this.pI2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pI2.TabIndex = 271;
            this.pI2.TabStop = false;
            // 
            // pI1
            // 
            this.pI1.BackColor = System.Drawing.Color.White;
            this.pI1.Location = new System.Drawing.Point(467, 468);
            this.pI1.Name = "pI1";
            this.pI1.Size = new System.Drawing.Size(20, 20);
            this.pI1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pI1.TabIndex = 270;
            this.pI1.TabStop = false;
            // 
            // pH10
            // 
            this.pH10.BackColor = System.Drawing.Color.White;
            this.pH10.Location = new System.Drawing.Point(701, 442);
            this.pH10.Name = "pH10";
            this.pH10.Size = new System.Drawing.Size(20, 20);
            this.pH10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pH10.TabIndex = 269;
            this.pH10.TabStop = false;
            // 
            // pH9
            // 
            this.pH9.BackColor = System.Drawing.Color.White;
            this.pH9.Location = new System.Drawing.Point(675, 442);
            this.pH9.Name = "pH9";
            this.pH9.Size = new System.Drawing.Size(20, 20);
            this.pH9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pH9.TabIndex = 268;
            this.pH9.TabStop = false;
            // 
            // pH8
            // 
            this.pH8.BackColor = System.Drawing.Color.White;
            this.pH8.Location = new System.Drawing.Point(649, 442);
            this.pH8.Name = "pH8";
            this.pH8.Size = new System.Drawing.Size(20, 20);
            this.pH8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pH8.TabIndex = 267;
            this.pH8.TabStop = false;
            // 
            // pH7
            // 
            this.pH7.BackColor = System.Drawing.Color.White;
            this.pH7.Location = new System.Drawing.Point(623, 442);
            this.pH7.Name = "pH7";
            this.pH7.Size = new System.Drawing.Size(20, 20);
            this.pH7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pH7.TabIndex = 266;
            this.pH7.TabStop = false;
            // 
            // pH6
            // 
            this.pH6.BackColor = System.Drawing.Color.White;
            this.pH6.Location = new System.Drawing.Point(597, 442);
            this.pH6.Name = "pH6";
            this.pH6.Size = new System.Drawing.Size(20, 20);
            this.pH6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pH6.TabIndex = 265;
            this.pH6.TabStop = false;
            // 
            // pH5
            // 
            this.pH5.BackColor = System.Drawing.Color.White;
            this.pH5.Location = new System.Drawing.Point(571, 442);
            this.pH5.Name = "pH5";
            this.pH5.Size = new System.Drawing.Size(20, 20);
            this.pH5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pH5.TabIndex = 264;
            this.pH5.TabStop = false;
            // 
            // pH4
            // 
            this.pH4.BackColor = System.Drawing.Color.White;
            this.pH4.Location = new System.Drawing.Point(545, 442);
            this.pH4.Name = "pH4";
            this.pH4.Size = new System.Drawing.Size(20, 20);
            this.pH4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pH4.TabIndex = 263;
            this.pH4.TabStop = false;
            // 
            // pH3
            // 
            this.pH3.BackColor = System.Drawing.Color.White;
            this.pH3.Location = new System.Drawing.Point(519, 442);
            this.pH3.Name = "pH3";
            this.pH3.Size = new System.Drawing.Size(20, 20);
            this.pH3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pH3.TabIndex = 262;
            this.pH3.TabStop = false;
            // 
            // pH2
            // 
            this.pH2.BackColor = System.Drawing.Color.White;
            this.pH2.Location = new System.Drawing.Point(493, 442);
            this.pH2.Name = "pH2";
            this.pH2.Size = new System.Drawing.Size(20, 20);
            this.pH2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pH2.TabIndex = 261;
            this.pH2.TabStop = false;
            // 
            // pH1
            // 
            this.pH1.BackColor = System.Drawing.Color.White;
            this.pH1.Location = new System.Drawing.Point(467, 442);
            this.pH1.Name = "pH1";
            this.pH1.Size = new System.Drawing.Size(20, 20);
            this.pH1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pH1.TabIndex = 260;
            this.pH1.TabStop = false;
            // 
            // A9
            // 
            this.A9.BackColor = System.Drawing.Color.White;
            this.A9.Location = new System.Drawing.Point(375, 160);
            this.A9.Name = "A9";
            this.A9.Size = new System.Drawing.Size(30, 30);
            this.A9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A9.TabIndex = 259;
            this.A9.TabStop = false;
            this.A9.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // A8
            // 
            this.A8.BackColor = System.Drawing.Color.White;
            this.A8.Location = new System.Drawing.Point(339, 160);
            this.A8.Name = "A8";
            this.A8.Size = new System.Drawing.Size(30, 30);
            this.A8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A8.TabIndex = 258;
            this.A8.TabStop = false;
            this.A8.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // A7
            // 
            this.A7.BackColor = System.Drawing.Color.White;
            this.A7.Location = new System.Drawing.Point(303, 160);
            this.A7.Name = "A7";
            this.A7.Size = new System.Drawing.Size(30, 30);
            this.A7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A7.TabIndex = 257;
            this.A7.TabStop = false;
            this.A7.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // A6
            // 
            this.A6.BackColor = System.Drawing.Color.White;
            this.A6.Location = new System.Drawing.Point(267, 160);
            this.A6.Name = "A6";
            this.A6.Size = new System.Drawing.Size(30, 30);
            this.A6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A6.TabIndex = 256;
            this.A6.TabStop = false;
            this.A6.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // A5
            // 
            this.A5.BackColor = System.Drawing.Color.White;
            this.A5.Location = new System.Drawing.Point(231, 160);
            this.A5.Name = "A5";
            this.A5.Size = new System.Drawing.Size(30, 30);
            this.A5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A5.TabIndex = 255;
            this.A5.TabStop = false;
            this.A5.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // A10
            // 
            this.A10.BackColor = System.Drawing.Color.White;
            this.A10.Location = new System.Drawing.Point(411, 160);
            this.A10.Name = "A10";
            this.A10.Size = new System.Drawing.Size(30, 30);
            this.A10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A10.TabIndex = 254;
            this.A10.TabStop = false;
            this.A10.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // A4
            // 
            this.A4.BackColor = System.Drawing.Color.White;
            this.A4.Location = new System.Drawing.Point(195, 160);
            this.A4.Name = "A4";
            this.A4.Size = new System.Drawing.Size(30, 30);
            this.A4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A4.TabIndex = 253;
            this.A4.TabStop = false;
            this.A4.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // A3
            // 
            this.A3.BackColor = System.Drawing.Color.White;
            this.A3.Location = new System.Drawing.Point(159, 160);
            this.A3.Name = "A3";
            this.A3.Size = new System.Drawing.Size(30, 30);
            this.A3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A3.TabIndex = 252;
            this.A3.TabStop = false;
            this.A3.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // A2
            // 
            this.A2.AccessibleDescription = "";
            this.A2.BackColor = System.Drawing.Color.White;
            this.A2.Location = new System.Drawing.Point(123, 160);
            this.A2.Name = "A2";
            this.A2.Size = new System.Drawing.Size(30, 30);
            this.A2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A2.TabIndex = 251;
            this.A2.TabStop = false;
            this.A2.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // A1
            // 
            this.A1.BackColor = System.Drawing.Color.White;
            this.A1.Location = new System.Drawing.Point(87, 160);
            this.A1.Name = "A1";
            this.A1.Size = new System.Drawing.Size(30, 30);
            this.A1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A1.TabIndex = 250;
            this.A1.TabStop = false;
            this.A1.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // B9
            // 
            this.B9.BackColor = System.Drawing.Color.White;
            this.B9.Location = new System.Drawing.Point(375, 196);
            this.B9.Name = "B9";
            this.B9.Size = new System.Drawing.Size(30, 30);
            this.B9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B9.TabIndex = 249;
            this.B9.TabStop = false;
            this.B9.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // B8
            // 
            this.B8.BackColor = System.Drawing.Color.White;
            this.B8.Location = new System.Drawing.Point(339, 196);
            this.B8.Name = "B8";
            this.B8.Size = new System.Drawing.Size(30, 30);
            this.B8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B8.TabIndex = 248;
            this.B8.TabStop = false;
            this.B8.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // B7
            // 
            this.B7.BackColor = System.Drawing.Color.White;
            this.B7.Location = new System.Drawing.Point(303, 196);
            this.B7.Name = "B7";
            this.B7.Size = new System.Drawing.Size(30, 30);
            this.B7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B7.TabIndex = 247;
            this.B7.TabStop = false;
            this.B7.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // B6
            // 
            this.B6.BackColor = System.Drawing.Color.White;
            this.B6.Location = new System.Drawing.Point(267, 196);
            this.B6.Name = "B6";
            this.B6.Size = new System.Drawing.Size(30, 30);
            this.B6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B6.TabIndex = 246;
            this.B6.TabStop = false;
            this.B6.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // B5
            // 
            this.B5.BackColor = System.Drawing.Color.White;
            this.B5.Location = new System.Drawing.Point(231, 196);
            this.B5.Name = "B5";
            this.B5.Size = new System.Drawing.Size(30, 30);
            this.B5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B5.TabIndex = 245;
            this.B5.TabStop = false;
            this.B5.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // B10
            // 
            this.B10.BackColor = System.Drawing.Color.White;
            this.B10.Location = new System.Drawing.Point(411, 196);
            this.B10.Name = "B10";
            this.B10.Size = new System.Drawing.Size(30, 30);
            this.B10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B10.TabIndex = 244;
            this.B10.TabStop = false;
            this.B10.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // B4
            // 
            this.B4.BackColor = System.Drawing.Color.White;
            this.B4.Location = new System.Drawing.Point(195, 196);
            this.B4.Name = "B4";
            this.B4.Size = new System.Drawing.Size(30, 30);
            this.B4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B4.TabIndex = 243;
            this.B4.TabStop = false;
            this.B4.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // B3
            // 
            this.B3.BackColor = System.Drawing.Color.White;
            this.B3.Location = new System.Drawing.Point(159, 196);
            this.B3.Name = "B3";
            this.B3.Size = new System.Drawing.Size(30, 30);
            this.B3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B3.TabIndex = 242;
            this.B3.TabStop = false;
            this.B3.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // B2
            // 
            this.B2.BackColor = System.Drawing.Color.White;
            this.B2.Location = new System.Drawing.Point(123, 196);
            this.B2.Name = "B2";
            this.B2.Size = new System.Drawing.Size(30, 30);
            this.B2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B2.TabIndex = 241;
            this.B2.TabStop = false;
            this.B2.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // B1
            // 
            this.B1.BackColor = System.Drawing.Color.White;
            this.B1.Location = new System.Drawing.Point(87, 196);
            this.B1.Name = "B1";
            this.B1.Size = new System.Drawing.Size(30, 30);
            this.B1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B1.TabIndex = 240;
            this.B1.TabStop = false;
            this.B1.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // J9
            // 
            this.J9.BackColor = System.Drawing.Color.White;
            this.J9.Location = new System.Drawing.Point(375, 484);
            this.J9.Name = "J9";
            this.J9.Size = new System.Drawing.Size(30, 30);
            this.J9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J9.TabIndex = 239;
            this.J9.TabStop = false;
            this.J9.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // J8
            // 
            this.J8.BackColor = System.Drawing.Color.White;
            this.J8.Location = new System.Drawing.Point(339, 484);
            this.J8.Name = "J8";
            this.J8.Size = new System.Drawing.Size(30, 30);
            this.J8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J8.TabIndex = 238;
            this.J8.TabStop = false;
            this.J8.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // J7
            // 
            this.J7.BackColor = System.Drawing.Color.White;
            this.J7.Location = new System.Drawing.Point(303, 484);
            this.J7.Name = "J7";
            this.J7.Size = new System.Drawing.Size(30, 30);
            this.J7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J7.TabIndex = 237;
            this.J7.TabStop = false;
            this.J7.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // J6
            // 
            this.J6.BackColor = System.Drawing.Color.White;
            this.J6.Location = new System.Drawing.Point(267, 484);
            this.J6.Name = "J6";
            this.J6.Size = new System.Drawing.Size(30, 30);
            this.J6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J6.TabIndex = 236;
            this.J6.TabStop = false;
            this.J6.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // J5
            // 
            this.J5.BackColor = System.Drawing.Color.White;
            this.J5.Location = new System.Drawing.Point(231, 484);
            this.J5.Name = "J5";
            this.J5.Size = new System.Drawing.Size(30, 30);
            this.J5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J5.TabIndex = 235;
            this.J5.TabStop = false;
            this.J5.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // J10
            // 
            this.J10.BackColor = System.Drawing.Color.White;
            this.J10.Location = new System.Drawing.Point(411, 484);
            this.J10.Name = "J10";
            this.J10.Size = new System.Drawing.Size(30, 30);
            this.J10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J10.TabIndex = 234;
            this.J10.TabStop = false;
            this.J10.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // J4
            // 
            this.J4.BackColor = System.Drawing.Color.White;
            this.J4.Location = new System.Drawing.Point(195, 484);
            this.J4.Name = "J4";
            this.J4.Size = new System.Drawing.Size(30, 30);
            this.J4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J4.TabIndex = 233;
            this.J4.TabStop = false;
            this.J4.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // J3
            // 
            this.J3.BackColor = System.Drawing.Color.White;
            this.J3.Location = new System.Drawing.Point(159, 484);
            this.J3.Name = "J3";
            this.J3.Size = new System.Drawing.Size(30, 30);
            this.J3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J3.TabIndex = 232;
            this.J3.TabStop = false;
            this.J3.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // J2
            // 
            this.J2.BackColor = System.Drawing.Color.White;
            this.J2.Location = new System.Drawing.Point(123, 484);
            this.J2.Name = "J2";
            this.J2.Size = new System.Drawing.Size(30, 30);
            this.J2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J2.TabIndex = 231;
            this.J2.TabStop = false;
            this.J2.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // J1
            // 
            this.J1.BackColor = System.Drawing.Color.White;
            this.J1.Location = new System.Drawing.Point(87, 484);
            this.J1.Name = "J1";
            this.J1.Size = new System.Drawing.Size(30, 30);
            this.J1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J1.TabIndex = 230;
            this.J1.TabStop = false;
            this.J1.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // C9
            // 
            this.C9.BackColor = System.Drawing.Color.White;
            this.C9.Location = new System.Drawing.Point(375, 232);
            this.C9.Name = "C9";
            this.C9.Size = new System.Drawing.Size(30, 30);
            this.C9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C9.TabIndex = 229;
            this.C9.TabStop = false;
            this.C9.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // C8
            // 
            this.C8.BackColor = System.Drawing.Color.White;
            this.C8.Location = new System.Drawing.Point(339, 232);
            this.C8.Name = "C8";
            this.C8.Size = new System.Drawing.Size(30, 30);
            this.C8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C8.TabIndex = 228;
            this.C8.TabStop = false;
            this.C8.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // C7
            // 
            this.C7.BackColor = System.Drawing.Color.White;
            this.C7.Location = new System.Drawing.Point(303, 232);
            this.C7.Name = "C7";
            this.C7.Size = new System.Drawing.Size(30, 30);
            this.C7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C7.TabIndex = 227;
            this.C7.TabStop = false;
            this.C7.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // C6
            // 
            this.C6.BackColor = System.Drawing.Color.White;
            this.C6.Location = new System.Drawing.Point(267, 232);
            this.C6.Name = "C6";
            this.C6.Size = new System.Drawing.Size(30, 30);
            this.C6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C6.TabIndex = 226;
            this.C6.TabStop = false;
            this.C6.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // C5
            // 
            this.C5.BackColor = System.Drawing.Color.White;
            this.C5.Location = new System.Drawing.Point(231, 232);
            this.C5.Name = "C5";
            this.C5.Size = new System.Drawing.Size(30, 30);
            this.C5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C5.TabIndex = 225;
            this.C5.TabStop = false;
            this.C5.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // C10
            // 
            this.C10.BackColor = System.Drawing.Color.White;
            this.C10.Location = new System.Drawing.Point(411, 232);
            this.C10.Name = "C10";
            this.C10.Size = new System.Drawing.Size(30, 30);
            this.C10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C10.TabIndex = 224;
            this.C10.TabStop = false;
            this.C10.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // C4
            // 
            this.C4.BackColor = System.Drawing.Color.White;
            this.C4.Location = new System.Drawing.Point(195, 232);
            this.C4.Name = "C4";
            this.C4.Size = new System.Drawing.Size(30, 30);
            this.C4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C4.TabIndex = 223;
            this.C4.TabStop = false;
            this.C4.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // C3
            // 
            this.C3.BackColor = System.Drawing.Color.White;
            this.C3.Location = new System.Drawing.Point(159, 232);
            this.C3.Name = "C3";
            this.C3.Size = new System.Drawing.Size(30, 30);
            this.C3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C3.TabIndex = 222;
            this.C3.TabStop = false;
            this.C3.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // C2
            // 
            this.C2.BackColor = System.Drawing.Color.White;
            this.C2.Location = new System.Drawing.Point(123, 232);
            this.C2.Name = "C2";
            this.C2.Size = new System.Drawing.Size(30, 30);
            this.C2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C2.TabIndex = 221;
            this.C2.TabStop = false;
            this.C2.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // C1
            // 
            this.C1.BackColor = System.Drawing.Color.White;
            this.C1.Location = new System.Drawing.Point(87, 232);
            this.C1.Name = "C1";
            this.C1.Size = new System.Drawing.Size(30, 30);
            this.C1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C1.TabIndex = 220;
            this.C1.TabStop = false;
            this.C1.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // I9
            // 
            this.I9.BackColor = System.Drawing.Color.White;
            this.I9.Location = new System.Drawing.Point(375, 448);
            this.I9.Name = "I9";
            this.I9.Size = new System.Drawing.Size(30, 30);
            this.I9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I9.TabIndex = 219;
            this.I9.TabStop = false;
            this.I9.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // I8
            // 
            this.I8.BackColor = System.Drawing.Color.White;
            this.I8.Location = new System.Drawing.Point(339, 448);
            this.I8.Name = "I8";
            this.I8.Size = new System.Drawing.Size(30, 30);
            this.I8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I8.TabIndex = 218;
            this.I8.TabStop = false;
            this.I8.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // I7
            // 
            this.I7.BackColor = System.Drawing.Color.White;
            this.I7.Location = new System.Drawing.Point(303, 448);
            this.I7.Name = "I7";
            this.I7.Size = new System.Drawing.Size(30, 30);
            this.I7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I7.TabIndex = 217;
            this.I7.TabStop = false;
            this.I7.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // I6
            // 
            this.I6.BackColor = System.Drawing.Color.White;
            this.I6.Location = new System.Drawing.Point(267, 448);
            this.I6.Name = "I6";
            this.I6.Size = new System.Drawing.Size(30, 30);
            this.I6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I6.TabIndex = 216;
            this.I6.TabStop = false;
            this.I6.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // I5
            // 
            this.I5.BackColor = System.Drawing.Color.White;
            this.I5.Location = new System.Drawing.Point(231, 448);
            this.I5.Name = "I5";
            this.I5.Size = new System.Drawing.Size(30, 30);
            this.I5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I5.TabIndex = 215;
            this.I5.TabStop = false;
            this.I5.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // I10
            // 
            this.I10.BackColor = System.Drawing.Color.White;
            this.I10.Location = new System.Drawing.Point(411, 448);
            this.I10.Name = "I10";
            this.I10.Size = new System.Drawing.Size(30, 30);
            this.I10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I10.TabIndex = 214;
            this.I10.TabStop = false;
            this.I10.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // I4
            // 
            this.I4.BackColor = System.Drawing.Color.White;
            this.I4.Location = new System.Drawing.Point(195, 448);
            this.I4.Name = "I4";
            this.I4.Size = new System.Drawing.Size(30, 30);
            this.I4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I4.TabIndex = 213;
            this.I4.TabStop = false;
            this.I4.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // I3
            // 
            this.I3.BackColor = System.Drawing.Color.White;
            this.I3.Location = new System.Drawing.Point(159, 448);
            this.I3.Name = "I3";
            this.I3.Size = new System.Drawing.Size(30, 30);
            this.I3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I3.TabIndex = 212;
            this.I3.TabStop = false;
            this.I3.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // I2
            // 
            this.I2.BackColor = System.Drawing.Color.White;
            this.I2.Location = new System.Drawing.Point(123, 448);
            this.I2.Name = "I2";
            this.I2.Size = new System.Drawing.Size(30, 30);
            this.I2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I2.TabIndex = 211;
            this.I2.TabStop = false;
            this.I2.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // I1
            // 
            this.I1.BackColor = System.Drawing.Color.White;
            this.I1.Location = new System.Drawing.Point(87, 448);
            this.I1.Name = "I1";
            this.I1.Size = new System.Drawing.Size(30, 30);
            this.I1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I1.TabIndex = 210;
            this.I1.TabStop = false;
            this.I1.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // H9
            // 
            this.H9.BackColor = System.Drawing.Color.White;
            this.H9.Location = new System.Drawing.Point(375, 412);
            this.H9.Name = "H9";
            this.H9.Size = new System.Drawing.Size(30, 30);
            this.H9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H9.TabIndex = 209;
            this.H9.TabStop = false;
            this.H9.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // H8
            // 
            this.H8.BackColor = System.Drawing.Color.White;
            this.H8.Location = new System.Drawing.Point(339, 412);
            this.H8.Name = "H8";
            this.H8.Size = new System.Drawing.Size(30, 30);
            this.H8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H8.TabIndex = 208;
            this.H8.TabStop = false;
            this.H8.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // H7
            // 
            this.H7.BackColor = System.Drawing.Color.White;
            this.H7.Location = new System.Drawing.Point(303, 412);
            this.H7.Name = "H7";
            this.H7.Size = new System.Drawing.Size(30, 30);
            this.H7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H7.TabIndex = 207;
            this.H7.TabStop = false;
            this.H7.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // H6
            // 
            this.H6.BackColor = System.Drawing.Color.White;
            this.H6.Location = new System.Drawing.Point(267, 412);
            this.H6.Name = "H6";
            this.H6.Size = new System.Drawing.Size(30, 30);
            this.H6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H6.TabIndex = 206;
            this.H6.TabStop = false;
            this.H6.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // H5
            // 
            this.H5.BackColor = System.Drawing.Color.White;
            this.H5.Location = new System.Drawing.Point(231, 412);
            this.H5.Name = "H5";
            this.H5.Size = new System.Drawing.Size(30, 30);
            this.H5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H5.TabIndex = 205;
            this.H5.TabStop = false;
            this.H5.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // H10
            // 
            this.H10.BackColor = System.Drawing.Color.White;
            this.H10.Location = new System.Drawing.Point(411, 412);
            this.H10.Name = "H10";
            this.H10.Size = new System.Drawing.Size(30, 30);
            this.H10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H10.TabIndex = 204;
            this.H10.TabStop = false;
            this.H10.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // H4
            // 
            this.H4.BackColor = System.Drawing.Color.White;
            this.H4.Location = new System.Drawing.Point(195, 412);
            this.H4.Name = "H4";
            this.H4.Size = new System.Drawing.Size(30, 30);
            this.H4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H4.TabIndex = 203;
            this.H4.TabStop = false;
            this.H4.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // H3
            // 
            this.H3.BackColor = System.Drawing.Color.White;
            this.H3.Location = new System.Drawing.Point(159, 412);
            this.H3.Name = "H3";
            this.H3.Size = new System.Drawing.Size(30, 30);
            this.H3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H3.TabIndex = 202;
            this.H3.TabStop = false;
            this.H3.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // H2
            // 
            this.H2.BackColor = System.Drawing.Color.White;
            this.H2.Location = new System.Drawing.Point(123, 412);
            this.H2.Name = "H2";
            this.H2.Size = new System.Drawing.Size(30, 30);
            this.H2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H2.TabIndex = 201;
            this.H2.TabStop = false;
            this.H2.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // H1
            // 
            this.H1.BackColor = System.Drawing.Color.White;
            this.H1.Location = new System.Drawing.Point(87, 412);
            this.H1.Name = "H1";
            this.H1.Size = new System.Drawing.Size(30, 30);
            this.H1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H1.TabIndex = 200;
            this.H1.TabStop = false;
            this.H1.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // G9
            // 
            this.G9.BackColor = System.Drawing.Color.White;
            this.G9.Location = new System.Drawing.Point(375, 376);
            this.G9.Name = "G9";
            this.G9.Size = new System.Drawing.Size(30, 30);
            this.G9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G9.TabIndex = 199;
            this.G9.TabStop = false;
            this.G9.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // G8
            // 
            this.G8.BackColor = System.Drawing.Color.White;
            this.G8.Location = new System.Drawing.Point(339, 376);
            this.G8.Name = "G8";
            this.G8.Size = new System.Drawing.Size(30, 30);
            this.G8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G8.TabIndex = 198;
            this.G8.TabStop = false;
            this.G8.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // G7
            // 
            this.G7.BackColor = System.Drawing.Color.White;
            this.G7.Location = new System.Drawing.Point(303, 376);
            this.G7.Name = "G7";
            this.G7.Size = new System.Drawing.Size(30, 30);
            this.G7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G7.TabIndex = 197;
            this.G7.TabStop = false;
            this.G7.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // G6
            // 
            this.G6.BackColor = System.Drawing.Color.White;
            this.G6.Location = new System.Drawing.Point(267, 376);
            this.G6.Name = "G6";
            this.G6.Size = new System.Drawing.Size(30, 30);
            this.G6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G6.TabIndex = 196;
            this.G6.TabStop = false;
            this.G6.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // G5
            // 
            this.G5.BackColor = System.Drawing.Color.White;
            this.G5.Location = new System.Drawing.Point(231, 376);
            this.G5.Name = "G5";
            this.G5.Size = new System.Drawing.Size(30, 30);
            this.G5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G5.TabIndex = 195;
            this.G5.TabStop = false;
            this.G5.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // G10
            // 
            this.G10.BackColor = System.Drawing.Color.White;
            this.G10.Location = new System.Drawing.Point(411, 376);
            this.G10.Name = "G10";
            this.G10.Size = new System.Drawing.Size(30, 30);
            this.G10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G10.TabIndex = 194;
            this.G10.TabStop = false;
            this.G10.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // G4
            // 
            this.G4.BackColor = System.Drawing.Color.White;
            this.G4.Location = new System.Drawing.Point(195, 376);
            this.G4.Name = "G4";
            this.G4.Size = new System.Drawing.Size(30, 30);
            this.G4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G4.TabIndex = 193;
            this.G4.TabStop = false;
            this.G4.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // G3
            // 
            this.G3.BackColor = System.Drawing.Color.White;
            this.G3.Location = new System.Drawing.Point(159, 376);
            this.G3.Name = "G3";
            this.G3.Size = new System.Drawing.Size(30, 30);
            this.G3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G3.TabIndex = 192;
            this.G3.TabStop = false;
            this.G3.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // G2
            // 
            this.G2.BackColor = System.Drawing.Color.White;
            this.G2.Location = new System.Drawing.Point(123, 376);
            this.G2.Name = "G2";
            this.G2.Size = new System.Drawing.Size(30, 30);
            this.G2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G2.TabIndex = 191;
            this.G2.TabStop = false;
            this.G2.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // G1
            // 
            this.G1.BackColor = System.Drawing.Color.White;
            this.G1.Location = new System.Drawing.Point(87, 376);
            this.G1.Name = "G1";
            this.G1.Size = new System.Drawing.Size(30, 30);
            this.G1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G1.TabIndex = 190;
            this.G1.TabStop = false;
            this.G1.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // F9
            // 
            this.F9.BackColor = System.Drawing.Color.White;
            this.F9.Location = new System.Drawing.Point(375, 340);
            this.F9.Name = "F9";
            this.F9.Size = new System.Drawing.Size(30, 30);
            this.F9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F9.TabIndex = 189;
            this.F9.TabStop = false;
            this.F9.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // F8
            // 
            this.F8.BackColor = System.Drawing.Color.White;
            this.F8.Location = new System.Drawing.Point(339, 340);
            this.F8.Name = "F8";
            this.F8.Size = new System.Drawing.Size(30, 30);
            this.F8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F8.TabIndex = 188;
            this.F8.TabStop = false;
            this.F8.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // F7
            // 
            this.F7.BackColor = System.Drawing.Color.White;
            this.F7.Location = new System.Drawing.Point(303, 340);
            this.F7.Name = "F7";
            this.F7.Size = new System.Drawing.Size(30, 30);
            this.F7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F7.TabIndex = 187;
            this.F7.TabStop = false;
            this.F7.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // F6
            // 
            this.F6.BackColor = System.Drawing.Color.White;
            this.F6.Location = new System.Drawing.Point(267, 340);
            this.F6.Name = "F6";
            this.F6.Size = new System.Drawing.Size(30, 30);
            this.F6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F6.TabIndex = 186;
            this.F6.TabStop = false;
            this.F6.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // F5
            // 
            this.F5.BackColor = System.Drawing.Color.White;
            this.F5.Location = new System.Drawing.Point(231, 340);
            this.F5.Name = "F5";
            this.F5.Size = new System.Drawing.Size(30, 30);
            this.F5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F5.TabIndex = 185;
            this.F5.TabStop = false;
            this.F5.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // F10
            // 
            this.F10.BackColor = System.Drawing.Color.White;
            this.F10.Location = new System.Drawing.Point(411, 340);
            this.F10.Name = "F10";
            this.F10.Size = new System.Drawing.Size(30, 30);
            this.F10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F10.TabIndex = 184;
            this.F10.TabStop = false;
            this.F10.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // F4
            // 
            this.F4.BackColor = System.Drawing.Color.White;
            this.F4.Location = new System.Drawing.Point(195, 340);
            this.F4.Name = "F4";
            this.F4.Size = new System.Drawing.Size(30, 30);
            this.F4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F4.TabIndex = 183;
            this.F4.TabStop = false;
            this.F4.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // F3
            // 
            this.F3.BackColor = System.Drawing.Color.White;
            this.F3.Location = new System.Drawing.Point(159, 340);
            this.F3.Name = "F3";
            this.F3.Size = new System.Drawing.Size(30, 30);
            this.F3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F3.TabIndex = 182;
            this.F3.TabStop = false;
            this.F3.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // F2
            // 
            this.F2.BackColor = System.Drawing.Color.White;
            this.F2.Location = new System.Drawing.Point(123, 340);
            this.F2.Name = "F2";
            this.F2.Size = new System.Drawing.Size(30, 30);
            this.F2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F2.TabIndex = 181;
            this.F2.TabStop = false;
            this.F2.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // F1
            // 
            this.F1.BackColor = System.Drawing.Color.White;
            this.F1.Location = new System.Drawing.Point(87, 340);
            this.F1.Name = "F1";
            this.F1.Size = new System.Drawing.Size(30, 30);
            this.F1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F1.TabIndex = 180;
            this.F1.TabStop = false;
            this.F1.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // E9
            // 
            this.E9.BackColor = System.Drawing.Color.White;
            this.E9.Location = new System.Drawing.Point(375, 304);
            this.E9.Name = "E9";
            this.E9.Size = new System.Drawing.Size(30, 30);
            this.E9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E9.TabIndex = 179;
            this.E9.TabStop = false;
            this.E9.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // E8
            // 
            this.E8.BackColor = System.Drawing.Color.White;
            this.E8.Location = new System.Drawing.Point(339, 304);
            this.E8.Name = "E8";
            this.E8.Size = new System.Drawing.Size(30, 30);
            this.E8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E8.TabIndex = 178;
            this.E8.TabStop = false;
            this.E8.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // E7
            // 
            this.E7.BackColor = System.Drawing.Color.White;
            this.E7.Location = new System.Drawing.Point(303, 304);
            this.E7.Name = "E7";
            this.E7.Size = new System.Drawing.Size(30, 30);
            this.E7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E7.TabIndex = 177;
            this.E7.TabStop = false;
            this.E7.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // E6
            // 
            this.E6.BackColor = System.Drawing.Color.White;
            this.E6.Location = new System.Drawing.Point(267, 304);
            this.E6.Name = "E6";
            this.E6.Size = new System.Drawing.Size(30, 30);
            this.E6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E6.TabIndex = 176;
            this.E6.TabStop = false;
            this.E6.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // E5
            // 
            this.E5.BackColor = System.Drawing.Color.White;
            this.E5.Location = new System.Drawing.Point(231, 304);
            this.E5.Name = "E5";
            this.E5.Size = new System.Drawing.Size(30, 30);
            this.E5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E5.TabIndex = 175;
            this.E5.TabStop = false;
            this.E5.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // E10
            // 
            this.E10.BackColor = System.Drawing.Color.White;
            this.E10.Location = new System.Drawing.Point(411, 304);
            this.E10.Name = "E10";
            this.E10.Size = new System.Drawing.Size(30, 30);
            this.E10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E10.TabIndex = 174;
            this.E10.TabStop = false;
            this.E10.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // E4
            // 
            this.E4.BackColor = System.Drawing.Color.White;
            this.E4.Location = new System.Drawing.Point(195, 304);
            this.E4.Name = "E4";
            this.E4.Size = new System.Drawing.Size(30, 30);
            this.E4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E4.TabIndex = 173;
            this.E4.TabStop = false;
            this.E4.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // E3
            // 
            this.E3.BackColor = System.Drawing.Color.White;
            this.E3.Location = new System.Drawing.Point(159, 304);
            this.E3.Name = "E3";
            this.E3.Size = new System.Drawing.Size(30, 30);
            this.E3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E3.TabIndex = 172;
            this.E3.TabStop = false;
            this.E3.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // E2
            // 
            this.E2.BackColor = System.Drawing.Color.White;
            this.E2.Location = new System.Drawing.Point(123, 304);
            this.E2.Name = "E2";
            this.E2.Size = new System.Drawing.Size(30, 30);
            this.E2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E2.TabIndex = 171;
            this.E2.TabStop = false;
            this.E2.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // E1
            // 
            this.E1.BackColor = System.Drawing.Color.White;
            this.E1.Location = new System.Drawing.Point(87, 304);
            this.E1.Name = "E1";
            this.E1.Size = new System.Drawing.Size(30, 30);
            this.E1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E1.TabIndex = 170;
            this.E1.TabStop = false;
            this.E1.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // pG10
            // 
            this.pG10.BackColor = System.Drawing.Color.White;
            this.pG10.Location = new System.Drawing.Point(701, 414);
            this.pG10.Name = "pG10";
            this.pG10.Size = new System.Drawing.Size(20, 20);
            this.pG10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pG10.TabIndex = 169;
            this.pG10.TabStop = false;
            // 
            // pG9
            // 
            this.pG9.BackColor = System.Drawing.Color.White;
            this.pG9.Location = new System.Drawing.Point(675, 414);
            this.pG9.Name = "pG9";
            this.pG9.Size = new System.Drawing.Size(20, 20);
            this.pG9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pG9.TabIndex = 168;
            this.pG9.TabStop = false;
            // 
            // pG8
            // 
            this.pG8.BackColor = System.Drawing.Color.White;
            this.pG8.Location = new System.Drawing.Point(649, 414);
            this.pG8.Name = "pG8";
            this.pG8.Size = new System.Drawing.Size(20, 20);
            this.pG8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pG8.TabIndex = 167;
            this.pG8.TabStop = false;
            // 
            // pG7
            // 
            this.pG7.BackColor = System.Drawing.Color.White;
            this.pG7.Location = new System.Drawing.Point(623, 414);
            this.pG7.Name = "pG7";
            this.pG7.Size = new System.Drawing.Size(20, 20);
            this.pG7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pG7.TabIndex = 166;
            this.pG7.TabStop = false;
            // 
            // pG6
            // 
            this.pG6.BackColor = System.Drawing.Color.White;
            this.pG6.Location = new System.Drawing.Point(597, 414);
            this.pG6.Name = "pG6";
            this.pG6.Size = new System.Drawing.Size(20, 20);
            this.pG6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pG6.TabIndex = 165;
            this.pG6.TabStop = false;
            // 
            // pG5
            // 
            this.pG5.BackColor = System.Drawing.Color.White;
            this.pG5.Location = new System.Drawing.Point(571, 414);
            this.pG5.Name = "pG5";
            this.pG5.Size = new System.Drawing.Size(20, 20);
            this.pG5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pG5.TabIndex = 164;
            this.pG5.TabStop = false;
            // 
            // pG4
            // 
            this.pG4.BackColor = System.Drawing.Color.White;
            this.pG4.Location = new System.Drawing.Point(545, 414);
            this.pG4.Name = "pG4";
            this.pG4.Size = new System.Drawing.Size(20, 20);
            this.pG4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pG4.TabIndex = 163;
            this.pG4.TabStop = false;
            // 
            // pG3
            // 
            this.pG3.BackColor = System.Drawing.Color.White;
            this.pG3.Location = new System.Drawing.Point(519, 414);
            this.pG3.Name = "pG3";
            this.pG3.Size = new System.Drawing.Size(20, 20);
            this.pG3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pG3.TabIndex = 162;
            this.pG3.TabStop = false;
            // 
            // pG2
            // 
            this.pG2.BackColor = System.Drawing.Color.White;
            this.pG2.Location = new System.Drawing.Point(493, 414);
            this.pG2.Name = "pG2";
            this.pG2.Size = new System.Drawing.Size(20, 20);
            this.pG2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pG2.TabIndex = 161;
            this.pG2.TabStop = false;
            // 
            // pG1
            // 
            this.pG1.BackColor = System.Drawing.Color.White;
            this.pG1.Location = new System.Drawing.Point(467, 414);
            this.pG1.Name = "pG1";
            this.pG1.Size = new System.Drawing.Size(20, 20);
            this.pG1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pG1.TabIndex = 160;
            this.pG1.TabStop = false;
            // 
            // D9
            // 
            this.D9.BackColor = System.Drawing.Color.White;
            this.D9.Location = new System.Drawing.Point(375, 268);
            this.D9.Name = "D9";
            this.D9.Size = new System.Drawing.Size(30, 30);
            this.D9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D9.TabIndex = 9;
            this.D9.TabStop = false;
            this.D9.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // D8
            // 
            this.D8.BackColor = System.Drawing.Color.White;
            this.D8.Location = new System.Drawing.Point(339, 268);
            this.D8.Name = "D8";
            this.D8.Size = new System.Drawing.Size(30, 30);
            this.D8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D8.TabIndex = 8;
            this.D8.TabStop = false;
            this.D8.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // D7
            // 
            this.D7.BackColor = System.Drawing.Color.White;
            this.D7.Location = new System.Drawing.Point(303, 268);
            this.D7.Name = "D7";
            this.D7.Size = new System.Drawing.Size(30, 30);
            this.D7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D7.TabIndex = 7;
            this.D7.TabStop = false;
            this.D7.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // D6
            // 
            this.D6.BackColor = System.Drawing.Color.White;
            this.D6.Location = new System.Drawing.Point(267, 268);
            this.D6.Name = "D6";
            this.D6.Size = new System.Drawing.Size(30, 30);
            this.D6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D6.TabIndex = 6;
            this.D6.TabStop = false;
            this.D6.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // D5
            // 
            this.D5.BackColor = System.Drawing.Color.White;
            this.D5.Location = new System.Drawing.Point(231, 268);
            this.D5.Name = "D5";
            this.D5.Size = new System.Drawing.Size(30, 30);
            this.D5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D5.TabIndex = 5;
            this.D5.TabStop = false;
            this.D5.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // D10
            // 
            this.D10.BackColor = System.Drawing.Color.White;
            this.D10.Location = new System.Drawing.Point(411, 268);
            this.D10.Name = "D10";
            this.D10.Size = new System.Drawing.Size(30, 30);
            this.D10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D10.TabIndex = 4;
            this.D10.TabStop = false;
            this.D10.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // D4
            // 
            this.D4.BackColor = System.Drawing.Color.White;
            this.D4.Location = new System.Drawing.Point(195, 268);
            this.D4.Name = "D4";
            this.D4.Size = new System.Drawing.Size(30, 30);
            this.D4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D4.TabIndex = 3;
            this.D4.TabStop = false;
            this.D4.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // D3
            // 
            this.D3.BackColor = System.Drawing.Color.White;
            this.D3.Location = new System.Drawing.Point(159, 268);
            this.D3.Name = "D3";
            this.D3.Size = new System.Drawing.Size(30, 30);
            this.D3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D3.TabIndex = 2;
            this.D3.TabStop = false;
            this.D3.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // D2
            // 
            this.D2.BackColor = System.Drawing.Color.White;
            this.D2.Location = new System.Drawing.Point(123, 268);
            this.D2.Name = "D2";
            this.D2.Size = new System.Drawing.Size(30, 30);
            this.D2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D2.TabIndex = 1;
            this.D2.TabStop = false;
            this.D2.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // D1
            // 
            this.D1.BackColor = System.Drawing.Color.White;
            this.D1.Location = new System.Drawing.Point(87, 268);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(30, 30);
            this.D1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D1.TabIndex = 0;
            this.D1.TabStop = false;
            this.D1.Click += new System.EventHandler(this.PicboxClicked);
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Black;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label11.Location = new System.Drawing.Point(76, 152);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(375, 371);
            this.label11.TabIndex = 366;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(87, 120);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 30);
            this.label13.TabIndex = 367;
            this.label13.Text = "1";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(404, 120);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 30);
            this.label14.TabIndex = 368;
            this.label14.Text = "10";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(375, 120);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 30);
            this.label15.TabIndex = 369;
            this.label15.Text = "9";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(339, 120);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(30, 30);
            this.label16.TabIndex = 370;
            this.label16.Text = "8";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(303, 120);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(30, 30);
            this.label17.TabIndex = 371;
            this.label17.Text = "7";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(267, 120);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(30, 30);
            this.label18.TabIndex = 372;
            this.label18.Text = "6";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(231, 120);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(30, 30);
            this.label19.TabIndex = 373;
            this.label19.Text = "5";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(195, 120);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(30, 30);
            this.label20.TabIndex = 374;
            this.label20.Text = "4";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(159, 120);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(30, 30);
            this.label21.TabIndex = 375;
            this.label21.Text = "3";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(123, 120);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(30, 30);
            this.label22.TabIndex = 376;
            this.label22.Text = "2";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(45, 160);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(30, 30);
            this.label23.TabIndex = 377;
            this.label23.Text = "A";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(45, 484);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(30, 30);
            this.label24.TabIndex = 378;
            this.label24.Text = "J";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(45, 448);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(30, 30);
            this.label25.TabIndex = 379;
            this.label25.Text = "I";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(45, 412);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(30, 30);
            this.label26.TabIndex = 380;
            this.label26.Text = "H";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(45, 376);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(30, 30);
            this.label27.TabIndex = 381;
            this.label27.Text = "G";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(45, 340);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(30, 30);
            this.label28.TabIndex = 382;
            this.label28.Text = "F";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(45, 304);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(30, 30);
            this.label29.TabIndex = 383;
            this.label29.Text = "E";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(45, 268);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(30, 30);
            this.label30.TabIndex = 384;
            this.label30.Text = "D";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(45, 232);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(30, 30);
            this.label31.TabIndex = 385;
            this.label31.Text = "C";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(45, 196);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(30, 30);
            this.label32.TabIndex = 386;
            this.label32.Text = "B";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picVolume
            // 
            this.picVolume.Image = global::Battleships.Properties.Resources.VolumeOn;
            this.picVolume.Location = new System.Drawing.Point(746, 699);
            this.picVolume.Name = "picVolume";
            this.picVolume.Size = new System.Drawing.Size(50, 50);
            this.picVolume.TabIndex = 8;
            this.picVolume.TabStop = false;
            this.picVolume.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picVolume_MouseDown);
            this.picVolume.MouseEnter += new System.EventHandler(this.picVolume_MouseEnter);
            this.picVolume.MouseLeave += new System.EventHandler(this.picVolume_MouseLeave);
            this.picVolume.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picVolume_MouseUp);
            // 
            // btnHit
            // 
            this.btnHit.BackColor = System.Drawing.Color.Silver;
            this.btnHit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnHit.Location = new System.Drawing.Point(493, 152);
            this.btnHit.Name = "btnHit";
            this.btnHit.Size = new System.Drawing.Size(202, 23);
            this.btnHit.TabIndex = 387;
            this.btnHit.Text = "Fire";
            this.btnHit.UseVisualStyleBackColor = false;
            this.btnHit.Click += new System.EventHandler(this.btn_hit_Click);
            // 
            // Battleships
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.BackgroundImage = global::Battleships.Properties.Resources.background;
            this.ClientSize = new System.Drawing.Size(808, 761);
            this.Controls.Add(this.btnHit);
            this.Controls.Add(this.picVolume);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btnSonar);
            this.Controls.Add(this.btnMove);
            this.Controls.Add(this.lblPlayer);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pJ10);
            this.Controls.Add(this.pJ9);
            this.Controls.Add(this.pJ8);
            this.Controls.Add(this.pJ7);
            this.Controls.Add(this.pJ6);
            this.Controls.Add(this.pJ5);
            this.Controls.Add(this.pJ4);
            this.Controls.Add(this.pJ3);
            this.Controls.Add(this.pJ2);
            this.Controls.Add(this.pJ1);
            this.Controls.Add(this.pC10);
            this.Controls.Add(this.pC9);
            this.Controls.Add(this.pC8);
            this.Controls.Add(this.pC7);
            this.Controls.Add(this.pC6);
            this.Controls.Add(this.pC5);
            this.Controls.Add(this.pC4);
            this.Controls.Add(this.pC3);
            this.Controls.Add(this.pC2);
            this.Controls.Add(this.pC1);
            this.Controls.Add(this.pB10);
            this.Controls.Add(this.pB9);
            this.Controls.Add(this.pB8);
            this.Controls.Add(this.pB7);
            this.Controls.Add(this.pB6);
            this.Controls.Add(this.pB5);
            this.Controls.Add(this.pB4);
            this.Controls.Add(this.pB3);
            this.Controls.Add(this.pB2);
            this.Controls.Add(this.pB1);
            this.Controls.Add(this.pA10);
            this.Controls.Add(this.pA9);
            this.Controls.Add(this.pA8);
            this.Controls.Add(this.pA7);
            this.Controls.Add(this.pA6);
            this.Controls.Add(this.pA5);
            this.Controls.Add(this.pA4);
            this.Controls.Add(this.pA3);
            this.Controls.Add(this.pA2);
            this.Controls.Add(this.pA1);
            this.Controls.Add(this.pF10);
            this.Controls.Add(this.pF9);
            this.Controls.Add(this.pF8);
            this.Controls.Add(this.pF7);
            this.Controls.Add(this.pF6);
            this.Controls.Add(this.pF5);
            this.Controls.Add(this.pF4);
            this.Controls.Add(this.pF3);
            this.Controls.Add(this.pF2);
            this.Controls.Add(this.pF1);
            this.Controls.Add(this.pE10);
            this.Controls.Add(this.pE9);
            this.Controls.Add(this.pE8);
            this.Controls.Add(this.pE7);
            this.Controls.Add(this.pE6);
            this.Controls.Add(this.pE5);
            this.Controls.Add(this.pE4);
            this.Controls.Add(this.pE3);
            this.Controls.Add(this.pE2);
            this.Controls.Add(this.pE1);
            this.Controls.Add(this.pD10);
            this.Controls.Add(this.pD9);
            this.Controls.Add(this.pD8);
            this.Controls.Add(this.pD7);
            this.Controls.Add(this.pD6);
            this.Controls.Add(this.pD5);
            this.Controls.Add(this.pD4);
            this.Controls.Add(this.pD3);
            this.Controls.Add(this.pD2);
            this.Controls.Add(this.pD1);
            this.Controls.Add(this.pI10);
            this.Controls.Add(this.pI9);
            this.Controls.Add(this.pI8);
            this.Controls.Add(this.pI7);
            this.Controls.Add(this.pI6);
            this.Controls.Add(this.pI5);
            this.Controls.Add(this.pI4);
            this.Controls.Add(this.pI3);
            this.Controls.Add(this.pI2);
            this.Controls.Add(this.pI1);
            this.Controls.Add(this.pH10);
            this.Controls.Add(this.pH9);
            this.Controls.Add(this.pH8);
            this.Controls.Add(this.pH7);
            this.Controls.Add(this.pH6);
            this.Controls.Add(this.pH5);
            this.Controls.Add(this.pH4);
            this.Controls.Add(this.pH3);
            this.Controls.Add(this.pH2);
            this.Controls.Add(this.pH1);
            this.Controls.Add(this.A9);
            this.Controls.Add(this.A8);
            this.Controls.Add(this.A7);
            this.Controls.Add(this.A6);
            this.Controls.Add(this.A5);
            this.Controls.Add(this.A10);
            this.Controls.Add(this.A4);
            this.Controls.Add(this.A3);
            this.Controls.Add(this.A2);
            this.Controls.Add(this.A1);
            this.Controls.Add(this.B9);
            this.Controls.Add(this.B8);
            this.Controls.Add(this.B7);
            this.Controls.Add(this.B6);
            this.Controls.Add(this.B5);
            this.Controls.Add(this.B10);
            this.Controls.Add(this.B4);
            this.Controls.Add(this.B3);
            this.Controls.Add(this.B2);
            this.Controls.Add(this.B1);
            this.Controls.Add(this.J9);
            this.Controls.Add(this.J8);
            this.Controls.Add(this.J7);
            this.Controls.Add(this.J6);
            this.Controls.Add(this.J5);
            this.Controls.Add(this.J10);
            this.Controls.Add(this.J4);
            this.Controls.Add(this.J3);
            this.Controls.Add(this.J2);
            this.Controls.Add(this.J1);
            this.Controls.Add(this.C9);
            this.Controls.Add(this.C8);
            this.Controls.Add(this.C7);
            this.Controls.Add(this.C6);
            this.Controls.Add(this.C5);
            this.Controls.Add(this.C10);
            this.Controls.Add(this.C4);
            this.Controls.Add(this.C3);
            this.Controls.Add(this.C2);
            this.Controls.Add(this.C1);
            this.Controls.Add(this.I9);
            this.Controls.Add(this.I8);
            this.Controls.Add(this.I7);
            this.Controls.Add(this.I6);
            this.Controls.Add(this.I5);
            this.Controls.Add(this.I10);
            this.Controls.Add(this.I4);
            this.Controls.Add(this.I3);
            this.Controls.Add(this.I2);
            this.Controls.Add(this.I1);
            this.Controls.Add(this.H9);
            this.Controls.Add(this.H8);
            this.Controls.Add(this.H7);
            this.Controls.Add(this.H6);
            this.Controls.Add(this.H5);
            this.Controls.Add(this.H10);
            this.Controls.Add(this.H4);
            this.Controls.Add(this.H3);
            this.Controls.Add(this.H2);
            this.Controls.Add(this.H1);
            this.Controls.Add(this.G9);
            this.Controls.Add(this.G8);
            this.Controls.Add(this.G7);
            this.Controls.Add(this.G6);
            this.Controls.Add(this.G5);
            this.Controls.Add(this.G10);
            this.Controls.Add(this.G4);
            this.Controls.Add(this.G3);
            this.Controls.Add(this.G2);
            this.Controls.Add(this.G1);
            this.Controls.Add(this.F9);
            this.Controls.Add(this.F8);
            this.Controls.Add(this.F7);
            this.Controls.Add(this.F6);
            this.Controls.Add(this.F5);
            this.Controls.Add(this.F10);
            this.Controls.Add(this.F4);
            this.Controls.Add(this.F3);
            this.Controls.Add(this.F2);
            this.Controls.Add(this.F1);
            this.Controls.Add(this.E9);
            this.Controls.Add(this.E8);
            this.Controls.Add(this.E7);
            this.Controls.Add(this.E6);
            this.Controls.Add(this.E5);
            this.Controls.Add(this.E10);
            this.Controls.Add(this.E4);
            this.Controls.Add(this.E3);
            this.Controls.Add(this.E2);
            this.Controls.Add(this.E1);
            this.Controls.Add(this.pG10);
            this.Controls.Add(this.pG9);
            this.Controls.Add(this.pG8);
            this.Controls.Add(this.pG7);
            this.Controls.Add(this.pG6);
            this.Controls.Add(this.pG5);
            this.Controls.Add(this.pG4);
            this.Controls.Add(this.pG3);
            this.Controls.Add(this.pG2);
            this.Controls.Add(this.pG1);
            this.Controls.Add(this.D9);
            this.Controls.Add(this.D8);
            this.Controls.Add(this.D7);
            this.Controls.Add(this.D6);
            this.Controls.Add(this.D5);
            this.Controls.Add(this.D10);
            this.Controls.Add(this.D4);
            this.Controls.Add(this.D3);
            this.Controls.Add(this.D2);
            this.Controls.Add(this.D1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Name = "Battleships";
            this.Text = "Battleships";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pJ10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJ1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pA1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pF1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pE1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pD1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pH1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pG1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVolume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox D1;
        private System.Windows.Forms.PictureBox D2;
        private System.Windows.Forms.PictureBox D3;
        private System.Windows.Forms.PictureBox D4;
        private System.Windows.Forms.PictureBox D10;
        private System.Windows.Forms.PictureBox D5;
        private System.Windows.Forms.PictureBox D6;
        private System.Windows.Forms.PictureBox D7;
        private System.Windows.Forms.PictureBox D8;
        private System.Windows.Forms.PictureBox D9;
        private System.Windows.Forms.PictureBox pG10;
        private System.Windows.Forms.PictureBox pG9;
        private System.Windows.Forms.PictureBox pG8;
        private System.Windows.Forms.PictureBox pG7;
        private System.Windows.Forms.PictureBox pG6;
        private System.Windows.Forms.PictureBox pG5;
        private System.Windows.Forms.PictureBox pG4;
        private System.Windows.Forms.PictureBox pG3;
        private System.Windows.Forms.PictureBox pG2;
        private System.Windows.Forms.PictureBox pG1;
        private System.Windows.Forms.PictureBox E9;
        private System.Windows.Forms.PictureBox E8;
        private System.Windows.Forms.PictureBox E7;
        private System.Windows.Forms.PictureBox E6;
        private System.Windows.Forms.PictureBox E5;
        private System.Windows.Forms.PictureBox E10;
        private System.Windows.Forms.PictureBox E4;
        private System.Windows.Forms.PictureBox E3;
        private System.Windows.Forms.PictureBox E2;
        private System.Windows.Forms.PictureBox E1;
        private System.Windows.Forms.PictureBox F9;
        private System.Windows.Forms.PictureBox F8;
        private System.Windows.Forms.PictureBox F7;
        private System.Windows.Forms.PictureBox F6;
        private System.Windows.Forms.PictureBox F5;
        private System.Windows.Forms.PictureBox F10;
        private System.Windows.Forms.PictureBox F4;
        private System.Windows.Forms.PictureBox F3;
        private System.Windows.Forms.PictureBox F2;
        private System.Windows.Forms.PictureBox F1;
        private System.Windows.Forms.PictureBox G9;
        private System.Windows.Forms.PictureBox G8;
        private System.Windows.Forms.PictureBox G7;
        private System.Windows.Forms.PictureBox G6;
        private System.Windows.Forms.PictureBox G5;
        private System.Windows.Forms.PictureBox G10;
        private System.Windows.Forms.PictureBox G4;
        private System.Windows.Forms.PictureBox G3;
        private System.Windows.Forms.PictureBox G2;
        private System.Windows.Forms.PictureBox G1;
        private System.Windows.Forms.PictureBox H9;
        private System.Windows.Forms.PictureBox H8;
        private System.Windows.Forms.PictureBox H7;
        private System.Windows.Forms.PictureBox H6;
        private System.Windows.Forms.PictureBox H5;
        private System.Windows.Forms.PictureBox H10;
        private System.Windows.Forms.PictureBox H4;
        private System.Windows.Forms.PictureBox H3;
        private System.Windows.Forms.PictureBox H2;
        private System.Windows.Forms.PictureBox H1;
        private System.Windows.Forms.PictureBox I9;
        private System.Windows.Forms.PictureBox I8;
        private System.Windows.Forms.PictureBox I7;
        private System.Windows.Forms.PictureBox I6;
        private System.Windows.Forms.PictureBox I5;
        private System.Windows.Forms.PictureBox I10;
        private System.Windows.Forms.PictureBox I4;
        private System.Windows.Forms.PictureBox I3;
        private System.Windows.Forms.PictureBox I2;
        private System.Windows.Forms.PictureBox I1;
        private System.Windows.Forms.PictureBox C9;
        private System.Windows.Forms.PictureBox C8;
        private System.Windows.Forms.PictureBox C7;
        private System.Windows.Forms.PictureBox C6;
        private System.Windows.Forms.PictureBox C5;
        private System.Windows.Forms.PictureBox C10;
        private System.Windows.Forms.PictureBox C4;
        private System.Windows.Forms.PictureBox C3;
        private System.Windows.Forms.PictureBox C2;
        private System.Windows.Forms.PictureBox C1;
        private System.Windows.Forms.PictureBox J9;
        private System.Windows.Forms.PictureBox J8;
        private System.Windows.Forms.PictureBox J7;
        private System.Windows.Forms.PictureBox J6;
        private System.Windows.Forms.PictureBox J5;
        private System.Windows.Forms.PictureBox J10;
        private System.Windows.Forms.PictureBox J4;
        private System.Windows.Forms.PictureBox J3;
        private System.Windows.Forms.PictureBox J2;
        private System.Windows.Forms.PictureBox J1;
        private System.Windows.Forms.PictureBox B9;
        private System.Windows.Forms.PictureBox B8;
        private System.Windows.Forms.PictureBox B7;
        private System.Windows.Forms.PictureBox B6;
        private System.Windows.Forms.PictureBox B5;
        private System.Windows.Forms.PictureBox B10;
        private System.Windows.Forms.PictureBox B4;
        private System.Windows.Forms.PictureBox B3;
        private System.Windows.Forms.PictureBox B2;
        private System.Windows.Forms.PictureBox B1;
        private System.Windows.Forms.PictureBox A9;
        private System.Windows.Forms.PictureBox A8;
        private System.Windows.Forms.PictureBox A7;
        private System.Windows.Forms.PictureBox A6;
        private System.Windows.Forms.PictureBox A5;
        private System.Windows.Forms.PictureBox A10;
        private System.Windows.Forms.PictureBox A4;
        private System.Windows.Forms.PictureBox A3;
        private System.Windows.Forms.PictureBox A2;
        private System.Windows.Forms.PictureBox A1;
        private System.Windows.Forms.PictureBox pH10;
        private System.Windows.Forms.PictureBox pH9;
        private System.Windows.Forms.PictureBox pH8;
        private System.Windows.Forms.PictureBox pH7;
        private System.Windows.Forms.PictureBox pH6;
        private System.Windows.Forms.PictureBox pH5;
        private System.Windows.Forms.PictureBox pH4;
        private System.Windows.Forms.PictureBox pH3;
        private System.Windows.Forms.PictureBox pH2;
        private System.Windows.Forms.PictureBox pH1;
        private System.Windows.Forms.PictureBox pI10;
        private System.Windows.Forms.PictureBox pI9;
        private System.Windows.Forms.PictureBox pI8;
        private System.Windows.Forms.PictureBox pI7;
        private System.Windows.Forms.PictureBox pI6;
        private System.Windows.Forms.PictureBox pI5;
        private System.Windows.Forms.PictureBox pI4;
        private System.Windows.Forms.PictureBox pI3;
        private System.Windows.Forms.PictureBox pI2;
        private System.Windows.Forms.PictureBox pI1;
        private System.Windows.Forms.PictureBox pF10;
        private System.Windows.Forms.PictureBox pF9;
        private System.Windows.Forms.PictureBox pF8;
        private System.Windows.Forms.PictureBox pF7;
        private System.Windows.Forms.PictureBox pF6;
        private System.Windows.Forms.PictureBox pF5;
        private System.Windows.Forms.PictureBox pF4;
        private System.Windows.Forms.PictureBox pF3;
        private System.Windows.Forms.PictureBox pF2;
        private System.Windows.Forms.PictureBox pF1;
        private System.Windows.Forms.PictureBox pE10;
        private System.Windows.Forms.PictureBox pE9;
        private System.Windows.Forms.PictureBox pE8;
        private System.Windows.Forms.PictureBox pE7;
        private System.Windows.Forms.PictureBox pE6;
        private System.Windows.Forms.PictureBox pE5;
        private System.Windows.Forms.PictureBox pE4;
        private System.Windows.Forms.PictureBox pE3;
        private System.Windows.Forms.PictureBox pE2;
        private System.Windows.Forms.PictureBox pE1;
        private System.Windows.Forms.PictureBox pD10;
        private System.Windows.Forms.PictureBox pD9;
        private System.Windows.Forms.PictureBox pD8;
        private System.Windows.Forms.PictureBox pD7;
        private System.Windows.Forms.PictureBox pD6;
        private System.Windows.Forms.PictureBox pD5;
        private System.Windows.Forms.PictureBox pD4;
        private System.Windows.Forms.PictureBox pD3;
        private System.Windows.Forms.PictureBox pD2;
        private System.Windows.Forms.PictureBox pD1;
        private System.Windows.Forms.PictureBox pC10;
        private System.Windows.Forms.PictureBox pC9;
        private System.Windows.Forms.PictureBox pC8;
        private System.Windows.Forms.PictureBox pC7;
        private System.Windows.Forms.PictureBox pC6;
        private System.Windows.Forms.PictureBox pC5;
        private System.Windows.Forms.PictureBox pC4;
        private System.Windows.Forms.PictureBox pC3;
        private System.Windows.Forms.PictureBox pC2;
        private System.Windows.Forms.PictureBox pC1;
        private System.Windows.Forms.PictureBox pB10;
        private System.Windows.Forms.PictureBox pB9;
        private System.Windows.Forms.PictureBox pB8;
        private System.Windows.Forms.PictureBox pB7;
        private System.Windows.Forms.PictureBox pB6;
        private System.Windows.Forms.PictureBox pB5;
        private System.Windows.Forms.PictureBox pB4;
        private System.Windows.Forms.PictureBox pB3;
        private System.Windows.Forms.PictureBox pB2;
        private System.Windows.Forms.PictureBox pB1;
        private System.Windows.Forms.PictureBox pA10;
        private System.Windows.Forms.PictureBox pA9;
        private System.Windows.Forms.PictureBox pA8;
        private System.Windows.Forms.PictureBox pA7;
        private System.Windows.Forms.PictureBox pA6;
        private System.Windows.Forms.PictureBox pA5;
        private System.Windows.Forms.PictureBox pA4;
        private System.Windows.Forms.PictureBox pA3;
        private System.Windows.Forms.PictureBox pA2;
        private System.Windows.Forms.PictureBox pA1;
        private System.Windows.Forms.PictureBox pJ10;
        private System.Windows.Forms.PictureBox pJ9;
        private System.Windows.Forms.PictureBox pJ8;
        private System.Windows.Forms.PictureBox pJ7;
        private System.Windows.Forms.PictureBox pJ6;
        private System.Windows.Forms.PictureBox pJ5;
        private System.Windows.Forms.PictureBox pJ4;
        private System.Windows.Forms.PictureBox pJ3;
        private System.Windows.Forms.PictureBox pJ2;
        private System.Windows.Forms.PictureBox pJ1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblPlayer;
        private System.Windows.Forms.Button btnMove;
        private System.Windows.Forms.Button btnSonar;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.PictureBox picVolume;
        private System.Windows.Forms.Button btnHit;
    }
}

