﻿namespace Battleships
{
    partial class AiDifficulty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.radTeagan = new System.Windows.Forms.RadioButton();
            this.radKaiya = new System.Windows.Forms.RadioButton();
            this.btnDamo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(40, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select AI Difficulty";
            // 
            // radTeagan
            // 
            this.radTeagan.AutoSize = true;
            this.radTeagan.Checked = true;
            this.radTeagan.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTeagan.Location = new System.Drawing.Point(90, 76);
            this.radTeagan.Name = "radTeagan";
            this.radTeagan.Size = new System.Drawing.Size(97, 35);
            this.radTeagan.TabIndex = 1;
            this.radTeagan.TabStop = true;
            this.radTeagan.Text = "Easy";
            this.radTeagan.UseVisualStyleBackColor = true;
            //this.radTeagan.CheckedChanged += new System.EventHandler(this.radTeagan_CheckedChanged);
            // 
            // radKaiya
            // 
            this.radKaiya.AutoSize = true;
            this.radKaiya.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radKaiya.Location = new System.Drawing.Point(92, 143);
            this.radKaiya.Name = "radKaiya";
            this.radKaiya.Size = new System.Drawing.Size(95, 35);
            this.radKaiya.TabIndex = 2;
            this.radKaiya.Text = "Hard";
            this.radKaiya.UseVisualStyleBackColor = true;
            //this.radKaiya.CheckedChanged += new System.EventHandler(this.radKaiya_CheckedChanged);
            // 
            // btnDamo
            // 
            this.btnDamo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDamo.Location = new System.Drawing.Point(75, 212);
            this.btnDamo.Name = "btnDamo";
            this.btnDamo.Size = new System.Drawing.Size(127, 39);
            this.btnDamo.TabIndex = 3;
            this.btnDamo.Text = "CONFIRM";
            this.btnDamo.UseVisualStyleBackColor = true;
            this.btnDamo.Click += new System.EventHandler(this.btnDamo_Click);
            // 
            // AiDifficulty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(291, 278);
            this.Controls.Add(this.btnDamo);
            this.Controls.Add(this.radKaiya);
            this.Controls.Add(this.radTeagan);
            this.Controls.Add(this.label1);
            this.Name = "AiDifficulty";
            this.Text = "AiDifficulty";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radTeagan;
        private System.Windows.Forms.RadioButton radKaiya;
        private System.Windows.Forms.Button btnDamo;
    }
}