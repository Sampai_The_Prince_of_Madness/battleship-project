﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships
{
    public partial class Lobby : Form
    {
        public bool boolIsAccepted;
        public bool boolIsDisconnected = false;
        public int intOpponentID;
        public int intPlayerID;
        int timeCounter = 0;
        Button Bob = new Button();
        GameHandler GameH = new GameHandler();
        System.Timers.Timer timer = new System.Timers.Timer(3000);

        public Lobby()
        {
            InitializeComponent();
            PopulateLobby();
            timer.AutoReset = true;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            bool boolInGame;
            string strOpponentName = "";

            boolInGame = GameH.GetInGame(intPlayerID);

            if (boolInGame == true)
            {
                timer.Stop();

                intOpponentID = GameH.GetPlayerIDByChallenge(intPlayerID);
                strOpponentName = GameH.GetPlayerByID(intOpponentID);

                ChallengePopup popup = new ChallengePopup(strOpponentName);
                popup.SetID(intPlayerID);

                if (popup.ShowDialog() == DialogResult.OK)
                {
                    boolIsAccepted = popup.boolIsAccept;
                }

                if (boolIsAccepted == true)
                {
                    GameH.PostPlayerIDIntoGame(intPlayerID);
                    intOpponentID = GameH.GetPlayerIDByChallenge(intPlayerID);
                    DialogResult = DialogResult.OK;
                }    
            }

            if (timeCounter == 60)
            {
                GameH.PutPlayerStatis(intPlayerID, false);
                boolIsDisconnected = true;
                DialogResult = DialogResult.OK;
            }

            timeCounter++;
        }

        //public List<int> IntList = new List<int>();
        private void btnAdd_Click(object sender, EventArgs e)
        {
            pnlUsers.Controls.Clear();

            PopulateLobby();
        }

        private void btnDeadd_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;

            GameH.PutInGameStatus(Int32.Parse(button.Tag.ToString()), true);
            GameH.PutPlayerIDInChallenge(Int32.Parse(button.Tag.ToString()), intPlayerID);

            intOpponentID = Int32.Parse(button.Tag.ToString());
            
            GameH.PostPlayerIDIntoGame(intPlayerID);
            timer.Stop();
            DialogResult = DialogResult.OK;
        }    
        
        public void SetPlayersID(int intPPlayerID)
        {
            intPlayerID = intPPlayerID;
        }

        public void PopulateLobby()
        {
            Bob = new Button();

            List<string> strList = new List<string>();
            List<int> intList = new List<int>();
            Dictionary<int, string> dic = GameH.GetPlayerByOnline();

            intList = dic.Keys.ToList();


            for (int c = 0; c < intList.Count; c++)
            {
                if (intList[c] != intPlayerID)
                {
                    Bob = new Button();
                    Bob.Tag = intList[c];
                    Bob.Text = dic[intList[c]];
                    Bob.Click += new EventHandler(btnDeadd_Click);
                    Bob.Width = 190;
                    Bob.Height = 35;
                    pnlUsers.Controls.Add(Bob);
                }
            }
        }
    }
}
