﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships
{
    public class Ship
    {
        //Assigns all required variables

        public int intShipId;
        public int intShipSize;
        public int intShipHits;

        public List<string> strShipPos;
        public string strName;
        public bool boolShipStat;

        //Creates a new Ship to given specifications

        public Ship(int pShipId, List<string> pShipPos, int pShipSize, string pShipName)
        {
            intShipId = pShipId;
            strShipPos = pShipPos;
            intShipSize = pShipSize;
            strName = pShipName;
            boolShipStat = true;
            intShipHits = 0;
        }

        //If a ship is struck this adds to the counter of hits the ship has taken

        public void IsHit()
        {
            intShipHits++;
        }

        //If a ship accumulates as manys hits as equal to the ship's length the ship will sned a return to confirm it has been sunk

        public bool CalcSink()
        {
            if (intShipHits == intShipSize)
            {
                boolShipStat = false;
            }

            return boolShipStat;
        }
    }
}
