﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships
{
    public partial class PlaceShip : Form
    {
        GameHandler gameH = new GameHandler();
        public List<int> intPlayerIDs = new List<int>();
        public int intShipIDGen = 0;
        public int sorryNotSorry;

        //Placeship constructor
        public PlaceShip(string strPlayerName, string difficultyMode)
        {
            InitializeComponent();
            SetPlayerName(strPlayerName);
            btnCancel.Visible = false;
            this.WindowState = FormWindowState.Normal;

            if (difficultyMode != null)
            {
                lblDiffy.Visible = true;
                lblDifficulty.Visible = true;
                lblDifficulty.Text = difficultyMode;
            }
            else
            {
                lblDiffy.Visible = false;
                lblDifficulty.Visible = false;
            }
        }

        //Sets the player name
        public void SetPlayerName(String pPlayerName)
        {
            lblName.Text = pPlayerName;
        }
        
        //Sets the players IDs
        public void SetIds(List<int> pPlayerIDs)
        {
            intPlayerIDs = pPlayerIDs;
        }

        public int intShipSize = 0;
        public string strShipSelected;
        public string strShipName;
        public string strShipOutput;
        public bool boolShowShip;
        public bool boolColOrRow = true;
        public bool boolSetPos = false;
        public bool boolAllDown = false;
        public List<string> strListShipsSet = new List<string>();
        public List<string> strListShipDisplay = new List<string>();
        public List<Ship> shipListOutput = new List<Ship>();
        public Dictionary<string, int> dicPlacedShips = new Dictionary<string, int>();

        //Event handler for ship size of 5
        private void shipSize5_Click(object sender, EventArgs e)
        {
            PictureBox picBox = sender as PictureBox;
            ShipSizesClicked(sender, 1);
        }

        //Event handler for ship size of 4
        private void shipSize4_Click(object sender, EventArgs e)
        {
            PictureBox picBox = sender as PictureBox;
            ShipSizesClicked(sender, 2);
        }

        //Event handler for ship size of 3 (A)
        private void shipSize3_1_Click(object sender, EventArgs e)
        {
            PictureBox picBox = sender as PictureBox;
            ShipSizesClicked(sender, 3);
        }

        //Event handler for ship size of 3 (B)
        private void shipSize3_2_Click(object sender, EventArgs e)
        {
            PictureBox picBox = sender as PictureBox;
            ShipSizesClicked(sender, 4);
        }

        //Event handler for ship size of 2
        private void shipSize2_Click(object sender, EventArgs e)
        {
            PictureBox picBox = sender as PictureBox;
            ShipSizesClicked(sender, 5);
        }

        //Run when a ship size is clicked 
        private void ShipSizesClicked(object sender, int intShipSelected)
        {
            PictureBox picBox = sender as PictureBox;
            picBox.Visible = false;

            switch(intShipSelected)
            {
                //If "1", set the ship selected as the carrier
                case 1:
                    intShipSize = 5;
                    strShipSelected = "5";
                    intShipIDGen = intPlayerIDs[0];
                    RemoveShipHandlers();
                    strShipName = "CARRIER";   
                    break;

                //If "2", set the ship selected as the battleship
                case 2:
                    intShipSize = 4;
                    strShipSelected = "4";
                    intShipIDGen = intPlayerIDs[1];
                    RemoveShipHandlers();
                    strShipName = "BATTLESHIP";
                    break;

                //If "3", set the ship selected as the crusier
                case 3:
                    intShipSize = 3;
                    strShipSelected = "3A";
                    intShipIDGen = intPlayerIDs[2];
                    RemoveShipHandlers();
                    strShipName = "CRUSIER";
                    break;

                //If "4", set the ship selected as the submarine
                case 4:
                    intShipSize = 3;
                    strShipSelected = "3B";
                    intShipIDGen = intPlayerIDs[3];
                    RemoveShipHandlers();
                    strShipName = "SUBMARINE";
                    break;

                //If "5", set the ship selected as the destoyer
                case 5:
                    intShipSize = 2;
                    strShipSelected = "2";
                    intShipIDGen = intPlayerIDs[4];
                    RemoveShipHandlers();
                    strShipName = "DESTROYER";
                    break;
            }

            lblShipName.Text = strShipName;
        }

        //Sets the selected ship name based on the number inputted
        public void SetShipName(string setShipName)
        {
            setShipName = intShipIDGen.ToString();

            if (setShipName == "1")
            {
                lblShipName.Text = "CARRIER";
            }
            else if (setShipName == "2")
            {
                lblShipName.Text = "BATTLESHIP";
            }
            else if (setShipName == "3")
            {
                lblShipName.Text = "CRUSIER";
            }
            else if (setShipName == "4")
            {
                lblShipName.Text = "SUBMARINE";
            }
            else if (setShipName == "5")
            {
                lblShipName.Text = "DESTROYER";
            }
        }

        //Event handler for the mouse entering into a square.
        private void ShowShip(object sender, EventArgs e)
        {
            if (boolShowShip == true)
            {
                HideNPlaceShips(sender, "place");
            }
        }

        //Event handler for the mouse leaving into a square.
        private void HideShip(object sender, EventArgs e)
        {
            HideNPlaceShips(sender, "hide");
        }

        //Switches between true/false based on which it was last.
        private void btnRotate_Click(object sender, EventArgs e)
        {
            if (boolColOrRow == true)
            {
                boolColOrRow = false;
            }

            else
            {
                boolColOrRow = true;
            }
        }

        //Event handler for when a picture box has been clicked
        private void PicBoxClicked(object sender, EventArgs e)
        {
            PictureBox picBox = sender as PictureBox;

            //If a ship size has been selected.
            if (boolShowShip == true)
            {
                strListShipDisplay = gameH.CanThereBeShip(picBox.Name, intShipSize, boolColOrRow);

                PlaceShips(picBox);
            }

            //If the space already has a ship in it
            else if (dicPlacedShips.ContainsKey(picBox.Name))
            {
                bool boolRemover = false;
                int intIDHolder = Int32.Parse(picBox.Tag.ToString());
                int intShipLength = 0;
                PictureBox pb = (PictureBox)this.Controls.Find(strListShipDisplay[0], true)[0];

                //Removes the ship that was clicked 
                while (boolRemover == false)
                {
                    if (dicPlacedShips.ContainsValue(intIDHolder))
                    {
                        for (int c = 0; c < strListShipsSet.Count; c++)
                        {
                            if (dicPlacedShips[strListShipsSet[c]] == intIDHolder)
                            {
                                pb = (PictureBox)this.Controls.Find(strListShipsSet[c], true)[0];
                                pb.Image = null;
                                intShipIDGen = dicPlacedShips[strListShipsSet[c]];
                                dicPlacedShips.Remove(strListShipsSet[c]);
                                strListShipsSet.Remove(strListShipsSet[c]);
                                intShipLength++;
                            }

                            SetShipName(intShipIDGen.ToString());
                        }
                    }

                    else
                    {
                        boolRemover = true;
                    }
                }

                intShipSize = intShipLength;

                //Special exections for setting the ShipIDGen
                if (intShipIDGen == 3)
                {
                    strShipSelected = "3A";
                }

                else if (intShipIDGen == 4)
                {
                    strShipSelected = "3B";
                }

                else
                {
                    strShipSelected = intShipLength.ToString();
                }

                RemoveShipHandlers();
            }
        }

        //Based on what the last ship size that was selected, brings back the option
        //when the cancel button is clicked. NOTE: The cancel button is set to be
        //NOT visible untill a ship is selected.
        private void btnCancel_Click(object sender, EventArgs e)
        {
            AddShipHandlers();

            if (strShipSelected == "Random")
            {
                ClearBoard();
                ShowShipChoice();
                btnConfirm.Visible = false;
                btnRandom.Visible = true;
            }

            else if (strShipSelected == "5")
            {
                shipSize5.Visible = true;
            }

            else if (strShipSelected == "4")
            {
                shipSize4.Visible = true;
            }

            else if (strShipSelected == "3A")
            {
                shipSize3_1.Visible = true;
            }

            else if (strShipSelected == "3B")
            {
                shipSize3_2.Visible = true;
            }

            else
            {
                shipSize2.Visible = true;
            }

            lblShipName.Text = "NONE";
        }

        //Event handler for when the random button is pressed
        private void btnRandom_Click(object sender, EventArgs e)
        {
            List<int> intTempList = new List<int>();
            PictureBox pb = (PictureBox)this.Controls.Find("A1", true)[0];

            //Adds ship sizes to a temperary list
            intTempList.Add(5);
            intTempList.Add(4);
            intTempList.Add(3);
            intTempList.Add(3);
            intTempList.Add(2);
            
            //Goes through this list and creates the ships in a random position
            for (int c = 0; c < intTempList.Count; c++)
            {
                strListShipDisplay = gameH.Random(intTempList[c]);

                intShipIDGen = intPlayerIDs[c];

                pb = (PictureBox)this.Controls.Find(strListShipDisplay[0], true)[0];

                PlaceShips(pb);
            }

            //Clean up after randoming
            strShipSelected = "Random";
            RemoveShipHandlers();
            boolShowShip = false;
            HideShipChoice();
            gameH.ClearTakenList();
            btnCancel.Visible = true;
        }

        //Event handler for the confirm button
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            List<string> strListTemp = new List<string>();

            //Creating the carrier ship with coordinates and ID
            strListTemp = dicPlacedShips.Where(p => p.Value == intPlayerIDs[0]).Select(p => p.Key).ToList();
            Ship carrier = new Ship(intPlayerIDs[0], strListTemp, 5, "carrier");
            shipListOutput.Add(carrier);

            //Creating the battleship ship with coordinates and ID
            strListTemp = dicPlacedShips.Where(p => p.Value == intPlayerIDs[1]).Select(p => p.Key).ToList();
            Ship battleship = new Ship(intPlayerIDs[1], strListTemp, 4, "battleship");
            shipListOutput.Add(battleship);

            //Creating the crusier ship with coordinates and ID
            strListTemp = dicPlacedShips.Where(p => p.Value == intPlayerIDs[2]).Select(p => p.Key).ToList();
            Ship crusier = new Ship(intPlayerIDs[2], strListTemp, 3, "crusier");
            shipListOutput.Add(crusier);

            //Creating the submarine ship with coordinates and ID
            strListTemp = dicPlacedShips.Where(p => p.Value == intPlayerIDs[3]).Select(p => p.Key).ToList();
            Ship submarine = new Ship(intPlayerIDs[3], strListTemp, 3, "submarine");
            shipListOutput.Add(submarine);

            //Creating the destroyer ship with coordinates and ID
            strListTemp = dicPlacedShips.Where(p => p.Value == intPlayerIDs[4]).Select(p => p.Key).ToList();
            Ship destroyer = new Ship(intPlayerIDs[4], strListTemp, 2, "destroyer");
            shipListOutput.Add(destroyer);

            SetStringShipOutput();

            DialogResult = DialogResult.OK;
        }

        private void SetStringShipOutput()
        {
            List<string> strListTemp = new List<string>();
            Ship shipTemp = new Ship(0, strListTemp, 0, "");

            for (int a = 0; a < shipListOutput.Count; a++)
            {
                shipTemp = shipListOutput[a];

                strListTemp = shipTemp.strShipPos;

                strShipOutput = strShipOutput + "(" + shipTemp.intShipId + "!";

                for (int b = 0; b < strListTemp.Count; b++)
                {
                    if (b == strListTemp.Count - 1)
                    {
                        strShipOutput = strShipOutput + strListTemp[b];
                    }

                    else
                    {
                        strShipOutput = strShipOutput + strListTemp[b] + ",";
                    }
                }

                strShipOutput = strShipOutput + "$" + shipTemp.intShipSize + "#" + shipTemp.strName + ")";
            }
        }

        //Run when the mouse enters or leaves the image 
        public void HideNPlaceShips(object sender, string strHideOrPlace)
        {
            //picBox represents the picture box which the mosue left.
            PictureBox picBox = sender as PictureBox;

            strListShipDisplay = gameH.CanThereBeShip(picBox.Name, intShipSize, boolColOrRow);

            PictureBox pb = (PictureBox)this.Controls.Find(strListShipDisplay[0], true)[0];

            //Goes through the List, finds their respective picture box and changes their picture to be "empty".
            for (int c = 0; c < strListShipDisplay.Count; c++)
            {
                pb = (PictureBox)this.Controls.Find(strListShipDisplay[c], true)[0];

                //Searches the dictionary of location of where ships have already
                //been placed to make sure they don't get errased.
                if (!dicPlacedShips.ContainsKey(strListShipDisplay[c]))
                {
                    //If it is entering the picture box
                    if (strHideOrPlace == "place")
                    {
                        pb.Image = global::Battleships.Properties.Resources.ShipPart;
                    }

                    //If it is leaving the picture box
                    else
                    {
                        pb.Image = null;
                        pb.Tag = "";
                    }  
                }

                else
                {
                    //If it is entering the picture box
                    if (strHideOrPlace == "place")
                    {
                        pb.Image = global::Battleships.Properties.Resources.BadShipPart;
                    }

                    //If it is leaving the picture box
                    else
                    {
                        pb.Image = global::Battleships.Properties.Resources.ShipPart;
                    }   
                }
            }
        }

        //Visual display the ships
        public void PlaceShips(PictureBox picBox)
        {
            bool boolIsTaken = false;
            //Checks to see if any of the places wanted are taken.
            for (int c = 0; c < strListShipDisplay.Count; c++)
            {
                //If  they are already taken, makes sure that bool is set to true.
                if (dicPlacedShips.ContainsKey(strListShipDisplay[c]))
                {
                    boolIsTaken = true;
                }
            }

            //And so if the spot is not taken, it is set in place, being placed in
            //the dictionary so their spots cannot be taken; resets some variables 
            //and restores Click events to the ship selection so that the process 
            //can begin again.
            if (boolIsTaken == false)
            {
                for (int c = 0; c < strListShipDisplay.Count; c++)
                {
                    dicPlacedShips.Add(strListShipDisplay[c], intShipIDGen);
                    strListShipsSet.Add(strListShipDisplay[c]);
                    picBox = (PictureBox)this.Controls.Find(strListShipDisplay[c], true)[0];
                    picBox.Image = global::Battleships.Properties.Resources.ShipPart;
                    picBox.Tag = intShipIDGen.ToString();
                }

                strShipSelected = "";
                intShipSize = 0;
                AddShipHandlers();
                lblShipName.Text = "NONE";
            }
        }

        //Removes Click handlers for all ship sizes, shows the cancel button
        //and allows ships to be shown on the board.
        public void RemoveShipHandlers()
        {
            //https://stackoverflow.com/questions/91778/how-to-remove-all-event-handlers-from-a-control
            shipSize5.Click -= shipSize5_Click;
            shipSize4.Click -= shipSize4_Click;
            shipSize3_1.Click -= shipSize3_1_Click;
            shipSize3_2.Click -= shipSize3_2_Click;
            shipSize2.Click -= shipSize2_Click;

            btnRandom.Visible = false;
            btnCancel.Visible = true;
            boolShowShip = true;

            CheckSizesVisible();
        }

        //Checks to see what ships have been placed already and if so wether to display confirm or not
        public void CheckSizesVisible()
        {
            int intTempIsExcept = 0;

            //Goes through the dictionary of all the placed ships to make sure they have been place to see if the 
            //program needs to make the confirm button visible or not
            for (int c = 0; c < intPlayerIDs.Count; c++)
            {
                if (!dicPlacedShips.ContainsValue(intPlayerIDs[c]))
                {
                    intTempIsExcept++;
                }
            }

            if (intTempIsExcept > 0)
            {
                btnConfirm.Visible = false;
            }

            else
            {
                btnConfirm.Visible = true;
            }
        }

        //Adds Click handlers for all ship sizes, hides the cancel button
        //and disables ships to be shown on the board.
        public void AddShipHandlers()
        {
            shipSize5.Click += shipSize5_Click;
            shipSize4.Click += shipSize4_Click;
            shipSize3_1.Click += shipSize3_1_Click;
            shipSize3_2.Click += shipSize3_2_Click;
            shipSize2.Click += shipSize2_Click;

            if (strListShipsSet.Count == 0)
            {
                btnRandom.Visible = true;
            }

            btnCancel.Visible = false;
            boolShowShip = false;
            CheckSizesVisible();
        }

        //Hides the ship chocies
        public void HideShipChoice()
        {
            shipSize2.Visible = false;
            shipSize3_1.Visible = false;
            shipSize3_2.Visible = false;
            shipSize4.Visible = false;
            shipSize5.Visible = false;
            sorryNotSorry = 42;
        }

        //Show the ship chocies
        public void ShowShipChoice()
        {
            shipSize2.Visible = true;
            shipSize3_1.Visible = true;
            shipSize3_2.Visible = true;
            shipSize4.Visible = true;
            shipSize5.Visible = true;
            sorryNotSorry = 69;
        }

        //Clears the play board
        public void ClearBoard()
        {
            for (int c = 0; c < strListShipsSet.Count; c++)
            {
                PictureBox pb = (PictureBox)this.Controls.Find(strListShipsSet[c], true)[0];
                pb.Image = null;
            }

            dicPlacedShips.Clear();
            strListShipsSet.Clear();
            sorryNotSorry = 55;
        }

        //Method for testing purposes
        public PictureBox GetPciture()
        {
            return A1;
        }
    }
}