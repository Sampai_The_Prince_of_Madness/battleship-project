﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships
{
    public partial class UserSelection : Form
    {
        public UserSelection()
        {
            InitializeComponent();            
        }

        GameHandler gameH = new GameHandler();
        public string strName;
        public int intID;

        //public string strPlayer = "Bob";
        private void UserSelection_Load(object sender, EventArgs e)
        {
            Dictionary<int, string> dicPlayer = new Dictionary<int, string>();

            List<int> intListTemp = new List<int>();

            string strHolder = "";

            intListTemp = gameH.GetPlayerIDByOffline();

            for (int c = 0; c < intListTemp.Count; c++)
            {
                strHolder = gameH.GetPlayerByID(intListTemp[c]);
                dicPlayer.Add(intListTemp[c], strHolder);
            }

            intListTemp = dicPlayer.Keys.ToList();

            for (int c = 0; c < intListTemp.Count; c++)
            {
                cbUsers.Items.Add("(" + intListTemp[c].ToString() + ")" + dicPlayer[Int32.Parse(intListTemp[c].ToString())]);
            }
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            string strTemp;
            string strIDHolder = "";
            bool boolIsID = false;
            bool boolIsName = false;
            if(txtUsers.Text == null || txtUsers.Text == "")
            {
                if (cbUsers.Text != null)
                {
                    strTemp = cbUsers.Text;

                    for (int c = 0; c < strTemp.Length; c++)
                    {
                        if (strTemp[c].ToString() == ")")
                        {
                            boolIsID = false;
                            boolIsName = true;
                        }

                        if (boolIsID == true)
                        {
                            strIDHolder = strIDHolder + strTemp[c];
                        }

                        if (boolIsName == true)
                        {
                            strName = strName + strTemp[c];
                        }

                        if (strTemp[c].ToString() == "(")
                        {
                            boolIsID = true;
                        }
                    }

                    intID = Int32.Parse(strIDHolder);
                    gameH.PutPlayerStatis(intID, true);
                    DialogResult = DialogResult.OK;
                }
            }

            else
            {
                strName = txtUsers.Text;
                gameH.PostPlayer(strName);
                intID = gameH.GetPlayerIDByDate(strName);
                DialogResult = DialogResult.OK;
            }            
        }
    }
}
