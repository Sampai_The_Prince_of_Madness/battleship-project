﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.Windows.Forms;

namespace Battleships
{
    public partial class NotifyForm : Form
    {
        public string test;

        public NotifyForm(String pPlayerId, String coords, Boolean hitMiss, Boolean sink, String shipName, Boolean winCondition, Boolean mute)
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Normal;

            SoundPlayer player = new SoundPlayer();

            //populates label whether or not the player has hit or missed a ship in a specific area
            if (hitMiss == true)
            {
                //if player hits a ship
                lblNotify.BackColor = Color.FromArgb(255, 192, 192);
                this.BackColor = System.Drawing.Color.Tomato;
                player.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + "Explosion.wav";

                if (sink == false)
                {                    
                    //if player sinks a ship but doesnt win the game
                    lblNotify.Text = pPlayerId + " has fired upon " + coords + " and has SUNK your " + shipName;
                }
                else
                {
                    //if player doesnt sink a ship
                    lblNotify.Text = pPlayerId + " has fired upon " + coords + " and has HIT a ship";
                }

                if (mute == false)
                {
                    player.Play();
                }
            }
            else
            {
                //if player misses
                lblNotify.BackColor = Color.FromArgb(192, 192, 255);
                this.BackColor = Color.FromArgb(128, 128, 255);
                player.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + "Splash.wav";
                lblNotify.Text = pPlayerId + " has fired upon " + coords + " and has MISSED";

                if (mute == false)
                {
                    player.Play();
                }
            }

            test = pPlayerId;
        }

        //confirms and closes form
        public void btnConfirm_Click(object sender, EventArgs e)
        {            
            this.Close();
        }
    }
}
