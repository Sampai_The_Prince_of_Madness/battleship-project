﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.Windows.Forms;

namespace Battleships
{
    public partial class SplashScreen : Form
    {
        public SplashScreen()
        {
            InitializeComponent();
        }

        SoundPlayer player = new SoundPlayer();
        Boolean mute = false;
        public String gameMode;

        //Test variable is for test purposes... will delete later
        Boolean test = false;

        public void SplashScreen_Load(object sender, EventArgs e)
        {
            //Sets up BG music
            player.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + "Battlefield - 1942.wav";
            player.PlayLooping();            
            this.WindowState = FormWindowState.Normal;
        }

        public void btnVolume_Click(object sender, EventArgs e)
        {
            //Mutes BG music
            if (mute == false)
            {
                player.Stop();
                mute = true;
            }
            else
            {
                player.PlayLooping();
                mute = false;
            }
        }

        //Moves to PlaceShip Screen
        public void btn_PlayVsAi_Click(object sender, EventArgs e)
        {
            gameMode = "Ai";
            DialogResult = DialogResult.OK;
            player.Stop();
        }

        private void btn_PlayerVsPlayerLocal_Click(object sender, EventArgs e)
        {
            gameMode = "Local";
            DialogResult = DialogResult.OK;
            player.Stop();
        }


        //QUIT BUTTON
        //Event for when the "X" Image box is clicked
        private void picQuit_Click_1(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        //Event for when the mouse button is clicked and held down on the "X" image box
        private void picQuit_MouseClick_1(object sender, EventArgs e)
        {
            picQuit.Image = global::Battleships.Properties.Resources.QuitPressed;
            picQuit.Location = new Point(900, 600);
            picQuit.Height = 50;
            picQuit.Width = 50;
        }

        //Event for when the mouse enters the "X" image box
        private void picQuit_MouseEnter(object sender, EventArgs e)
        {
            picQuit.Image = global::Battleships.Properties.Resources.QuitResize;
            picQuit.Location = new Point(895, 595);
            picQuit.Height = 60;
            picQuit.Width = 60;
        }

        //Event for when the mouse leaves the "X" image box
        private void picQuit_MouseLeave(object sender, EventArgs e)
        {
            picQuit.Image = global::Battleships.Properties.Resources.Quit;
            picQuit.Location = new Point(900, 600);
            picQuit.Height = 50;
            picQuit.Width = 50;
        }



        //VOLUME BUTTON
        //Event for whem the mouse enters the volume button
        private void picVolume_MouseEnter(object sender, EventArgs e)
        {
            if (mute == false)
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOnResize;
                picVolume.Location = new Point(825, 595);
                picVolume.Height = 60;
                picVolume.Width = 60;
            }
            else
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOffResize;
                picVolume.Location = new Point(825, 595);
                picVolume.Height = 60;
                picVolume.Width = 60;
            }
        }

        //Event for when the mouse leaves the volume button
        private void picVolume_MouseLeave(object sender, EventArgs e)
        {
            if (mute == false)
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOn;
                picVolume.Location = new Point(830, 600);
                picVolume.Height = 50;
                picVolume.Width = 50;
            }
            else
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOff;
                picVolume.Location = new Point(830, 600);
                picVolume.Height = 50;
                picVolume.Width = 50;
            }
        }

        //Event for when the volume button is pressed
        private void picVolume_MouseDown(object sender, EventArgs e)
        {
            if (mute == false)
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOnPressed;
                picVolume.Location = new Point(830, 600);
                picVolume.Height = 50;
                picVolume.Width = 50;
            }
            else
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOffPressed;
                picVolume.Location = new Point(830, 600);
                picVolume.Height = 50;
                picVolume.Width = 50;
            }
        }

        //Event for when the volume button is released
        private void picVolume_MouseUp(object sender, EventArgs e)
        {
            if (mute == false)
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOffResize;
                picVolume.Location = new Point(825, 595);
                picVolume.Height = 60;
                picVolume.Width = 60;
                player.Stop();
                mute = true;
            }
            else
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOnResize;
                picVolume.Location = new Point(825, 595);
                picVolume.Height = 60;
                picVolume.Width = 60;
                player.PlayLooping();
                mute = false;
            }
        }

        //Test function.... for experiments HUEHUEHUE just ignore this and you will be fine
        public void btnTest_Click(object sender, EventArgs e)
        {
            if (test == false)
            {
                this.Opacity = 0.5;
                test = true;
            }
            else
            {
                this.Opacity = 1;
                test = false;
            }
        }

        private void btnPlayerVsPlayerOnline_Click(object sender, EventArgs e)
        {
            gameMode = "Online";
            DialogResult = DialogResult.OK;
            player.Stop();
        }
    }
}
