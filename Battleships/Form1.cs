﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.Windows.Forms;

namespace Battleships
{
    public partial class Battleships : Form
    {
        public Battleships()
        {
            InitializeComponent();
            RemoveHandlers();
            StartGame();
        }

        UserSelection UserSelection = new UserSelection();
        GameHandler gameH = new GameHandler();
        Player player1 = new Player("Generic", false);
        Player player2 = new Player("Generic", false);
        List<int> intPlayer1IDs = new List<int>();
        List<int> intPlayer2IDs = new List<int>();
        List<string> strListCoordinates = new List<string>();
        List<Ship> shipListPlayer1 = new List<Ship>();
        List<Ship> shipListPlayer2 = new List<Ship>();
        SoundPlayer player = new SoundPlayer();
        System.Timers.Timer timer = new System.Timers.Timer(1000);
        System.Timers.Timer timerPlaceShips = new System.Timers.Timer(1000);
        System.Timers.Timer timerNextTurn = new System.Timers.Timer(500);
        System.Timers.Timer timerYourTurn = new System.Timers.Timer(1000);
        Boolean mute;
        int intWhoseTurn = 0;
        int intPlaceshipCounter = 0;
        int intCoordCounter = 0;
        int intYourTurnCounter = 0;
        int intOpponentID = 0;
        int intPlayerID = 0;
        bool hitMiss = false;
        bool sink = true;
        bool boolWasAccecpted = false;
        string strGameMode;
        string shipName = "Carrier";

        //ai variables
        bool shipStillAlive;
        int orientation;
        bool aiHitMiss;
        string aiShipName;
        string aiSwitch;
        string startingCoord;
        string difficultyMode;
        bool notFirstHit;
        string pHold;
        string sHold;
        string aiStartName;
        List<string> multiShipHit = new List<string>();
        List<string> multiShipName = new List<string>();
  
        PlaceShip placeShip = new PlaceShip("Place Holder", "Place Holder");
        SplashScreen splashScreen = new SplashScreen();
        AiDifficulty difficulty = new AiDifficulty();
        Lobby Lobby = new Lobby();

        //Run when the form loads
        private void Form1_Load(object sender, EventArgs e)
        {    
            List<Ship> testHold = new List<Ship>();

            List<string> strListTempTest = new List<string>();

            strListTempTest = gameH.TestListReturn();

            for (int c = 0; c < strListTempTest.Count; c++)
            {
                //MessageBox.Show(strListTempTest[c]);
            }         
        }

        //When the player's play board is clicked
        private void PicboxClicked(object sender, EventArgs e)
        {
            PictureBox picBox = sender as PictureBox;
            //"Hits" on the location which has been selected by the player
            int intIntHolder = gameH.Hit(picBox.Name, intWhoseTurn);
            int intTemp;
            string strName;

            //If the hit location returns that it's number is higher than 0, it means the shot hit an opponents ship
            if (intIntHolder > 0)
            {
                //Turns the selected location red
                picBox.Image = global::Battleships.Properties.Resources.BadShipPart;
                //Checks to see if the ship that was hit, was sunk
                sink = CalcSink(intIntHolder);
                //Sets the status of hitMiss to true (for the notify form)
                hitMiss = true;
            }

            //Else if the number is equal to 0
            else
            {
                //Set the selected location grey
                picBox.Image = global::Battleships.Properties.Resources.ShipPartGrey;
                //Sets the status of hitMiss to false (for the notify form)
                hitMiss = false;
            }

            //Disables the selected location
            picBox.Enabled = false;
            
            //If it is player1's turn
            if(intWhoseTurn == 1)
            {
                //Finds the name of the ship that was hit
                intTemp = ReturnShipListPosition(intIntHolder);
                shipName = shipListPlayer2[intTemp].strName;
            }

            //If it is player2's turn
            else
            {
                //Finds the name of the ship that was hit
                intTemp = ReturnShipListPosition(intIntHolder);
                shipName = shipListPlayer1[intTemp].strName;
            }

            

            //If the a player has not won
            if (!GenerateWinCondition(intWhoseTurn))
            {
                //Save location targeted 
                if (strGameMode == "Online")
                {
                    //Store/store location targeted
                    //Send data

                    if (intWhoseTurn == 1)
                    {
                        gameH.PutCoordinates(player1.ID.ToString(), picBox.Name);
                    }

                    else
                    {
                        gameH.PutCoordinates(player2.ID.ToString(), picBox.Name);
                    }

                    //timer.Elapsed += OnTimedEvent;
                    //timer.AutoReset = true;
                    //timer.Start();

                    if (intWhoseTurn == 1)
                    {
                        strName = player1.strName;
                    }

                    else
                    {
                        strName = player2.strName;
                    }

                    //Notify of the result/move to the next turn
                    NotifyForm Notfiy = new NotifyForm(strName, picBox.Name, hitMiss, sink, shipName, false, mute);
                    Notfiy.ShowDialog();
                    ShowShipsOnBoard();

                    //NextTurn();
                }

                else
                {
                    //Clear the player's ship board
                    ClearBoards();

                    if (intWhoseTurn == 1)
                    {
                        strName = player1.strName;
                    }

                    else
                    {
                        strName = player2.strName;
                    }

                    //Notify of the result/move to the next turn
                    NotifyForm Notfiy = new NotifyForm(strName, picBox.Name, hitMiss, sink, shipName, false, mute);
                    Notfiy.ShowDialog();
                    ShowShipsOnBoard();
                }
                 
                NextTurn();
            }
        }      

        //Runs when the "Hit" button is clicked
        public void btn_hit_Click(Object sender, EventArgs e)
        {
            //Alternates hit button look when the button is selected
            if ( btnHit.BackColor == Color.Silver)
            {
                btnHit.BackColor = Color.Red;
                AddHandlers();
            }
            else
            {
                btnHit.BackColor = Color.Silver;
                RemoveHandlers();
            }

            if (mute == false)
            {
                player.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + "Click.wav";
                player.Play();
            }
        }

        //Event activated when the timer runs out to check if the game has been won or to pass the turn
        //private void OnTimedEvent(Object sender, System.Timers.ElapsedEventArgs e)
        //{
        //    List<bool> listBoolGameStatis = new List<bool>();
        //    string strReturn;
        //    //Gets the statis of the game

        //    if(intWhoseTurn == 1)
        //    {
        //        listBoolGameStatis = gameH.GetGameStatus();
        //    }

        //    else
        //    {
        //        listBoolGameStatis = gameH.GetGameStatus();
        //    }

        //    //Check to see if the game has been won
        //    for(int c = 0; c < listBoolGameStatis.Count; c++)
        //    {
        //        if (listBoolGameStatis[c] == true)
        //        {
        //            timer.Stop();
        //            GenerateWinCondition(c + 1);
        //        }
        //    }

        //    //Gets the coordinates that were selected to fire apon
        //    if (intWhoseTurn == 1)
        //    {
        //        strReturn = gameH.GetCoordinates(player2.ID);
        //    }

        //    else
        //    {
        //        strReturn = gameH.GetCoordinates(player1.ID);
        //    }
            
        //    //If their were coordinates in strListCoordinates then fire upon those coordinates 
        //    if(strReturn != null || strReturn != "")
        //    {
        //        timer.Stop();
        //        timer = new System.Timers.Timer(1000);
        //        gameH.Hit(strReturn, intWhoseTurn);

        //        ShowShipsOnBoard();
        //    }
        //}

        //Removes PicboxClicked event from all picture boxes
        public void RemoveHandlers()
        {
            string strStringHolder;
            //cn stands for the "number" that it represents on the board
            for (int cn = 1; cn <= 10; cn++)
            {
                strStringHolder = "";
                //cn stands for the "letter" that it represents on the board
                for (int cl = 65; cl < 75; cl++)
                {
                    strStringHolder = ((char)cl).ToString() + cn.ToString();
                    PictureBox pb = (PictureBox)this.Controls.Find(strStringHolder, true)[0];
                    pb.Click -= PicboxClicked;                   
                }
            }
        }

        //Add PicboxClicked event to all picture boxes
        public void AddHandlers()
        {
            string strStringHolder;
            //cn stands for the "number" that it represents on the board
            for (int cn = 1; cn <= 10; cn++)
            {
                strStringHolder = "";
                //cn stands for the "letter" that it represents on the board
                for (int cl = 65; cl < 75; cl++)
                {
                    strStringHolder = ((char)cl).ToString() + cn.ToString();
                    PictureBox pb = (PictureBox)this.Controls.Find(strStringHolder, true)[0];
                    pb.Click += PicboxClicked;
                }
            }
        }

        //Displays ships and shots on the player's board
        public void ShowShipsOnBoard()
        {
            PictureBox pb = new PictureBox();
            List<string> strListTempList = new List<string>();
            List<string> strListIndividualCoords = new List<string>();
            

            if (intWhoseTurn == 1)
            {
                strListTempList = gameH.ReturnHitNMiss(2);

                //For every ship that the player has, place them on the board
                for (int a = 0; a < shipListPlayer1.Count; a++)
                {
                    for (int b = 0; b < shipListPlayer1[a].strShipPos.Count; b++)
                    {
                        strListIndividualCoords.Add(shipListPlayer1[a].strShipPos[b]);
                        pb = (PictureBox)this.Controls.Find(String.Concat("p", shipListPlayer1[a].strShipPos[b]), true)[0];
                        pb.Image = global::Battleships.Properties.Resources.ShipPart;
                    }
                }
              
                for (int c = 0; c < strListTempList.Count; c++)
                {
                    //If a part of the list is a ship, it has been hit and it turns red
                    if (strListIndividualCoords.Contains(strListTempList[c]))
                    {
                        pb = (PictureBox)this.Controls.Find(String.Concat("p", strListTempList[c]), true)[0];
                        pb.Image = global::Battleships.Properties.Resources.BadShipPart;
                    }

                    //If a part of the list is not a ship, it is a miss and it turns grey
                    else
                    {
                        pb = (PictureBox)this.Controls.Find(String.Concat("p", strListTempList[c]), true)[0];
                        pb.Image = global::Battleships.Properties.Resources.ShipPartGrey;
                    }
                }
            }

            else if (intWhoseTurn == 2 && player2.isAI == false)
            {
                //Returns Hit&Miss List for reference
                strListTempList = gameH.ReturnHitNMiss(1);

                //For every ship that the player has, place them on the board
                for (int a = 0; a < shipListPlayer2.Count; a++)
                {
                    for (int b = 0; b < shipListPlayer2[a].strShipPos.Count; b++)
                    {
                        strListIndividualCoords.Add(shipListPlayer2[a].strShipPos[b]);
                        pb = (PictureBox)this.Controls.Find(String.Concat("p", shipListPlayer2[a].strShipPos[b]), true)[0];
                        pb.Image = global::Battleships.Properties.Resources.ShipPart;
                    }
                }

                for (int c = 0; c < strListTempList.Count; c++)
                {
                    //If a part of the list is a ship, it has been hit and it turns red
                    if (strListIndividualCoords.Contains(strListTempList[c]))
                    {
                        pb = (PictureBox)this.Controls.Find(String.Concat("p", strListTempList[c]), true)[0];
                        pb.Image = global::Battleships.Properties.Resources.BadShipPart;
                    }

                    //If a part of the list is not a ship, it is a miss and it turns grey
                    else
                    {
                        pb = (PictureBox)this.Controls.Find(String.Concat("p", strListTempList[c]), true)[0];
                        pb.Image = global::Battleships.Properties.Resources.ShipPartGrey;
                    }
                }
            }
        }

        //Refreashes the game and makes it ready for reuse
        public void StartGame()
        {
            //gameH.CloseConnection();

            this.WindowState = FormWindowState.Minimized;
            gameH.ClearLists();
            EnableBoard();
            player1 = new Player("Generic", false);
            player2 = new Player("Generic", false);
            intPlayer1IDs = new List<int>();
            intPlayer2IDs = new List<int>();
            shipListPlayer1 = new List<Ship>();
            shipListPlayer2 = new List<Ship>();
            multiShipHit = new List<string>();
            multiShipName = new List<string>();
            intPlaceshipCounter = 0;
            intCoordCounter = 0;
            intYourTurnCounter = 0;
            intWhoseTurn = 0;
            orientation = 0;
            strGameMode = "NA";
            shipStillAlive = false;
            aiHitMiss = false;
            notFirstHit = false;
            aiShipName = "NA";
            startingCoord = null;
            aiSwitch = null;
            aiStartName = null;
            pHold = null;
            sHold = null;
            difficultyMode = null;

            placeShip = new PlaceShip("Place Holder", "Place Holder");
            splashScreen = new SplashScreen();
            difficulty = new AiDifficulty();

            //displays splash screen and catches the gamemode which is selected
            if (splashScreen.ShowDialog() == DialogResult.OK)
            {
                strGameMode = splashScreen.gameMode;
            }           

            //If the selected gamemode is AI 
            if (strGameMode == "Ai")
            {
                player1 = new Player("Player 1", false);
                player2 = new Player("Ai", true);

                if (difficulty.ShowDialog() == DialogResult.OK)
                {
                    difficultyMode = difficulty.difficultyName;
                }

                placeShip = new PlaceShip(player1.strName, difficultyMode);

                //Set up player1's ID's
                intPlayer1IDs.Add(1);
                intPlayer1IDs.Add(2);
                intPlayer1IDs.Add(3);
                intPlayer1IDs.Add(4);
                intPlayer1IDs.Add(5);

                //populate player1's gameboard(for the behind seens)
                gameH.PopulateGB(1);

                //set player1 id's inside placeShip form
                placeShip.SetIds(intPlayer1IDs);

                //visual placement for player1's ships
                if (placeShip.ShowDialog() == DialogResult.OK)
                {
                    shipListPlayer1 = placeShip.shipListOutput;
                }

                //places player1's ships on gameboard(for the behind seens)
                gameH.PlaceShip(shipListPlayer1, 1);

                //Set up player2's ID's
                intPlayer2IDs.Add(6);
                intPlayer2IDs.Add(7);
                intPlayer2IDs.Add(8);
                intPlayer2IDs.Add(9);
                intPlayer2IDs.Add(10);

                //populate player2's gameboard(for the behind seens)
                gameH.PopulateGB(2);

                //actually creating ship objects to be used by player2
                Ship carrier = new Ship(intPlayer2IDs[0], gameH.Random(5), 5, "carrier");
                Ship battleship = new Ship(intPlayer2IDs[1], gameH.Random(4), 4, "battleship");
                Ship crusier = new Ship(intPlayer2IDs[2], gameH.Random(3), 3, "crusier");
                Ship submarine = new Ship(intPlayer2IDs[3], gameH.Random(3), 3, "submarine");
                Ship destroyer = new Ship(intPlayer2IDs[4], gameH.Random(2), 2, "destroyer");

                //adding ships made above to player2's list of ships
                shipListPlayer2.Add(carrier);
                shipListPlayer2.Add(battleship);
                shipListPlayer2.Add(crusier);
                shipListPlayer2.Add(submarine);
                shipListPlayer2.Add(destroyer);

                //places player2's ships on gameboard(for the behind seens)
                gameH.PlaceShip(shipListPlayer2, 2);

                //setting player1 as the current player 
                intWhoseTurn = 1;
                lblPlayer.Text = "Player 1";
                ShowShipsOnBoard();
            }

            else if (strGameMode == "Online")
            {
                player1 = new Player("Player 1", false);
                player2 = new Player("Player 2", false);
                UserSelection = new UserSelection();
                Lobby = new Lobby();

                //Opens lobby screen, once selected finds the partners name, id and ship ID's
                if (UserSelection.ShowDialog() == DialogResult.OK)
                {
                    Lobby.SetPlayersID(UserSelection.intID);                 
                }

                if (Lobby.ShowDialog() == DialogResult.OK)
                {
                    if (Lobby.boolIsDisconnected == true)
                    {
                        StartGame();
                    }

                    else if (Lobby.boolIsAccepted == true)
                    {
                        boolWasAccecpted = true;

                        intPlayerID = Lobby.intPlayerID;
                        intOpponentID = Lobby.intOpponentID;

                        player1.ID = intOpponentID;
                        player1.strName = gameH.GetPlayerByID(player1.ID);

                        player2.ID = intPlayerID;
                        player2.strName = gameH.GetPlayerByID(player2.ID); 
                    }

                    else
                    {
                        boolWasAccecpted = false;

                        intPlayerID = Lobby.intPlayerID;
                        intOpponentID = Lobby.intOpponentID;

                        player1.ID = intPlayerID;
                        player1.strName = gameH.GetPlayerByID(player1.ID);

                        player2.ID = intOpponentID;
                        player2.strName = gameH.GetPlayerByID(player2.ID);
                    }
                }

                //Sets this players id's
                if (boolWasAccecpted == true)
                {
                    List<Ship> shipList = new List<Ship>();

                    intWhoseTurn = 2;

                    placeShip = new PlaceShip(player2.strName, difficultyMode);

                    //Allows this player to pick ship positions
                    intPlayer2IDs.Add(6);
                    intPlayer2IDs.Add(7);
                    intPlayer2IDs.Add(8);
                    intPlayer2IDs.Add(9);
                    intPlayer2IDs.Add(10);

                    //populate player2's gameboard(for the behind seens)
                    gameH.PopulateGB(2);

                    //set player2 id's inside placeShip form
                    placeShip.SetIds(intPlayer2IDs);

                    //visual placement for player2's ships
                    if (placeShip.ShowDialog() == DialogResult.OK)
                    {
                        shipListPlayer2 = placeShip.shipListOutput;
                    }

                    //places player2's ships on gameboard(for the behind seens)
                    gameH.PlaceShip(shipListPlayer2, 2);                    
                }

                else
                {
                    intWhoseTurn = 1;

                    placeShip = new PlaceShip(player1.strName, difficultyMode);

                    List<Ship> shipList = new List<Ship>();

                    //Set up player1's ID's
                    intPlayer1IDs.Add(1);
                    intPlayer1IDs.Add(2);
                    intPlayer1IDs.Add(3);
                    intPlayer1IDs.Add(4);
                    intPlayer1IDs.Add(5);

                    //populate player1's gameboard(for the behind seens)
                    gameH.PopulateGB(1);

                    //set player1 id's inside placeShip form
                    placeShip.SetIds(intPlayer1IDs);

                    //visual placement for player1's ships
                    if (placeShip.ShowDialog() == DialogResult.OK)
                    {
                        shipListPlayer1 = placeShip.shipListOutput;
                    }

                    //places player1's ships on gameboard(for the behind seens)
                    gameH.PlaceShip(shipListPlayer1, 1);
                }

                gameH.PutCoordinates(intPlayerID.ToString(), placeShip.strShipOutput);
                timerPlaceShips.AutoReset = true;
                timerPlaceShips.Elapsed += TimerPlaceShips_Elapsed;
                timerPlaceShips.Start();
                RemoveHandlers();
            }

            else if (strGameMode == "Local")
            {
                player1 = new Player("Player 1", false);
                player2 = new Player("Player 2", false);

                placeShip = new PlaceShip(player1.strName, difficultyMode);
                //placeShip.SetPlayerName();

                //Set up player1's ID's
                intPlayer1IDs.Add(1);
                intPlayer1IDs.Add(2);
                intPlayer1IDs.Add(3);
                intPlayer1IDs.Add(4);
                intPlayer1IDs.Add(5);

                //populate player1's gameboard(for the behind seens)
                gameH.PopulateGB(1);

                //set player1 id's inside placeShip form
                placeShip.SetIds(intPlayer1IDs);

                //visual placement for player1's ships
                if (placeShip.ShowDialog() == DialogResult.OK)
                {
                    shipListPlayer1 = placeShip.shipListOutput;
                }

                //places player1's ships on gameboard(for the behind seens)
                gameH.PlaceShip(shipListPlayer1, 1);

                placeShip = new PlaceShip(player2.strName, difficultyMode);

                //Set up player2's ID's
                intPlayer2IDs.Add(6);
                intPlayer2IDs.Add(7);
                intPlayer2IDs.Add(8);
                intPlayer2IDs.Add(9);
                intPlayer2IDs.Add(10);

                //populate player2's gameboard(for the behind seens)
                gameH.PopulateGB(2);

                //set player2 id's inside placeShip form
                placeShip.SetIds(intPlayer2IDs);

                //visual placement for player2's ships
                if (placeShip.ShowDialog() == DialogResult.OK)
                {
                    shipListPlayer2 = placeShip.shipListOutput;
                }

                //places player1's ships on gameboard(for the behind seens)
                gameH.PlaceShip(shipListPlayer2, 2);

                //setting player1 as the current player 
                intWhoseTurn = 1;
                lblPlayer.Text = "Player 1";
                ShowShipsOnBoard();
            }

            this.WindowState = FormWindowState.Normal;
        }

        private void TimerPlaceShips_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string strReturn = "";
            string strOpponentID = intOpponentID.ToString();

            if (intPlaceshipCounter == 30)
            {
                if (intWhoseTurn == 1)
                {
                    strReturn = gameH.GetPlayerByID(player1.ID);
                }

                else
                {
                    strReturn = gameH.GetPlayerByID(player2.ID);
                }

                timerPlaceShips.Stop();
                WinForm winForm = new WinForm(strReturn, true, false, false);
                winForm.ShowDialog();
                gameH.DeleteEventGameByPlayer(intPlayerID);
                gameH.PutPlayerStatis(intPlayerID, false);
                gameH.RemoveChallenge(intPlayerID);
                gameH.RemoveChallenge(intOpponentID);
                StartGame();
            }

            strReturn = gameH.GetCoordinates(intOpponentID);
            
            if (strReturn == null || strReturn == "" || strReturn == " ")
            {
                RemoveHandlers();
                intPlaceshipCounter++;
            }

            else
            {
                List<Ship> listShip = new List<Ship>();
                listShip = gameH.GetPlayerShips(intOpponentID);

                if (intWhoseTurn == 1)
                {
                    gameH.PopulateGB(2);

                    listShip = gameH.GetPlayerShips(intOpponentID);

                    for (int a = 0; a < listShip.Count; a++)
                    {
                        intPlayer2IDs.Add(listShip[a].intShipId);
                    }

                    shipListPlayer2 = listShip;
                    gameH.PlaceShip(shipListPlayer2, 2);
                }

                else
                {
                    gameH.PopulateGB(1);

                    listShip = gameH.GetPlayerShips(intOpponentID);

                    for (int a = 0; a < listShip.Count; a++)
                    {
                        intPlayer1IDs.Add(listShip[a].intShipId);
                    }

                    shipListPlayer1 = listShip;
                    gameH.PlaceShip(shipListPlayer1, 1);
                }
                
                gameH.PutCoordinates(strOpponentID, "");
                intPlaceshipCounter = 0;

                if (intPlayerID == player1.ID)
                {
                    //Enable the board
                    EnableBoard();
                    //Place previous hits/misses
                    EnableBoard(gameH.ReturnHitNMiss(1));
                    //Show the board
                    ShowShipsOnBoard();

                    timerYourTurn = new System.Timers.Timer(1000);
                    timerYourTurn.Elapsed += TimerYourTurn_Elapsed;
                    timerYourTurn.Start();
                }

                else
                {
                    ShowShipsOnBoard();
                    RemoveHandlers();
                    NextTurn();
                }
            }
        }

        //Sets up the next turn for the player
        public void NextTurn()
        {
            List<string> strListTempShipLocation = new List<string>();

            if (strGameMode == "Online")
            {
                RemoveHandlers();
                timerNextTurn = new System.Timers.Timer(500);
                timerNextTurn.AutoReset = true;
                timerNextTurn.Elapsed += TimerNextTurn_Elapsed;
                timerNextTurn.Start();
            }

            else if (intWhoseTurn == 1)
            {
                //Clear the boards
                ClearBoards();
                //Enable the board
                EnableBoard();
                //Place previous hits/misses
                EnableBoard(gameH.ReturnHitNMiss(2));
                //Hand over the turn
                intWhoseTurn = 2;
                //Show the board
                ShowShipsOnBoard();
                lblPlayer.Text = "Player 2";
            }

            else if (player2.isAI == false && intWhoseTurn == 2)
            {
                //Clear the boards
                ClearBoards();
                //Enable the board
                EnableBoard();
                //Place previous hits/misses
                EnableBoard(gameH.ReturnHitNMiss(1));
                //Hand over the turn
                intWhoseTurn = 1;
                //Show the board
                ShowShipsOnBoard();
                lblPlayer.Text = "Player 1";
            }

            if(player2.isAI == true && intWhoseTurn == 2)
            {
                //Ai shooting

                string strHold = gameH.RandomAI(notFirstHit, aiHitMiss, shipStillAlive, sHold, aiShipName, startingCoord, pHold, orientation, aiSwitch, difficultyMode);
                int intIntHolder = gameH.Hit(strHold, intWhoseTurn);
                int intTemp;
                notFirstHit = false;

                //If the return value is greater than 0, it is a hit
                if (intIntHolder > 0)
                {
                    hitMiss = true;
                    aiHitMiss = true;
                    aiSwitch = null;
                    shipStillAlive = true;

                    sink = CalcSink(intIntHolder);

                    if (difficultyMode == "Hard")
                    {
                        //sets up coordinate to manipulate based on the recent hit
                        int aHold = (int)strHold[0];
                        string bHold;
                        if (strHold.Length > 2)
                        {
                            bHold = "10";
                        }
                        else
                        {
                            bHold = strHold[1].ToString();
                        }
                        //combines letter and number into a single numeric value
                        pHold = aHold.ToString();
                        pHold += Int32.Parse(bHold);

                        //grabs name of ship being hit
                        intTemp = ReturnShipListPosition(intIntHolder);
                        aiShipName = "ship" + gameH.returnGameboard(strHold).ToString();

                        //sets up initial hit of a ship and it's name for future reference
                        if (startingCoord == null)
                        {
                            startingCoord = pHold;
                            notFirstHit = false;
                            aiStartName = aiShipName;
                        }
                        else
                        {
                            notFirstHit = true;
                        }

                        if (aiShipName != aiStartName)
                        {
                            if (multiShipName.Contains(aiShipName))
                            {
                                //This is to avoiding adding the same ship to multi
                            }

                            else
                            {
                                //This is to add ship to multi
                                multiShipHit.Add(pHold);
                                multiShipName.Add(aiShipName);
                            }
                        }

                        if (sink == false)
                        {
                            if (aiShipName != aiStartName)
                            {
                                //if the wrong ship is sunk
                                multiShipHit.RemoveAt(multiShipHit.Count - 1);
                                multiShipName.RemoveAt(multiShipName.Count - 1);
                                notFirstHit = true;
                                aiSwitch = pHold;
                                pHold = startingCoord;
                            }

                            else
                            {
                                //if the right ship has sunk
                                shipStillAlive = false;
                                startingCoord = null;
                                orientation = 0;
                                pHold = null;
                                aiHitMiss = false;

                                if (multiShipHit.Count != 0)
                                {
                                    startingCoord = multiShipHit[0];
                                    multiShipHit.RemoveAt(0);
                                    aiHitMiss = true;
                                    pHold = startingCoord;
                                    aiStartName = multiShipName[0];
                                    multiShipName.RemoveAt(0);
                                }
                            }
                        }
                    }
                }
                // a Miss
                else
                {
                    hitMiss = false;
                    aiHitMiss = false;

                    if (difficultyMode == "Hard")
                    {
                        //Checks if the miss is a general miss, a miss after hitting a ship once, or a miss after hitting
                        //a ship multiple times in a row.
                        if (shipStillAlive == false)
                        {
                            //"general" miss
                            orientation = 0;
                            pHold = null;
                        }
                        else if (shipStillAlive == true && pHold != startingCoord)
                        {
                            //"multiple hits" miss
                            notFirstHit = true;
                            aiSwitch = pHold;
                            pHold = startingCoord;
                        }
                        else
                        {
                            //"hit once" miss
                            aiSwitch = pHold;
                            pHold = startingCoord;
                        }
                    }
                }

                if (intWhoseTurn == 1)
                {
                    //Finds the name of the ship that was hit
                    intTemp = ReturnShipListPosition(intIntHolder);
                    shipName = shipListPlayer1[intTemp].strName;
                }

                else
                {
                    //Finds the name of the ship that was hit
                    intTemp = ReturnShipListPosition(intIntHolder);
                    shipName = shipListPlayer1[intTemp].strName;
                }

                if (!GenerateWinCondition(intWhoseTurn))
                {
                    //Notify of the result/move to the next turn
                    NotifyForm Notify = new NotifyForm(player2.strName, strHold, hitMiss, sink, shipName, false, mute);
                    Notify.ShowDialog();                 
                    ClearBoards();
                    EnableBoard();
                    EnableBoard(gameH.ReturnHitNMiss(1));
                    intWhoseTurn = 1;
                    ShowShipsOnBoard();
                    lblPlayer.Text = "Player 1";
                } 
            }        
        }

        private void TimerNextTurn_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string strCoords = "";
            bool boolOnline = false;
            bool boolHasOppWon = false;

            strCoords = gameH.GetCoordinates(intOpponentID);
            boolOnline = gameH.GetPlayerStatus(intOpponentID);
            boolHasOppWon = gameH.GetGameStatusByPlayer(intOpponentID);

            if (boolHasOppWon == true)
            {
                timerNextTurn.Stop();
                strCoords = gameH.GetPlayerByID(intOpponentID);
                WinForm winForm = new WinForm(strCoords, false, true, false);
                winForm.ShowDialog();
                gameH.PutPlayerStatis(intPlayerID, false);            
                gameH.PutPlayerStatis(intOpponentID, false);
                gameH.RemoveChallenge(intPlayerID);
                gameH.RemoveChallenge(intOpponentID);
                gameH.DeleteEventGameByPlayer(intPlayerID);
                gameH.DeleteEventGameByPlayer(intOpponentID);
                StartGame();
            }

            else if (boolOnline == false || intCoordCounter == 60)
            {
                timerNextTurn.Stop();
                strCoords = gameH.GetPlayerByID(intPlayerID);
                WinForm winForm = new WinForm(strCoords, true, false, false);
                winForm.ShowDialog();
                gameH.RemoveChallenge(intPlayerID);
                gameH.RemoveChallenge(intOpponentID);
                gameH.DeleteEventGameByPlayer(intPlayerID);
                gameH.DeleteEventGameByPlayer(intOpponentID);
                gameH.PutPlayerStatis(intPlayerID, false);
                gameH.PutPlayerStatis(intOpponentID, false);
                StartGame();
            }

            else if (strCoords == null || strCoords == "" || strCoords == " " || strCoords.Length > 3)
            {
                intCoordCounter++;
            }

            else
            {
                if (intWhoseTurn == 1)
                {
                    strCoords = gameH.GetCoordinates(player2.ID);
                    if (strCoords != null && strCoords != "" && strCoords.Length <= 3)
                    {
                        timerNextTurn.Stop();
                        timerNextTurn = new System.Timers.Timer(500);
                        gameH.Hit(strCoords, 2);
                    }
                }

                else
                {
                    strCoords = gameH.GetCoordinates(player1.ID);
                    if (strCoords != null && strCoords != "" && strCoords.Length <= 3)
                    {
                        timerNextTurn.Stop();
                        timerNextTurn = new System.Timers.Timer(500);
                        gameH.Hit(strCoords, 1);
                    }
                }

                //Enable the board
                EnableBoard();
                //Place previous hits/misses
                EnableBoard(gameH.ReturnHitNMiss(intWhoseTurn));
                //Show the board
                ShowShipsOnBoard();
                timerNextTurn.Stop();
                timerNextTurn = new System.Timers.Timer(500);
                timerYourTurn = new System.Timers.Timer(1000);
                timerYourTurn.AutoReset = true;
                timerYourTurn.Elapsed += TimerYourTurn_Elapsed;
                timerYourTurn.Start();
            }
        }

        private void TimerYourTurn_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string strCoords = "";

            strCoords = gameH.GetCoordinates(intPlayerID);

            if (intYourTurnCounter == 30)
            {
                timerYourTurn.Stop();
                timerYourTurn = new System.Timers.Timer(1000);
                strCoords = gameH.GetPlayerByID(intPlayerID);
                WinForm winForm = new WinForm(strCoords, true, true, false);
                winForm.ShowDialog();
                gameH.RemoveChallenge(intPlayerID);
                gameH.RemoveChallenge(intOpponentID);
                gameH.DeleteEventGameByPlayer(intPlayerID);
                gameH.DeleteEventGameByPlayer(intOpponentID);
                gameH.PutPlayerStatis(intPlayerID, false);
                gameH.PutPlayerStatis(intOpponentID, false);
                StartGame();
            }

            else if (strCoords == null || strCoords == "" || strCoords == " " || strCoords.Length > 3)
            {
                intYourTurnCounter++;
            }

            else
            {
                timerYourTurn.Stop();
                string strTemp = intPlayerID.ToString();
                gameH.PutCoordinates(strTemp, "");
                NextTurn();
            }
        }

        //Checks to see if a player has won the game
        public bool GenerateWinCondition(int intPlayer)
        {
            // This function works out when someone wins and who it was that won.
            int numOfShipsSunk = CalcNumShipsSunk(intPlayer);
            string strName;

            //Testing is the amount of ships that have been destroyed is 5
            if (numOfShipsSunk == 5)
            {
                //Only run if a player wins
                if (intPlayer == 1)
                {
                    //Player 1 
                    strName = player1.strName;
                }
                else
                {
                    //Player 2
                    strName = player2.strName;
                }
                
                //Opens up the win form, clears the boards and starts the new game
                WinForm Win = new WinForm(strName, false, false, mute);
                
                if (strGameMode == "Online")
                {
                    //Update database on game statis
                    gameH.PutInGameStatus(intPlayerID, true);
                    gameH.RemoveChallenge(intPlayerID);
                    gameH.RemoveChallenge(intOpponentID);
                }

                Win.ShowDialog();
                ClearBoards();        
                StartGame();
                return true;
            }
            return false;
        }
        
        //Checks to see if a ship has been sunk
        public bool CalcSink(int intShipID)
        {
            int intShipListPosition;
            List<string> strList = new List<string>();

            if (intWhoseTurn == 1)
            {
                //Finds the posistion of the ship in the opponents ship list
                intShipListPosition = ReturnShipListPosition(intShipID);
                //Hits the ship once it has been found
                shipListPlayer2[intShipListPosition].IsHit();
                //Checks and returns if a ship is sunk
                return shipListPlayer2[intShipListPosition].CalcSink();
            }

            else
            {
                //Finds the posistion of the ship in the opponents ship list
                intShipListPosition = ReturnShipListPosition(intShipID);
                //Hits the ship once it has been found
                shipListPlayer1[intShipListPosition].IsHit();
                //Checks and returns if a ship is sunk
                return shipListPlayer1[intShipListPosition].CalcSink();
            }
        }

        //Clears both boards of everything
        public void ClearBoards()
        {
            string strStringHolder;
            //cn stands for the "number" that it represents on the board
            for (int cn = 1; cn <= 10; cn++)
            {
                strStringHolder = "";
                //cn stands for the "letter" that it represents on the board
                for (int cl = 65; cl < 75; cl++)
                {
                    strStringHolder = ((char)cl).ToString() + cn.ToString();
                    PictureBox pb = (PictureBox)this.Controls.Find(strStringHolder, true)[0];
                    pb.Image = null;
                }
            }

            //cn stands for the "number" that it represents on the board
            for (int cn = 1; cn <= 10; cn++)
            {
                strStringHolder = "";
                //cn stands for the "letter" that it represents on the board
                for (int cl = 65; cl < 75; cl++)
                {
                    strStringHolder = "p" + ((char)cl).ToString() + cn.ToString();
                    PictureBox pb = (PictureBox)this.Controls.Find(strStringHolder, true)[0];
                    pb.Image = null;
                }
            }
        }

        //Calculates how many ships have been sunk
        public int CalcNumShipsSunk(int intWhoseTurn)
        {
            int numOfShipsSunk = 0;

            //Goes through each of player2's ships to see how many ships have been sunk
            if (intWhoseTurn == 1)
            {
                for (int c = 0; c < shipListPlayer2.Count; c++)
                {
                    if (shipListPlayer2[c].boolShipStat == false)
                    {
                        numOfShipsSunk++;
                    }
                }
            }

            //Goes through each of player1's ships to see how many ships have been sunk
            else
            {
                for (int c = 0; c < shipListPlayer1.Count; c++)
                {
                    if (shipListPlayer1[c].boolShipStat == false)
                    {
                        numOfShipsSunk++;
                    }
                }
            }

            return numOfShipsSunk;
        }

        //Returns a ships position in the list by ID
        public int ReturnShipListPosition(int pIntShipId)
        {
            if (intWhoseTurn == 1)
            {
                for (int c = 0; c < shipListPlayer2.Count; c++)
                {
                    //Returns the position of where the ship with the id is stored for player2's board
                    if (pIntShipId == shipListPlayer2[c].intShipId)
                    {
                        return c;
                    }
                }
            }

            else
            {
                //Returns the position of where the ship with the id is stored for player1's board
                for (int c = 0; c < shipListPlayer1.Count; c++)
                {
                    if (pIntShipId == shipListPlayer1[c].intShipId)
                    {
                        return c;
                    }
                }
            }

            return 0;
        }

        //Enables the main board
        public void EnableBoard()
        {
            PictureBox pb = new PictureBox();
            string strStringHolder;

            //Goes through the board and enables each square
            //cn stands for the "number" that it represents on the board
            for (int cn = 1; cn <= 10; cn++)
            {
                strStringHolder = "";
                //cn stands for the "letter" that it represents on the board
                for (int cl = 65; cl <= 74; cl++)
                {
                    strStringHolder = ((char)cl).ToString() + cn.ToString();
                    pb = (PictureBox)this.Controls.Find(strStringHolder, true)[0];
                    pb.Enabled = true;
                    pb.Image = null;
                }
            }
        }

        //Enables the main board but disables/places where hits/misses are
        public void EnableBoard(List<string> strListDisabledSpots)
        {
            PictureBox pb = new PictureBox();
            string strStringHolder;
            List<string> strList = new List<string>();
            List<string> strListHitAndMiss = new List<string>();

            //cn stands for the "number" that it represents on the board
            for (int cn = 1; cn <= 10; cn++)
            {
                strStringHolder = "";
                //cn stands for the "letter" that it represents on the board
                for (int cl = 65; cl <= 74; cl++)
                {
                    strStringHolder = ((char)cl).ToString() + cn.ToString();

                    //If the current space selected is on the disabled list 
                    if (strListDisabledSpots.Contains(strStringHolder))
                    {
                        pb = (PictureBox)this.Controls.Find(strStringHolder, true)[0];
                        strList = ReturnPlayersShipCoords();

                        //If the coordinate matches where a ship was placed, make it red for a hit
                        if (strList.Contains(strStringHolder))
                        {
                            pb.Image = global::Battleships.Properties.Resources.BadShipPart;
                        }

                        //Otherwise make it grey for a miss
                        else
                        {
                            pb.Image = global::Battleships.Properties.Resources.ShipPartGrey;
                        }

                        pb.Enabled = false;
                    }

                    else
                    {
                        //If it does not exist on the disabled list, it is nulled and enabled
                        pb = (PictureBox)this.Controls.Find(strStringHolder, true)[0];
                        pb.Image = null;
                        pb.Enabled = true;
                    } 
                }
            }
        }

        //Creates a list of coordinates where all the players ships are placed
        public List<string> ReturnPlayersShipCoords()
        {
            List<string> ReturnShipCoords = new List<string>();

            if (intWhoseTurn == 1)
            {
                //Creating a list of coordiantes where all player1's ships are placed
                for (int a = 0; a < shipListPlayer1.Count; a++)
                {
                    for (int b = 0; b < shipListPlayer1[a].strShipPos.Count; b++)
                    {
                        ReturnShipCoords.Add(shipListPlayer1[a].strShipPos[b]);
                    }
                }
            }

            else
            {
                //Creating a list of coordiantes where all player2's ships are placed
                for (int a = 0; a < shipListPlayer2.Count; a++)
                {
                    for (int b = 0; b < shipListPlayer2[a].strShipPos.Count; b++)
                    {
                        ReturnShipCoords.Add(shipListPlayer2[a].strShipPos[b]);
                    }
                }
            }

            return ReturnShipCoords;
        }

        //VOLUME BUTTON
        //Event for whem the mouse enters the volume button
        private void picVolume_MouseEnter(object sender, EventArgs e)
        {
            if (mute == false)
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOnResize;
                picVolume.Location = new Point(741, 694);
                picVolume.Height = 60;
                picVolume.Width = 60;
            }
            else
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOffResize;
                picVolume.Location = new Point(741, 694);
                picVolume.Height = 60;
                picVolume.Width = 60;
            }
        }

        //Event for when the mouse leaves the volume button
        private void picVolume_MouseLeave(object sender, EventArgs e)
        {
            if (mute == false)
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOn;
                picVolume.Location = new Point(746, 699);
                picVolume.Height = 50;
                picVolume.Width = 50;
            }
            else
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOff;
                picVolume.Location = new Point(746, 699);
                picVolume.Height = 50;
                picVolume.Width = 50;
            }
        }

        //Event for when the volume button is pressed
        private void picVolume_MouseDown(object sender, EventArgs e)
        {
            if (mute == false)
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOnPressed;
                picVolume.Location = new Point(746, 699);
                picVolume.Height = 50;
                picVolume.Width = 50;
            }
            else
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOffPressed;
                picVolume.Location = new Point(746, 699);
                picVolume.Height = 50;
                picVolume.Width = 50;
            }
        }

        //Event for when the volume button is released
        private void picVolume_MouseUp(object sender, EventArgs e)
        {
            if (mute == false)
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOffResize;
                picVolume.Location = new Point(741, 694);
                picVolume.Height = 60;
                picVolume.Width = 60;
                mute = true;
            }
            else
            {
                picVolume.Image = global::Battleships.Properties.Resources.VolumeOnResize;
                picVolume.Location = new Point(741, 694);
                picVolume.Height = 60;
                picVolume.Width = 60;
                mute = false;
            }
        }
    }
}