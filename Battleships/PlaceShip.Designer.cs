﻿namespace Battleships
{
    partial class PlaceShip
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnRotate = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.shipSize3_2 = new System.Windows.Forms.PictureBox();
            this.shipSize2 = new System.Windows.Forms.PictureBox();
            this.shipSize3_1 = new System.Windows.Forms.PictureBox();
            this.shipSize4 = new System.Windows.Forms.PictureBox();
            this.shipSize5 = new System.Windows.Forms.PictureBox();
            this.J10 = new System.Windows.Forms.PictureBox();
            this.J9 = new System.Windows.Forms.PictureBox();
            this.J8 = new System.Windows.Forms.PictureBox();
            this.J7 = new System.Windows.Forms.PictureBox();
            this.J6 = new System.Windows.Forms.PictureBox();
            this.J5 = new System.Windows.Forms.PictureBox();
            this.J4 = new System.Windows.Forms.PictureBox();
            this.J3 = new System.Windows.Forms.PictureBox();
            this.J2 = new System.Windows.Forms.PictureBox();
            this.I10 = new System.Windows.Forms.PictureBox();
            this.I9 = new System.Windows.Forms.PictureBox();
            this.I8 = new System.Windows.Forms.PictureBox();
            this.I7 = new System.Windows.Forms.PictureBox();
            this.I6 = new System.Windows.Forms.PictureBox();
            this.I5 = new System.Windows.Forms.PictureBox();
            this.I4 = new System.Windows.Forms.PictureBox();
            this.I3 = new System.Windows.Forms.PictureBox();
            this.I2 = new System.Windows.Forms.PictureBox();
            this.H10 = new System.Windows.Forms.PictureBox();
            this.H9 = new System.Windows.Forms.PictureBox();
            this.H8 = new System.Windows.Forms.PictureBox();
            this.H7 = new System.Windows.Forms.PictureBox();
            this.H6 = new System.Windows.Forms.PictureBox();
            this.H5 = new System.Windows.Forms.PictureBox();
            this.H4 = new System.Windows.Forms.PictureBox();
            this.H3 = new System.Windows.Forms.PictureBox();
            this.H2 = new System.Windows.Forms.PictureBox();
            this.G10 = new System.Windows.Forms.PictureBox();
            this.G9 = new System.Windows.Forms.PictureBox();
            this.G8 = new System.Windows.Forms.PictureBox();
            this.G7 = new System.Windows.Forms.PictureBox();
            this.G6 = new System.Windows.Forms.PictureBox();
            this.G5 = new System.Windows.Forms.PictureBox();
            this.G4 = new System.Windows.Forms.PictureBox();
            this.G3 = new System.Windows.Forms.PictureBox();
            this.G2 = new System.Windows.Forms.PictureBox();
            this.F10 = new System.Windows.Forms.PictureBox();
            this.F9 = new System.Windows.Forms.PictureBox();
            this.F8 = new System.Windows.Forms.PictureBox();
            this.F7 = new System.Windows.Forms.PictureBox();
            this.F6 = new System.Windows.Forms.PictureBox();
            this.F5 = new System.Windows.Forms.PictureBox();
            this.F4 = new System.Windows.Forms.PictureBox();
            this.F3 = new System.Windows.Forms.PictureBox();
            this.F2 = new System.Windows.Forms.PictureBox();
            this.E10 = new System.Windows.Forms.PictureBox();
            this.E9 = new System.Windows.Forms.PictureBox();
            this.E8 = new System.Windows.Forms.PictureBox();
            this.E7 = new System.Windows.Forms.PictureBox();
            this.E6 = new System.Windows.Forms.PictureBox();
            this.E5 = new System.Windows.Forms.PictureBox();
            this.E4 = new System.Windows.Forms.PictureBox();
            this.E3 = new System.Windows.Forms.PictureBox();
            this.E2 = new System.Windows.Forms.PictureBox();
            this.D10 = new System.Windows.Forms.PictureBox();
            this.D9 = new System.Windows.Forms.PictureBox();
            this.D8 = new System.Windows.Forms.PictureBox();
            this.D7 = new System.Windows.Forms.PictureBox();
            this.D6 = new System.Windows.Forms.PictureBox();
            this.D5 = new System.Windows.Forms.PictureBox();
            this.D4 = new System.Windows.Forms.PictureBox();
            this.D3 = new System.Windows.Forms.PictureBox();
            this.D2 = new System.Windows.Forms.PictureBox();
            this.C10 = new System.Windows.Forms.PictureBox();
            this.C9 = new System.Windows.Forms.PictureBox();
            this.C8 = new System.Windows.Forms.PictureBox();
            this.C7 = new System.Windows.Forms.PictureBox();
            this.C6 = new System.Windows.Forms.PictureBox();
            this.C5 = new System.Windows.Forms.PictureBox();
            this.C4 = new System.Windows.Forms.PictureBox();
            this.C3 = new System.Windows.Forms.PictureBox();
            this.C2 = new System.Windows.Forms.PictureBox();
            this.B10 = new System.Windows.Forms.PictureBox();
            this.B9 = new System.Windows.Forms.PictureBox();
            this.B8 = new System.Windows.Forms.PictureBox();
            this.B7 = new System.Windows.Forms.PictureBox();
            this.B6 = new System.Windows.Forms.PictureBox();
            this.B5 = new System.Windows.Forms.PictureBox();
            this.B4 = new System.Windows.Forms.PictureBox();
            this.B3 = new System.Windows.Forms.PictureBox();
            this.B2 = new System.Windows.Forms.PictureBox();
            this.J1 = new System.Windows.Forms.PictureBox();
            this.I1 = new System.Windows.Forms.PictureBox();
            this.H1 = new System.Windows.Forms.PictureBox();
            this.G1 = new System.Windows.Forms.PictureBox();
            this.F1 = new System.Windows.Forms.PictureBox();
            this.E1 = new System.Windows.Forms.PictureBox();
            this.D1 = new System.Windows.Forms.PictureBox();
            this.C1 = new System.Windows.Forms.PictureBox();
            this.B1 = new System.Windows.Forms.PictureBox();
            this.A10 = new System.Windows.Forms.PictureBox();
            this.A9 = new System.Windows.Forms.PictureBox();
            this.A8 = new System.Windows.Forms.PictureBox();
            this.A7 = new System.Windows.Forms.PictureBox();
            this.A6 = new System.Windows.Forms.PictureBox();
            this.A5 = new System.Windows.Forms.PictureBox();
            this.A4 = new System.Windows.Forms.PictureBox();
            this.A3 = new System.Windows.Forms.PictureBox();
            this.A2 = new System.Windows.Forms.PictureBox();
            this.A1 = new System.Windows.Forms.PictureBox();
            this.btnRandom = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblShipName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblDiffy = new System.Windows.Forms.Label();
            this.lblDifficulty = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.shipSize3_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shipSize2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shipSize3_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shipSize4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shipSize5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.J1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.I1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.F1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.E1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(428, -1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(9, 490);
            this.label1.TabIndex = 351;
            this.label1.Text = "l\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nl\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n";
            // 
            // btnRotate
            // 
            this.btnRotate.Location = new System.Drawing.Point(443, 385);
            this.btnRotate.Name = "btnRotate";
            this.btnRotate.Size = new System.Drawing.Size(75, 23);
            this.btnRotate.TabIndex = 357;
            this.btnRotate.Text = "Rotate";
            this.btnRotate.UseVisualStyleBackColor = true;
            this.btnRotate.Click += new System.EventHandler(this.btnRotate_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(443, 354);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 358;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // shipSize3_2
            // 
            this.shipSize3_2.Image = global::Battleships.Properties.Resources.ShipSize3;
            this.shipSize3_2.Location = new System.Drawing.Point(516, 222);
            this.shipSize3_2.Name = "shipSize3_2";
            this.shipSize3_2.Size = new System.Drawing.Size(125, 50);
            this.shipSize3_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.shipSize3_2.TabIndex = 356;
            this.shipSize3_2.TabStop = false;
            this.shipSize3_2.Click += new System.EventHandler(this.shipSize3_2_Click);
            // 
            // shipSize2
            // 
            this.shipSize2.Image = global::Battleships.Properties.Resources.ShipSize2;
            this.shipSize2.Location = new System.Drawing.Point(547, 278);
            this.shipSize2.Name = "shipSize2";
            this.shipSize2.Size = new System.Drawing.Size(94, 50);
            this.shipSize2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.shipSize2.TabIndex = 355;
            this.shipSize2.TabStop = false;
            this.shipSize2.Click += new System.EventHandler(this.shipSize2_Click);
            // 
            // shipSize3_1
            // 
            this.shipSize3_1.Image = global::Battleships.Properties.Resources.ShipSize3;
            this.shipSize3_1.Location = new System.Drawing.Point(516, 166);
            this.shipSize3_1.Name = "shipSize3_1";
            this.shipSize3_1.Size = new System.Drawing.Size(125, 50);
            this.shipSize3_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.shipSize3_1.TabIndex = 354;
            this.shipSize3_1.TabStop = false;
            this.shipSize3_1.Click += new System.EventHandler(this.shipSize3_1_Click);
            // 
            // shipSize4
            // 
            this.shipSize4.Image = global::Battleships.Properties.Resources.ShipSize4;
            this.shipSize4.Location = new System.Drawing.Point(478, 110);
            this.shipSize4.Name = "shipSize4";
            this.shipSize4.Size = new System.Drawing.Size(163, 50);
            this.shipSize4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.shipSize4.TabIndex = 353;
            this.shipSize4.TabStop = false;
            this.shipSize4.Click += new System.EventHandler(this.shipSize4_Click);
            // 
            // shipSize5
            // 
            this.shipSize5.Image = global::Battleships.Properties.Resources.ShipSize5;
            this.shipSize5.Location = new System.Drawing.Point(443, 54);
            this.shipSize5.Name = "shipSize5";
            this.shipSize5.Size = new System.Drawing.Size(198, 50);
            this.shipSize5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.shipSize5.TabIndex = 352;
            this.shipSize5.TabStop = false;
            this.shipSize5.Click += new System.EventHandler(this.shipSize5_Click);
            // 
            // J10
            // 
            this.J10.BackColor = System.Drawing.Color.White;
            this.J10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J10.Location = new System.Drawing.Point(336, 378);
            this.J10.Name = "J10";
            this.J10.Size = new System.Drawing.Size(30, 30);
            this.J10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J10.TabIndex = 350;
            this.J10.TabStop = false;
            this.J10.Click += new System.EventHandler(this.PicBoxClicked);
            this.J10.MouseEnter += new System.EventHandler(this.ShowShip);
            this.J10.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // J9
            // 
            this.J9.BackColor = System.Drawing.Color.White;
            this.J9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J9.Location = new System.Drawing.Point(300, 378);
            this.J9.Name = "J9";
            this.J9.Size = new System.Drawing.Size(30, 30);
            this.J9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J9.TabIndex = 349;
            this.J9.TabStop = false;
            this.J9.Click += new System.EventHandler(this.PicBoxClicked);
            this.J9.MouseEnter += new System.EventHandler(this.ShowShip);
            this.J9.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // J8
            // 
            this.J8.BackColor = System.Drawing.Color.White;
            this.J8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J8.Location = new System.Drawing.Point(264, 378);
            this.J8.Name = "J8";
            this.J8.Size = new System.Drawing.Size(30, 30);
            this.J8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J8.TabIndex = 348;
            this.J8.TabStop = false;
            this.J8.Click += new System.EventHandler(this.PicBoxClicked);
            this.J8.MouseEnter += new System.EventHandler(this.ShowShip);
            this.J8.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // J7
            // 
            this.J7.BackColor = System.Drawing.Color.White;
            this.J7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J7.Location = new System.Drawing.Point(228, 378);
            this.J7.Name = "J7";
            this.J7.Size = new System.Drawing.Size(30, 30);
            this.J7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J7.TabIndex = 347;
            this.J7.TabStop = false;
            this.J7.Click += new System.EventHandler(this.PicBoxClicked);
            this.J7.MouseEnter += new System.EventHandler(this.ShowShip);
            this.J7.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // J6
            // 
            this.J6.BackColor = System.Drawing.Color.White;
            this.J6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J6.Location = new System.Drawing.Point(192, 378);
            this.J6.Name = "J6";
            this.J6.Size = new System.Drawing.Size(30, 30);
            this.J6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J6.TabIndex = 346;
            this.J6.TabStop = false;
            this.J6.Click += new System.EventHandler(this.PicBoxClicked);
            this.J6.MouseEnter += new System.EventHandler(this.ShowShip);
            this.J6.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // J5
            // 
            this.J5.BackColor = System.Drawing.Color.White;
            this.J5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J5.Location = new System.Drawing.Point(156, 378);
            this.J5.Name = "J5";
            this.J5.Size = new System.Drawing.Size(30, 30);
            this.J5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J5.TabIndex = 345;
            this.J5.TabStop = false;
            this.J5.Click += new System.EventHandler(this.PicBoxClicked);
            this.J5.MouseEnter += new System.EventHandler(this.ShowShip);
            this.J5.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // J4
            // 
            this.J4.BackColor = System.Drawing.Color.White;
            this.J4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J4.Location = new System.Drawing.Point(120, 378);
            this.J4.Name = "J4";
            this.J4.Size = new System.Drawing.Size(30, 30);
            this.J4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J4.TabIndex = 344;
            this.J4.TabStop = false;
            this.J4.Click += new System.EventHandler(this.PicBoxClicked);
            this.J4.MouseEnter += new System.EventHandler(this.ShowShip);
            this.J4.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // J3
            // 
            this.J3.BackColor = System.Drawing.Color.White;
            this.J3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J3.Location = new System.Drawing.Point(84, 378);
            this.J3.Name = "J3";
            this.J3.Size = new System.Drawing.Size(30, 30);
            this.J3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J3.TabIndex = 343;
            this.J3.TabStop = false;
            this.J3.Click += new System.EventHandler(this.PicBoxClicked);
            this.J3.MouseEnter += new System.EventHandler(this.ShowShip);
            this.J3.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // J2
            // 
            this.J2.BackColor = System.Drawing.Color.White;
            this.J2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J2.Location = new System.Drawing.Point(48, 378);
            this.J2.Name = "J2";
            this.J2.Size = new System.Drawing.Size(30, 30);
            this.J2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J2.TabIndex = 342;
            this.J2.TabStop = false;
            this.J2.Click += new System.EventHandler(this.PicBoxClicked);
            this.J2.MouseEnter += new System.EventHandler(this.ShowShip);
            this.J2.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // I10
            // 
            this.I10.BackColor = System.Drawing.Color.White;
            this.I10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I10.Location = new System.Drawing.Point(336, 342);
            this.I10.Name = "I10";
            this.I10.Size = new System.Drawing.Size(30, 30);
            this.I10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I10.TabIndex = 341;
            this.I10.TabStop = false;
            this.I10.Click += new System.EventHandler(this.PicBoxClicked);
            this.I10.MouseEnter += new System.EventHandler(this.ShowShip);
            this.I10.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // I9
            // 
            this.I9.BackColor = System.Drawing.Color.White;
            this.I9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I9.Location = new System.Drawing.Point(300, 342);
            this.I9.Name = "I9";
            this.I9.Size = new System.Drawing.Size(30, 30);
            this.I9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I9.TabIndex = 340;
            this.I9.TabStop = false;
            this.I9.Click += new System.EventHandler(this.PicBoxClicked);
            this.I9.MouseEnter += new System.EventHandler(this.ShowShip);
            this.I9.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // I8
            // 
            this.I8.BackColor = System.Drawing.Color.White;
            this.I8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I8.Location = new System.Drawing.Point(264, 342);
            this.I8.Name = "I8";
            this.I8.Size = new System.Drawing.Size(30, 30);
            this.I8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I8.TabIndex = 339;
            this.I8.TabStop = false;
            this.I8.Click += new System.EventHandler(this.PicBoxClicked);
            this.I8.MouseEnter += new System.EventHandler(this.ShowShip);
            this.I8.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // I7
            // 
            this.I7.BackColor = System.Drawing.Color.White;
            this.I7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I7.Location = new System.Drawing.Point(228, 342);
            this.I7.Name = "I7";
            this.I7.Size = new System.Drawing.Size(30, 30);
            this.I7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I7.TabIndex = 338;
            this.I7.TabStop = false;
            this.I7.Click += new System.EventHandler(this.PicBoxClicked);
            this.I7.MouseEnter += new System.EventHandler(this.ShowShip);
            this.I7.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // I6
            // 
            this.I6.BackColor = System.Drawing.Color.White;
            this.I6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I6.Location = new System.Drawing.Point(192, 342);
            this.I6.Name = "I6";
            this.I6.Size = new System.Drawing.Size(30, 30);
            this.I6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I6.TabIndex = 337;
            this.I6.TabStop = false;
            this.I6.Click += new System.EventHandler(this.PicBoxClicked);
            this.I6.MouseEnter += new System.EventHandler(this.ShowShip);
            this.I6.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // I5
            // 
            this.I5.BackColor = System.Drawing.Color.White;
            this.I5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I5.Location = new System.Drawing.Point(156, 342);
            this.I5.Name = "I5";
            this.I5.Size = new System.Drawing.Size(30, 30);
            this.I5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I5.TabIndex = 336;
            this.I5.TabStop = false;
            this.I5.Click += new System.EventHandler(this.PicBoxClicked);
            this.I5.MouseEnter += new System.EventHandler(this.ShowShip);
            this.I5.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // I4
            // 
            this.I4.BackColor = System.Drawing.Color.White;
            this.I4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I4.Location = new System.Drawing.Point(120, 342);
            this.I4.Name = "I4";
            this.I4.Size = new System.Drawing.Size(30, 30);
            this.I4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I4.TabIndex = 335;
            this.I4.TabStop = false;
            this.I4.Click += new System.EventHandler(this.PicBoxClicked);
            this.I4.MouseEnter += new System.EventHandler(this.ShowShip);
            this.I4.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // I3
            // 
            this.I3.BackColor = System.Drawing.Color.White;
            this.I3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I3.Location = new System.Drawing.Point(84, 342);
            this.I3.Name = "I3";
            this.I3.Size = new System.Drawing.Size(30, 30);
            this.I3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I3.TabIndex = 334;
            this.I3.TabStop = false;
            this.I3.Click += new System.EventHandler(this.PicBoxClicked);
            this.I3.MouseEnter += new System.EventHandler(this.ShowShip);
            this.I3.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // I2
            // 
            this.I2.BackColor = System.Drawing.Color.White;
            this.I2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I2.Location = new System.Drawing.Point(48, 342);
            this.I2.Name = "I2";
            this.I2.Size = new System.Drawing.Size(30, 30);
            this.I2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I2.TabIndex = 333;
            this.I2.TabStop = false;
            this.I2.Click += new System.EventHandler(this.PicBoxClicked);
            this.I2.MouseEnter += new System.EventHandler(this.ShowShip);
            this.I2.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // H10
            // 
            this.H10.BackColor = System.Drawing.Color.White;
            this.H10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.H10.Location = new System.Drawing.Point(336, 306);
            this.H10.Name = "H10";
            this.H10.Size = new System.Drawing.Size(30, 30);
            this.H10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H10.TabIndex = 332;
            this.H10.TabStop = false;
            this.H10.Click += new System.EventHandler(this.PicBoxClicked);
            this.H10.MouseEnter += new System.EventHandler(this.ShowShip);
            this.H10.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // H9
            // 
            this.H9.BackColor = System.Drawing.Color.White;
            this.H9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.H9.Location = new System.Drawing.Point(300, 306);
            this.H9.Name = "H9";
            this.H9.Size = new System.Drawing.Size(30, 30);
            this.H9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H9.TabIndex = 331;
            this.H9.TabStop = false;
            this.H9.Click += new System.EventHandler(this.PicBoxClicked);
            this.H9.MouseEnter += new System.EventHandler(this.ShowShip);
            this.H9.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // H8
            // 
            this.H8.BackColor = System.Drawing.Color.White;
            this.H8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.H8.Location = new System.Drawing.Point(264, 306);
            this.H8.Name = "H8";
            this.H8.Size = new System.Drawing.Size(30, 30);
            this.H8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H8.TabIndex = 330;
            this.H8.TabStop = false;
            this.H8.Click += new System.EventHandler(this.PicBoxClicked);
            this.H8.MouseEnter += new System.EventHandler(this.ShowShip);
            this.H8.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // H7
            // 
            this.H7.BackColor = System.Drawing.Color.White;
            this.H7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.H7.Location = new System.Drawing.Point(228, 306);
            this.H7.Name = "H7";
            this.H7.Size = new System.Drawing.Size(30, 30);
            this.H7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H7.TabIndex = 329;
            this.H7.TabStop = false;
            this.H7.Click += new System.EventHandler(this.PicBoxClicked);
            this.H7.MouseEnter += new System.EventHandler(this.ShowShip);
            this.H7.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // H6
            // 
            this.H6.BackColor = System.Drawing.Color.White;
            this.H6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.H6.Location = new System.Drawing.Point(192, 306);
            this.H6.Name = "H6";
            this.H6.Size = new System.Drawing.Size(30, 30);
            this.H6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H6.TabIndex = 328;
            this.H6.TabStop = false;
            this.H6.Click += new System.EventHandler(this.PicBoxClicked);
            this.H6.MouseEnter += new System.EventHandler(this.ShowShip);
            this.H6.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // H5
            // 
            this.H5.BackColor = System.Drawing.Color.White;
            this.H5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.H5.Location = new System.Drawing.Point(156, 306);
            this.H5.Name = "H5";
            this.H5.Size = new System.Drawing.Size(30, 30);
            this.H5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H5.TabIndex = 327;
            this.H5.TabStop = false;
            this.H5.Click += new System.EventHandler(this.PicBoxClicked);
            this.H5.MouseEnter += new System.EventHandler(this.ShowShip);
            this.H5.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // H4
            // 
            this.H4.BackColor = System.Drawing.Color.White;
            this.H4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.H4.Location = new System.Drawing.Point(120, 306);
            this.H4.Name = "H4";
            this.H4.Size = new System.Drawing.Size(30, 30);
            this.H4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H4.TabIndex = 326;
            this.H4.TabStop = false;
            this.H4.Click += new System.EventHandler(this.PicBoxClicked);
            this.H4.MouseEnter += new System.EventHandler(this.ShowShip);
            this.H4.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // H3
            // 
            this.H3.BackColor = System.Drawing.Color.White;
            this.H3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.H3.Location = new System.Drawing.Point(84, 306);
            this.H3.Name = "H3";
            this.H3.Size = new System.Drawing.Size(30, 30);
            this.H3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H3.TabIndex = 325;
            this.H3.TabStop = false;
            this.H3.Click += new System.EventHandler(this.PicBoxClicked);
            this.H3.MouseEnter += new System.EventHandler(this.ShowShip);
            this.H3.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // H2
            // 
            this.H2.BackColor = System.Drawing.Color.White;
            this.H2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.H2.Location = new System.Drawing.Point(48, 306);
            this.H2.Name = "H2";
            this.H2.Size = new System.Drawing.Size(30, 30);
            this.H2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H2.TabIndex = 324;
            this.H2.TabStop = false;
            this.H2.Click += new System.EventHandler(this.PicBoxClicked);
            this.H2.MouseEnter += new System.EventHandler(this.ShowShip);
            this.H2.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // G10
            // 
            this.G10.BackColor = System.Drawing.Color.White;
            this.G10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.G10.Location = new System.Drawing.Point(336, 270);
            this.G10.Name = "G10";
            this.G10.Size = new System.Drawing.Size(30, 30);
            this.G10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G10.TabIndex = 323;
            this.G10.TabStop = false;
            this.G10.Click += new System.EventHandler(this.PicBoxClicked);
            this.G10.MouseEnter += new System.EventHandler(this.ShowShip);
            this.G10.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // G9
            // 
            this.G9.BackColor = System.Drawing.Color.White;
            this.G9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.G9.Location = new System.Drawing.Point(300, 270);
            this.G9.Name = "G9";
            this.G9.Size = new System.Drawing.Size(30, 30);
            this.G9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G9.TabIndex = 322;
            this.G9.TabStop = false;
            this.G9.Click += new System.EventHandler(this.PicBoxClicked);
            this.G9.MouseEnter += new System.EventHandler(this.ShowShip);
            this.G9.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // G8
            // 
            this.G8.BackColor = System.Drawing.Color.White;
            this.G8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.G8.Location = new System.Drawing.Point(264, 270);
            this.G8.Name = "G8";
            this.G8.Size = new System.Drawing.Size(30, 30);
            this.G8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G8.TabIndex = 321;
            this.G8.TabStop = false;
            this.G8.Click += new System.EventHandler(this.PicBoxClicked);
            this.G8.MouseEnter += new System.EventHandler(this.ShowShip);
            this.G8.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // G7
            // 
            this.G7.BackColor = System.Drawing.Color.White;
            this.G7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.G7.Location = new System.Drawing.Point(228, 270);
            this.G7.Name = "G7";
            this.G7.Size = new System.Drawing.Size(30, 30);
            this.G7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G7.TabIndex = 320;
            this.G7.TabStop = false;
            this.G7.Click += new System.EventHandler(this.PicBoxClicked);
            this.G7.MouseEnter += new System.EventHandler(this.ShowShip);
            this.G7.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // G6
            // 
            this.G6.BackColor = System.Drawing.Color.White;
            this.G6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.G6.Location = new System.Drawing.Point(192, 270);
            this.G6.Name = "G6";
            this.G6.Size = new System.Drawing.Size(30, 30);
            this.G6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G6.TabIndex = 319;
            this.G6.TabStop = false;
            this.G6.Click += new System.EventHandler(this.PicBoxClicked);
            this.G6.MouseEnter += new System.EventHandler(this.ShowShip);
            this.G6.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // G5
            // 
            this.G5.BackColor = System.Drawing.Color.White;
            this.G5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.G5.Location = new System.Drawing.Point(156, 270);
            this.G5.Name = "G5";
            this.G5.Size = new System.Drawing.Size(30, 30);
            this.G5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G5.TabIndex = 318;
            this.G5.TabStop = false;
            this.G5.Click += new System.EventHandler(this.PicBoxClicked);
            this.G5.MouseEnter += new System.EventHandler(this.ShowShip);
            this.G5.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // G4
            // 
            this.G4.BackColor = System.Drawing.Color.White;
            this.G4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.G4.Location = new System.Drawing.Point(120, 270);
            this.G4.Name = "G4";
            this.G4.Size = new System.Drawing.Size(30, 30);
            this.G4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G4.TabIndex = 317;
            this.G4.TabStop = false;
            this.G4.Click += new System.EventHandler(this.PicBoxClicked);
            this.G4.MouseEnter += new System.EventHandler(this.ShowShip);
            this.G4.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // G3
            // 
            this.G3.BackColor = System.Drawing.Color.White;
            this.G3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.G3.Location = new System.Drawing.Point(84, 270);
            this.G3.Name = "G3";
            this.G3.Size = new System.Drawing.Size(30, 30);
            this.G3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G3.TabIndex = 316;
            this.G3.TabStop = false;
            this.G3.Click += new System.EventHandler(this.PicBoxClicked);
            this.G3.MouseEnter += new System.EventHandler(this.ShowShip);
            this.G3.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // G2
            // 
            this.G2.BackColor = System.Drawing.Color.White;
            this.G2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.G2.Location = new System.Drawing.Point(48, 270);
            this.G2.Name = "G2";
            this.G2.Size = new System.Drawing.Size(30, 30);
            this.G2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G2.TabIndex = 315;
            this.G2.TabStop = false;
            this.G2.Click += new System.EventHandler(this.PicBoxClicked);
            this.G2.MouseEnter += new System.EventHandler(this.ShowShip);
            this.G2.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // F10
            // 
            this.F10.BackColor = System.Drawing.Color.White;
            this.F10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.F10.Location = new System.Drawing.Point(336, 234);
            this.F10.Name = "F10";
            this.F10.Size = new System.Drawing.Size(30, 30);
            this.F10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F10.TabIndex = 314;
            this.F10.TabStop = false;
            this.F10.Click += new System.EventHandler(this.PicBoxClicked);
            this.F10.MouseEnter += new System.EventHandler(this.ShowShip);
            this.F10.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // F9
            // 
            this.F9.BackColor = System.Drawing.Color.White;
            this.F9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.F9.Location = new System.Drawing.Point(300, 234);
            this.F9.Name = "F9";
            this.F9.Size = new System.Drawing.Size(30, 30);
            this.F9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F9.TabIndex = 313;
            this.F9.TabStop = false;
            this.F9.Click += new System.EventHandler(this.PicBoxClicked);
            this.F9.MouseEnter += new System.EventHandler(this.ShowShip);
            this.F9.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // F8
            // 
            this.F8.BackColor = System.Drawing.Color.White;
            this.F8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.F8.Location = new System.Drawing.Point(264, 234);
            this.F8.Name = "F8";
            this.F8.Size = new System.Drawing.Size(30, 30);
            this.F8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F8.TabIndex = 312;
            this.F8.TabStop = false;
            this.F8.Click += new System.EventHandler(this.PicBoxClicked);
            this.F8.MouseEnter += new System.EventHandler(this.ShowShip);
            this.F8.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // F7
            // 
            this.F7.BackColor = System.Drawing.Color.White;
            this.F7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.F7.Location = new System.Drawing.Point(228, 234);
            this.F7.Name = "F7";
            this.F7.Size = new System.Drawing.Size(30, 30);
            this.F7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F7.TabIndex = 311;
            this.F7.TabStop = false;
            this.F7.Click += new System.EventHandler(this.PicBoxClicked);
            this.F7.MouseEnter += new System.EventHandler(this.ShowShip);
            this.F7.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // F6
            // 
            this.F6.BackColor = System.Drawing.Color.White;
            this.F6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.F6.Location = new System.Drawing.Point(192, 234);
            this.F6.Name = "F6";
            this.F6.Size = new System.Drawing.Size(30, 30);
            this.F6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F6.TabIndex = 310;
            this.F6.TabStop = false;
            this.F6.Click += new System.EventHandler(this.PicBoxClicked);
            this.F6.MouseEnter += new System.EventHandler(this.ShowShip);
            this.F6.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // F5
            // 
            this.F5.BackColor = System.Drawing.Color.White;
            this.F5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.F5.Location = new System.Drawing.Point(156, 234);
            this.F5.Name = "F5";
            this.F5.Size = new System.Drawing.Size(30, 30);
            this.F5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F5.TabIndex = 309;
            this.F5.TabStop = false;
            this.F5.Click += new System.EventHandler(this.PicBoxClicked);
            this.F5.MouseEnter += new System.EventHandler(this.ShowShip);
            this.F5.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // F4
            // 
            this.F4.BackColor = System.Drawing.Color.White;
            this.F4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.F4.Location = new System.Drawing.Point(120, 234);
            this.F4.Name = "F4";
            this.F4.Size = new System.Drawing.Size(30, 30);
            this.F4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F4.TabIndex = 308;
            this.F4.TabStop = false;
            this.F4.Click += new System.EventHandler(this.PicBoxClicked);
            this.F4.MouseEnter += new System.EventHandler(this.ShowShip);
            this.F4.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // F3
            // 
            this.F3.BackColor = System.Drawing.Color.White;
            this.F3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.F3.Location = new System.Drawing.Point(84, 234);
            this.F3.Name = "F3";
            this.F3.Size = new System.Drawing.Size(30, 30);
            this.F3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F3.TabIndex = 307;
            this.F3.TabStop = false;
            this.F3.Click += new System.EventHandler(this.PicBoxClicked);
            this.F3.MouseEnter += new System.EventHandler(this.ShowShip);
            this.F3.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // F2
            // 
            this.F2.BackColor = System.Drawing.Color.White;
            this.F2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.F2.Location = new System.Drawing.Point(48, 234);
            this.F2.Name = "F2";
            this.F2.Size = new System.Drawing.Size(30, 30);
            this.F2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F2.TabIndex = 306;
            this.F2.TabStop = false;
            this.F2.Click += new System.EventHandler(this.PicBoxClicked);
            this.F2.MouseEnter += new System.EventHandler(this.ShowShip);
            this.F2.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // E10
            // 
            this.E10.BackColor = System.Drawing.Color.White;
            this.E10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.E10.Location = new System.Drawing.Point(336, 198);
            this.E10.Name = "E10";
            this.E10.Size = new System.Drawing.Size(30, 30);
            this.E10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E10.TabIndex = 305;
            this.E10.TabStop = false;
            this.E10.Click += new System.EventHandler(this.PicBoxClicked);
            this.E10.MouseEnter += new System.EventHandler(this.ShowShip);
            this.E10.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // E9
            // 
            this.E9.BackColor = System.Drawing.Color.White;
            this.E9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.E9.Location = new System.Drawing.Point(300, 198);
            this.E9.Name = "E9";
            this.E9.Size = new System.Drawing.Size(30, 30);
            this.E9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E9.TabIndex = 304;
            this.E9.TabStop = false;
            this.E9.Click += new System.EventHandler(this.PicBoxClicked);
            this.E9.MouseEnter += new System.EventHandler(this.ShowShip);
            this.E9.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // E8
            // 
            this.E8.BackColor = System.Drawing.Color.White;
            this.E8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.E8.Location = new System.Drawing.Point(264, 198);
            this.E8.Name = "E8";
            this.E8.Size = new System.Drawing.Size(30, 30);
            this.E8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E8.TabIndex = 303;
            this.E8.TabStop = false;
            this.E8.Click += new System.EventHandler(this.PicBoxClicked);
            this.E8.MouseEnter += new System.EventHandler(this.ShowShip);
            this.E8.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // E7
            // 
            this.E7.BackColor = System.Drawing.Color.White;
            this.E7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.E7.Location = new System.Drawing.Point(228, 198);
            this.E7.Name = "E7";
            this.E7.Size = new System.Drawing.Size(30, 30);
            this.E7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E7.TabIndex = 302;
            this.E7.TabStop = false;
            this.E7.Click += new System.EventHandler(this.PicBoxClicked);
            this.E7.MouseEnter += new System.EventHandler(this.ShowShip);
            this.E7.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // E6
            // 
            this.E6.BackColor = System.Drawing.Color.White;
            this.E6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.E6.Location = new System.Drawing.Point(192, 198);
            this.E6.Name = "E6";
            this.E6.Size = new System.Drawing.Size(30, 30);
            this.E6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E6.TabIndex = 301;
            this.E6.TabStop = false;
            this.E6.Click += new System.EventHandler(this.PicBoxClicked);
            this.E6.MouseEnter += new System.EventHandler(this.ShowShip);
            this.E6.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // E5
            // 
            this.E5.BackColor = System.Drawing.Color.White;
            this.E5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.E5.Location = new System.Drawing.Point(156, 198);
            this.E5.Name = "E5";
            this.E5.Size = new System.Drawing.Size(30, 30);
            this.E5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E5.TabIndex = 300;
            this.E5.TabStop = false;
            this.E5.Click += new System.EventHandler(this.PicBoxClicked);
            this.E5.MouseEnter += new System.EventHandler(this.ShowShip);
            this.E5.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // E4
            // 
            this.E4.BackColor = System.Drawing.Color.White;
            this.E4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.E4.Location = new System.Drawing.Point(120, 198);
            this.E4.Name = "E4";
            this.E4.Size = new System.Drawing.Size(30, 30);
            this.E4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E4.TabIndex = 299;
            this.E4.TabStop = false;
            this.E4.Click += new System.EventHandler(this.PicBoxClicked);
            this.E4.MouseEnter += new System.EventHandler(this.ShowShip);
            this.E4.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // E3
            // 
            this.E3.BackColor = System.Drawing.Color.White;
            this.E3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.E3.Location = new System.Drawing.Point(84, 198);
            this.E3.Name = "E3";
            this.E3.Size = new System.Drawing.Size(30, 30);
            this.E3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E3.TabIndex = 298;
            this.E3.TabStop = false;
            this.E3.Click += new System.EventHandler(this.PicBoxClicked);
            this.E3.MouseEnter += new System.EventHandler(this.ShowShip);
            this.E3.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // E2
            // 
            this.E2.BackColor = System.Drawing.Color.White;
            this.E2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.E2.Location = new System.Drawing.Point(48, 198);
            this.E2.Name = "E2";
            this.E2.Size = new System.Drawing.Size(30, 30);
            this.E2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E2.TabIndex = 297;
            this.E2.TabStop = false;
            this.E2.Click += new System.EventHandler(this.PicBoxClicked);
            this.E2.MouseEnter += new System.EventHandler(this.ShowShip);
            this.E2.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // D10
            // 
            this.D10.BackColor = System.Drawing.Color.White;
            this.D10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.D10.Location = new System.Drawing.Point(336, 162);
            this.D10.Name = "D10";
            this.D10.Size = new System.Drawing.Size(30, 30);
            this.D10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D10.TabIndex = 296;
            this.D10.TabStop = false;
            this.D10.Click += new System.EventHandler(this.PicBoxClicked);
            this.D10.MouseEnter += new System.EventHandler(this.ShowShip);
            this.D10.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // D9
            // 
            this.D9.BackColor = System.Drawing.Color.White;
            this.D9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.D9.Location = new System.Drawing.Point(300, 162);
            this.D9.Name = "D9";
            this.D9.Size = new System.Drawing.Size(30, 30);
            this.D9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D9.TabIndex = 295;
            this.D9.TabStop = false;
            this.D9.Click += new System.EventHandler(this.PicBoxClicked);
            this.D9.MouseEnter += new System.EventHandler(this.ShowShip);
            this.D9.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // D8
            // 
            this.D8.BackColor = System.Drawing.Color.White;
            this.D8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.D8.Location = new System.Drawing.Point(264, 162);
            this.D8.Name = "D8";
            this.D8.Size = new System.Drawing.Size(30, 30);
            this.D8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D8.TabIndex = 294;
            this.D8.TabStop = false;
            this.D8.Click += new System.EventHandler(this.PicBoxClicked);
            this.D8.MouseEnter += new System.EventHandler(this.ShowShip);
            this.D8.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // D7
            // 
            this.D7.BackColor = System.Drawing.Color.White;
            this.D7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.D7.Location = new System.Drawing.Point(228, 162);
            this.D7.Name = "D7";
            this.D7.Size = new System.Drawing.Size(30, 30);
            this.D7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D7.TabIndex = 293;
            this.D7.TabStop = false;
            this.D7.Click += new System.EventHandler(this.PicBoxClicked);
            this.D7.MouseEnter += new System.EventHandler(this.ShowShip);
            this.D7.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // D6
            // 
            this.D6.BackColor = System.Drawing.Color.White;
            this.D6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.D6.Location = new System.Drawing.Point(192, 162);
            this.D6.Name = "D6";
            this.D6.Size = new System.Drawing.Size(30, 30);
            this.D6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D6.TabIndex = 292;
            this.D6.TabStop = false;
            this.D6.Click += new System.EventHandler(this.PicBoxClicked);
            this.D6.MouseEnter += new System.EventHandler(this.ShowShip);
            this.D6.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // D5
            // 
            this.D5.BackColor = System.Drawing.Color.White;
            this.D5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.D5.Location = new System.Drawing.Point(156, 162);
            this.D5.Name = "D5";
            this.D5.Size = new System.Drawing.Size(30, 30);
            this.D5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D5.TabIndex = 291;
            this.D5.TabStop = false;
            this.D5.Click += new System.EventHandler(this.PicBoxClicked);
            this.D5.MouseEnter += new System.EventHandler(this.ShowShip);
            this.D5.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // D4
            // 
            this.D4.BackColor = System.Drawing.Color.White;
            this.D4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.D4.Location = new System.Drawing.Point(120, 162);
            this.D4.Name = "D4";
            this.D4.Size = new System.Drawing.Size(30, 30);
            this.D4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D4.TabIndex = 290;
            this.D4.TabStop = false;
            this.D4.Click += new System.EventHandler(this.PicBoxClicked);
            this.D4.MouseEnter += new System.EventHandler(this.ShowShip);
            this.D4.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // D3
            // 
            this.D3.BackColor = System.Drawing.Color.White;
            this.D3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.D3.Location = new System.Drawing.Point(84, 162);
            this.D3.Name = "D3";
            this.D3.Size = new System.Drawing.Size(30, 30);
            this.D3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D3.TabIndex = 289;
            this.D3.TabStop = false;
            this.D3.Click += new System.EventHandler(this.PicBoxClicked);
            this.D3.MouseEnter += new System.EventHandler(this.ShowShip);
            this.D3.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // D2
            // 
            this.D2.BackColor = System.Drawing.Color.White;
            this.D2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.D2.Location = new System.Drawing.Point(48, 162);
            this.D2.Name = "D2";
            this.D2.Size = new System.Drawing.Size(30, 30);
            this.D2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D2.TabIndex = 288;
            this.D2.TabStop = false;
            this.D2.Click += new System.EventHandler(this.PicBoxClicked);
            this.D2.MouseEnter += new System.EventHandler(this.ShowShip);
            this.D2.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // C10
            // 
            this.C10.BackColor = System.Drawing.Color.White;
            this.C10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.C10.Location = new System.Drawing.Point(336, 126);
            this.C10.Name = "C10";
            this.C10.Size = new System.Drawing.Size(30, 30);
            this.C10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C10.TabIndex = 287;
            this.C10.TabStop = false;
            this.C10.Click += new System.EventHandler(this.PicBoxClicked);
            this.C10.MouseEnter += new System.EventHandler(this.ShowShip);
            this.C10.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // C9
            // 
            this.C9.BackColor = System.Drawing.Color.White;
            this.C9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.C9.Location = new System.Drawing.Point(300, 126);
            this.C9.Name = "C9";
            this.C9.Size = new System.Drawing.Size(30, 30);
            this.C9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C9.TabIndex = 286;
            this.C9.TabStop = false;
            this.C9.Click += new System.EventHandler(this.PicBoxClicked);
            this.C9.MouseEnter += new System.EventHandler(this.ShowShip);
            this.C9.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // C8
            // 
            this.C8.BackColor = System.Drawing.Color.White;
            this.C8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.C8.Location = new System.Drawing.Point(264, 126);
            this.C8.Name = "C8";
            this.C8.Size = new System.Drawing.Size(30, 30);
            this.C8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C8.TabIndex = 285;
            this.C8.TabStop = false;
            this.C8.Click += new System.EventHandler(this.PicBoxClicked);
            this.C8.MouseEnter += new System.EventHandler(this.ShowShip);
            this.C8.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // C7
            // 
            this.C7.BackColor = System.Drawing.Color.White;
            this.C7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.C7.Location = new System.Drawing.Point(228, 126);
            this.C7.Name = "C7";
            this.C7.Size = new System.Drawing.Size(30, 30);
            this.C7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C7.TabIndex = 284;
            this.C7.TabStop = false;
            this.C7.Click += new System.EventHandler(this.PicBoxClicked);
            this.C7.MouseEnter += new System.EventHandler(this.ShowShip);
            this.C7.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // C6
            // 
            this.C6.BackColor = System.Drawing.Color.White;
            this.C6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.C6.Location = new System.Drawing.Point(192, 126);
            this.C6.Name = "C6";
            this.C6.Size = new System.Drawing.Size(30, 30);
            this.C6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C6.TabIndex = 283;
            this.C6.TabStop = false;
            this.C6.Click += new System.EventHandler(this.PicBoxClicked);
            this.C6.MouseEnter += new System.EventHandler(this.ShowShip);
            this.C6.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // C5
            // 
            this.C5.BackColor = System.Drawing.Color.White;
            this.C5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.C5.Location = new System.Drawing.Point(156, 126);
            this.C5.Name = "C5";
            this.C5.Size = new System.Drawing.Size(30, 30);
            this.C5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C5.TabIndex = 282;
            this.C5.TabStop = false;
            this.C5.Click += new System.EventHandler(this.PicBoxClicked);
            this.C5.MouseEnter += new System.EventHandler(this.ShowShip);
            this.C5.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // C4
            // 
            this.C4.BackColor = System.Drawing.Color.White;
            this.C4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.C4.Location = new System.Drawing.Point(120, 126);
            this.C4.Name = "C4";
            this.C4.Size = new System.Drawing.Size(30, 30);
            this.C4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C4.TabIndex = 281;
            this.C4.TabStop = false;
            this.C4.Click += new System.EventHandler(this.PicBoxClicked);
            this.C4.MouseEnter += new System.EventHandler(this.ShowShip);
            this.C4.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // C3
            // 
            this.C3.BackColor = System.Drawing.Color.White;
            this.C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.C3.Location = new System.Drawing.Point(84, 126);
            this.C3.Name = "C3";
            this.C3.Size = new System.Drawing.Size(30, 30);
            this.C3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C3.TabIndex = 280;
            this.C3.TabStop = false;
            this.C3.Click += new System.EventHandler(this.PicBoxClicked);
            this.C3.MouseEnter += new System.EventHandler(this.ShowShip);
            this.C3.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // C2
            // 
            this.C2.BackColor = System.Drawing.Color.White;
            this.C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.C2.Location = new System.Drawing.Point(48, 126);
            this.C2.Name = "C2";
            this.C2.Size = new System.Drawing.Size(30, 30);
            this.C2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C2.TabIndex = 279;
            this.C2.TabStop = false;
            this.C2.Click += new System.EventHandler(this.PicBoxClicked);
            this.C2.MouseEnter += new System.EventHandler(this.ShowShip);
            this.C2.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // B10
            // 
            this.B10.BackColor = System.Drawing.Color.White;
            this.B10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.B10.Location = new System.Drawing.Point(336, 90);
            this.B10.Name = "B10";
            this.B10.Size = new System.Drawing.Size(30, 30);
            this.B10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B10.TabIndex = 278;
            this.B10.TabStop = false;
            this.B10.Click += new System.EventHandler(this.PicBoxClicked);
            this.B10.MouseEnter += new System.EventHandler(this.ShowShip);
            this.B10.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // B9
            // 
            this.B9.BackColor = System.Drawing.Color.White;
            this.B9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.B9.Location = new System.Drawing.Point(300, 90);
            this.B9.Name = "B9";
            this.B9.Size = new System.Drawing.Size(30, 30);
            this.B9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B9.TabIndex = 277;
            this.B9.TabStop = false;
            this.B9.Click += new System.EventHandler(this.PicBoxClicked);
            this.B9.MouseEnter += new System.EventHandler(this.ShowShip);
            this.B9.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // B8
            // 
            this.B8.BackColor = System.Drawing.Color.White;
            this.B8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.B8.Location = new System.Drawing.Point(264, 90);
            this.B8.Name = "B8";
            this.B8.Size = new System.Drawing.Size(30, 30);
            this.B8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B8.TabIndex = 276;
            this.B8.TabStop = false;
            this.B8.Click += new System.EventHandler(this.PicBoxClicked);
            this.B8.MouseEnter += new System.EventHandler(this.ShowShip);
            this.B8.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // B7
            // 
            this.B7.BackColor = System.Drawing.Color.White;
            this.B7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.B7.Location = new System.Drawing.Point(228, 90);
            this.B7.Name = "B7";
            this.B7.Size = new System.Drawing.Size(30, 30);
            this.B7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B7.TabIndex = 275;
            this.B7.TabStop = false;
            this.B7.Click += new System.EventHandler(this.PicBoxClicked);
            this.B7.MouseEnter += new System.EventHandler(this.ShowShip);
            this.B7.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // B6
            // 
            this.B6.BackColor = System.Drawing.Color.White;
            this.B6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.B6.Location = new System.Drawing.Point(192, 90);
            this.B6.Name = "B6";
            this.B6.Size = new System.Drawing.Size(30, 30);
            this.B6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B6.TabIndex = 274;
            this.B6.TabStop = false;
            this.B6.Click += new System.EventHandler(this.PicBoxClicked);
            this.B6.MouseEnter += new System.EventHandler(this.ShowShip);
            this.B6.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // B5
            // 
            this.B5.BackColor = System.Drawing.Color.White;
            this.B5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.B5.Location = new System.Drawing.Point(156, 90);
            this.B5.Name = "B5";
            this.B5.Size = new System.Drawing.Size(30, 30);
            this.B5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B5.TabIndex = 273;
            this.B5.TabStop = false;
            this.B5.Click += new System.EventHandler(this.PicBoxClicked);
            this.B5.MouseEnter += new System.EventHandler(this.ShowShip);
            this.B5.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // B4
            // 
            this.B4.BackColor = System.Drawing.Color.White;
            this.B4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.B4.Location = new System.Drawing.Point(120, 90);
            this.B4.Name = "B4";
            this.B4.Size = new System.Drawing.Size(30, 30);
            this.B4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B4.TabIndex = 272;
            this.B4.TabStop = false;
            this.B4.Click += new System.EventHandler(this.PicBoxClicked);
            this.B4.MouseEnter += new System.EventHandler(this.ShowShip);
            this.B4.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // B3
            // 
            this.B3.BackColor = System.Drawing.Color.White;
            this.B3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.B3.Location = new System.Drawing.Point(84, 90);
            this.B3.Name = "B3";
            this.B3.Size = new System.Drawing.Size(30, 30);
            this.B3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B3.TabIndex = 271;
            this.B3.TabStop = false;
            this.B3.Click += new System.EventHandler(this.PicBoxClicked);
            this.B3.MouseEnter += new System.EventHandler(this.ShowShip);
            this.B3.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // B2
            // 
            this.B2.BackColor = System.Drawing.Color.White;
            this.B2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.B2.Location = new System.Drawing.Point(48, 90);
            this.B2.Name = "B2";
            this.B2.Size = new System.Drawing.Size(30, 30);
            this.B2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B2.TabIndex = 270;
            this.B2.TabStop = false;
            this.B2.Click += new System.EventHandler(this.PicBoxClicked);
            this.B2.MouseEnter += new System.EventHandler(this.ShowShip);
            this.B2.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // J1
            // 
            this.J1.BackColor = System.Drawing.Color.White;
            this.J1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J1.Location = new System.Drawing.Point(12, 378);
            this.J1.Name = "J1";
            this.J1.Size = new System.Drawing.Size(30, 30);
            this.J1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.J1.TabIndex = 269;
            this.J1.TabStop = false;
            this.J1.Click += new System.EventHandler(this.PicBoxClicked);
            this.J1.MouseEnter += new System.EventHandler(this.ShowShip);
            this.J1.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // I1
            // 
            this.I1.BackColor = System.Drawing.Color.White;
            this.I1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I1.Location = new System.Drawing.Point(12, 342);
            this.I1.Name = "I1";
            this.I1.Size = new System.Drawing.Size(30, 30);
            this.I1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.I1.TabIndex = 268;
            this.I1.TabStop = false;
            this.I1.Click += new System.EventHandler(this.PicBoxClicked);
            this.I1.MouseEnter += new System.EventHandler(this.ShowShip);
            this.I1.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // H1
            // 
            this.H1.BackColor = System.Drawing.Color.White;
            this.H1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.H1.Location = new System.Drawing.Point(12, 306);
            this.H1.Name = "H1";
            this.H1.Size = new System.Drawing.Size(30, 30);
            this.H1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.H1.TabIndex = 267;
            this.H1.TabStop = false;
            this.H1.Click += new System.EventHandler(this.PicBoxClicked);
            this.H1.MouseEnter += new System.EventHandler(this.ShowShip);
            this.H1.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // G1
            // 
            this.G1.BackColor = System.Drawing.Color.White;
            this.G1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.G1.Location = new System.Drawing.Point(12, 270);
            this.G1.Name = "G1";
            this.G1.Size = new System.Drawing.Size(30, 30);
            this.G1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.G1.TabIndex = 266;
            this.G1.TabStop = false;
            this.G1.Click += new System.EventHandler(this.PicBoxClicked);
            this.G1.MouseEnter += new System.EventHandler(this.ShowShip);
            this.G1.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // F1
            // 
            this.F1.BackColor = System.Drawing.Color.White;
            this.F1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.F1.Location = new System.Drawing.Point(12, 234);
            this.F1.Name = "F1";
            this.F1.Size = new System.Drawing.Size(30, 30);
            this.F1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.F1.TabIndex = 265;
            this.F1.TabStop = false;
            this.F1.Click += new System.EventHandler(this.PicBoxClicked);
            this.F1.MouseEnter += new System.EventHandler(this.ShowShip);
            this.F1.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // E1
            // 
            this.E1.BackColor = System.Drawing.Color.White;
            this.E1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.E1.Location = new System.Drawing.Point(12, 198);
            this.E1.Name = "E1";
            this.E1.Size = new System.Drawing.Size(30, 30);
            this.E1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.E1.TabIndex = 264;
            this.E1.TabStop = false;
            this.E1.Click += new System.EventHandler(this.PicBoxClicked);
            this.E1.MouseEnter += new System.EventHandler(this.ShowShip);
            this.E1.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // D1
            // 
            this.D1.BackColor = System.Drawing.Color.White;
            this.D1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.D1.Location = new System.Drawing.Point(12, 162);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(30, 30);
            this.D1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D1.TabIndex = 263;
            this.D1.TabStop = false;
            this.D1.Click += new System.EventHandler(this.PicBoxClicked);
            this.D1.MouseEnter += new System.EventHandler(this.ShowShip);
            this.D1.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // C1
            // 
            this.C1.BackColor = System.Drawing.Color.White;
            this.C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.C1.Location = new System.Drawing.Point(12, 126);
            this.C1.Name = "C1";
            this.C1.Size = new System.Drawing.Size(30, 30);
            this.C1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.C1.TabIndex = 262;
            this.C1.TabStop = false;
            this.C1.Click += new System.EventHandler(this.PicBoxClicked);
            this.C1.MouseEnter += new System.EventHandler(this.ShowShip);
            this.C1.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // B1
            // 
            this.B1.BackColor = System.Drawing.Color.White;
            this.B1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.B1.Location = new System.Drawing.Point(12, 90);
            this.B1.Name = "B1";
            this.B1.Size = new System.Drawing.Size(30, 30);
            this.B1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.B1.TabIndex = 261;
            this.B1.TabStop = false;
            this.B1.Click += new System.EventHandler(this.PicBoxClicked);
            this.B1.MouseEnter += new System.EventHandler(this.ShowShip);
            this.B1.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // A10
            // 
            this.A10.BackColor = System.Drawing.Color.White;
            this.A10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.A10.Location = new System.Drawing.Point(336, 54);
            this.A10.Name = "A10";
            this.A10.Size = new System.Drawing.Size(30, 30);
            this.A10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A10.TabIndex = 260;
            this.A10.TabStop = false;
            this.A10.Click += new System.EventHandler(this.PicBoxClicked);
            this.A10.MouseEnter += new System.EventHandler(this.ShowShip);
            this.A10.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // A9
            // 
            this.A9.BackColor = System.Drawing.Color.White;
            this.A9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.A9.Location = new System.Drawing.Point(300, 54);
            this.A9.Name = "A9";
            this.A9.Size = new System.Drawing.Size(30, 30);
            this.A9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A9.TabIndex = 259;
            this.A9.TabStop = false;
            this.A9.Click += new System.EventHandler(this.PicBoxClicked);
            this.A9.MouseEnter += new System.EventHandler(this.ShowShip);
            this.A9.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // A8
            // 
            this.A8.BackColor = System.Drawing.Color.White;
            this.A8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.A8.Location = new System.Drawing.Point(264, 54);
            this.A8.Name = "A8";
            this.A8.Size = new System.Drawing.Size(30, 30);
            this.A8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A8.TabIndex = 258;
            this.A8.TabStop = false;
            this.A8.Click += new System.EventHandler(this.PicBoxClicked);
            this.A8.MouseEnter += new System.EventHandler(this.ShowShip);
            this.A8.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // A7
            // 
            this.A7.BackColor = System.Drawing.Color.White;
            this.A7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.A7.Location = new System.Drawing.Point(228, 54);
            this.A7.Name = "A7";
            this.A7.Size = new System.Drawing.Size(30, 30);
            this.A7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A7.TabIndex = 257;
            this.A7.TabStop = false;
            this.A7.Click += new System.EventHandler(this.PicBoxClicked);
            this.A7.MouseEnter += new System.EventHandler(this.ShowShip);
            this.A7.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // A6
            // 
            this.A6.BackColor = System.Drawing.Color.White;
            this.A6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.A6.Location = new System.Drawing.Point(192, 54);
            this.A6.Name = "A6";
            this.A6.Size = new System.Drawing.Size(30, 30);
            this.A6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A6.TabIndex = 256;
            this.A6.TabStop = false;
            this.A6.Click += new System.EventHandler(this.PicBoxClicked);
            this.A6.MouseEnter += new System.EventHandler(this.ShowShip);
            this.A6.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // A5
            // 
            this.A5.BackColor = System.Drawing.Color.White;
            this.A5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.A5.Location = new System.Drawing.Point(156, 54);
            this.A5.Name = "A5";
            this.A5.Size = new System.Drawing.Size(30, 30);
            this.A5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A5.TabIndex = 255;
            this.A5.TabStop = false;
            this.A5.Click += new System.EventHandler(this.PicBoxClicked);
            this.A5.MouseEnter += new System.EventHandler(this.ShowShip);
            this.A5.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // A4
            // 
            this.A4.BackColor = System.Drawing.Color.White;
            this.A4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.A4.Location = new System.Drawing.Point(120, 54);
            this.A4.Name = "A4";
            this.A4.Size = new System.Drawing.Size(30, 30);
            this.A4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A4.TabIndex = 254;
            this.A4.TabStop = false;
            this.A4.Click += new System.EventHandler(this.PicBoxClicked);
            this.A4.MouseEnter += new System.EventHandler(this.ShowShip);
            this.A4.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // A3
            // 
            this.A3.BackColor = System.Drawing.Color.White;
            this.A3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.A3.Location = new System.Drawing.Point(84, 54);
            this.A3.Name = "A3";
            this.A3.Size = new System.Drawing.Size(30, 30);
            this.A3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A3.TabIndex = 253;
            this.A3.TabStop = false;
            this.A3.Click += new System.EventHandler(this.PicBoxClicked);
            this.A3.MouseEnter += new System.EventHandler(this.ShowShip);
            this.A3.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // A2
            // 
            this.A2.BackColor = System.Drawing.Color.White;
            this.A2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.A2.Location = new System.Drawing.Point(48, 54);
            this.A2.Name = "A2";
            this.A2.Size = new System.Drawing.Size(29, 30);
            this.A2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A2.TabIndex = 252;
            this.A2.TabStop = false;
            this.A2.Click += new System.EventHandler(this.PicBoxClicked);
            this.A2.MouseEnter += new System.EventHandler(this.ShowShip);
            this.A2.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // A1
            // 
            this.A1.BackColor = System.Drawing.Color.White;
            this.A1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.A1.Location = new System.Drawing.Point(12, 54);
            this.A1.Name = "A1";
            this.A1.Size = new System.Drawing.Size(30, 30);
            this.A1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.A1.TabIndex = 251;
            this.A1.TabStop = false;
            this.A1.Click += new System.EventHandler(this.PicBoxClicked);
            this.A1.MouseEnter += new System.EventHandler(this.ShowShip);
            this.A1.MouseLeave += new System.EventHandler(this.HideShip);
            // 
            // btnRandom
            // 
            this.btnRandom.Location = new System.Drawing.Point(532, 354);
            this.btnRandom.Name = "btnRandom";
            this.btnRandom.Size = new System.Drawing.Size(98, 23);
            this.btnRandom.TabIndex = 359;
            this.btnRandom.Text = "Randomise Ships";
            this.btnRandom.UseVisualStyleBackColor = true;
            this.btnRandom.Click += new System.EventHandler(this.btnRandom_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(532, 384);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(75, 23);
            this.btnConfirm.TabIndex = 360;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Visible = false;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(443, -1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 19);
            this.label2.TabIndex = 361;
            this.label2.Text = "Ship Selected:";
            // 
            // lblShipName
            // 
            this.lblShipName.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShipName.Location = new System.Drawing.Point(441, 18);
            this.lblShipName.Name = "lblShipName";
            this.lblShipName.Size = new System.Drawing.Size(185, 35);
            this.lblShipName.TabIndex = 362;
            this.lblShipName.Text = "NONE";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 19);
            this.label3.TabIndex = 363;
            this.label3.Text = "Player:";
            // 
            // lblName
            // 
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(5, 18);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(289, 35);
            this.lblName.TabIndex = 364;
            this.lblName.Text = "TEST";
            // 
            // lblDiffy
            // 
            this.lblDiffy.AutoSize = true;
            this.lblDiffy.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiffy.Location = new System.Drawing.Point(292, 1);
            this.lblDiffy.Name = "lblDiffy";
            this.lblDiffy.Size = new System.Drawing.Size(74, 19);
            this.lblDiffy.TabIndex = 365;
            this.lblDiffy.Text = "Difficulty:";
            this.lblDiffy.Visible = false;
            // 
            // lblDifficulty
            // 
            this.lblDifficulty.BackColor = System.Drawing.Color.Transparent;
            this.lblDifficulty.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDifficulty.Location = new System.Drawing.Point(296, 20);
            this.lblDifficulty.Name = "lblDifficulty";
            this.lblDifficulty.Size = new System.Drawing.Size(126, 31);
            this.lblDifficulty.TabIndex = 366;
            this.lblDifficulty.Text = "TEST";
            this.lblDifficulty.Visible = false;
            // 
            // PlaceShip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 420);
            this.ControlBox = false;
            this.Controls.Add(this.lblDifficulty);
            this.Controls.Add(this.lblDiffy);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblShipName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.btnRandom);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnRotate);
            this.Controls.Add(this.shipSize3_2);
            this.Controls.Add(this.shipSize2);
            this.Controls.Add(this.shipSize3_1);
            this.Controls.Add(this.shipSize4);
            this.Controls.Add(this.shipSize5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.J10);
            this.Controls.Add(this.J9);
            this.Controls.Add(this.J8);
            this.Controls.Add(this.J7);
            this.Controls.Add(this.J6);
            this.Controls.Add(this.J5);
            this.Controls.Add(this.J4);
            this.Controls.Add(this.J3);
            this.Controls.Add(this.J2);
            this.Controls.Add(this.I10);
            this.Controls.Add(this.I9);
            this.Controls.Add(this.I8);
            this.Controls.Add(this.I7);
            this.Controls.Add(this.I6);
            this.Controls.Add(this.I5);
            this.Controls.Add(this.I4);
            this.Controls.Add(this.I3);
            this.Controls.Add(this.I2);
            this.Controls.Add(this.H10);
            this.Controls.Add(this.H9);
            this.Controls.Add(this.H8);
            this.Controls.Add(this.H7);
            this.Controls.Add(this.H6);
            this.Controls.Add(this.H5);
            this.Controls.Add(this.H4);
            this.Controls.Add(this.H3);
            this.Controls.Add(this.H2);
            this.Controls.Add(this.G10);
            this.Controls.Add(this.G9);
            this.Controls.Add(this.G8);
            this.Controls.Add(this.G7);
            this.Controls.Add(this.G6);
            this.Controls.Add(this.G5);
            this.Controls.Add(this.G4);
            this.Controls.Add(this.G3);
            this.Controls.Add(this.G2);
            this.Controls.Add(this.F10);
            this.Controls.Add(this.F9);
            this.Controls.Add(this.F8);
            this.Controls.Add(this.F7);
            this.Controls.Add(this.F6);
            this.Controls.Add(this.F5);
            this.Controls.Add(this.F4);
            this.Controls.Add(this.F3);
            this.Controls.Add(this.F2);
            this.Controls.Add(this.E10);
            this.Controls.Add(this.E9);
            this.Controls.Add(this.E8);
            this.Controls.Add(this.E7);
            this.Controls.Add(this.E6);
            this.Controls.Add(this.E5);
            this.Controls.Add(this.E4);
            this.Controls.Add(this.E3);
            this.Controls.Add(this.E2);
            this.Controls.Add(this.D10);
            this.Controls.Add(this.D9);
            this.Controls.Add(this.D8);
            this.Controls.Add(this.D7);
            this.Controls.Add(this.D6);
            this.Controls.Add(this.D5);
            this.Controls.Add(this.D4);
            this.Controls.Add(this.D3);
            this.Controls.Add(this.D2);
            this.Controls.Add(this.C10);
            this.Controls.Add(this.C9);
            this.Controls.Add(this.C8);
            this.Controls.Add(this.C7);
            this.Controls.Add(this.C6);
            this.Controls.Add(this.C5);
            this.Controls.Add(this.C4);
            this.Controls.Add(this.C3);
            this.Controls.Add(this.C2);
            this.Controls.Add(this.B10);
            this.Controls.Add(this.B9);
            this.Controls.Add(this.B8);
            this.Controls.Add(this.B7);
            this.Controls.Add(this.B6);
            this.Controls.Add(this.B5);
            this.Controls.Add(this.B4);
            this.Controls.Add(this.B3);
            this.Controls.Add(this.B2);
            this.Controls.Add(this.J1);
            this.Controls.Add(this.I1);
            this.Controls.Add(this.H1);
            this.Controls.Add(this.G1);
            this.Controls.Add(this.F1);
            this.Controls.Add(this.E1);
            this.Controls.Add(this.D1);
            this.Controls.Add(this.C1);
            this.Controls.Add(this.B1);
            this.Controls.Add(this.A10);
            this.Controls.Add(this.A9);
            this.Controls.Add(this.A8);
            this.Controls.Add(this.A7);
            this.Controls.Add(this.A6);
            this.Controls.Add(this.A5);
            this.Controls.Add(this.A4);
            this.Controls.Add(this.A3);
            this.Controls.Add(this.A2);
            this.Controls.Add(this.A1);
            this.Name = "PlaceShip";
            this.Text = "PlaceShip";
            ((System.ComponentModel.ISupportInitialize)(this.shipSize3_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shipSize2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shipSize3_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shipSize4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shipSize5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.J1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.I1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.F1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.E1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox A1;
        private System.Windows.Forms.PictureBox A2;
        private System.Windows.Forms.PictureBox A3;
        private System.Windows.Forms.PictureBox A4;
        private System.Windows.Forms.PictureBox A5;
        private System.Windows.Forms.PictureBox A6;
        private System.Windows.Forms.PictureBox A7;
        private System.Windows.Forms.PictureBox A8;
        private System.Windows.Forms.PictureBox A9;
        private System.Windows.Forms.PictureBox A10;
        private System.Windows.Forms.PictureBox B1;
        private System.Windows.Forms.PictureBox C1;
        private System.Windows.Forms.PictureBox D1;
        private System.Windows.Forms.PictureBox E1;
        private System.Windows.Forms.PictureBox F1;
        private System.Windows.Forms.PictureBox G1;
        private System.Windows.Forms.PictureBox H1;
        private System.Windows.Forms.PictureBox I1;
        private System.Windows.Forms.PictureBox J1;
        private System.Windows.Forms.PictureBox B2;
        private System.Windows.Forms.PictureBox B3;
        private System.Windows.Forms.PictureBox B4;
        private System.Windows.Forms.PictureBox B5;
        private System.Windows.Forms.PictureBox B6;
        private System.Windows.Forms.PictureBox B7;
        private System.Windows.Forms.PictureBox B8;
        private System.Windows.Forms.PictureBox B9;
        private System.Windows.Forms.PictureBox B10;
        private System.Windows.Forms.PictureBox C2;
        private System.Windows.Forms.PictureBox C3;
        private System.Windows.Forms.PictureBox C4;
        private System.Windows.Forms.PictureBox C5;
        private System.Windows.Forms.PictureBox C6;
        private System.Windows.Forms.PictureBox C7;
        private System.Windows.Forms.PictureBox C8;
        private System.Windows.Forms.PictureBox C9;
        private System.Windows.Forms.PictureBox C10;
        private System.Windows.Forms.PictureBox D2;
        private System.Windows.Forms.PictureBox D3;
        private System.Windows.Forms.PictureBox D4;
        private System.Windows.Forms.PictureBox D5;
        private System.Windows.Forms.PictureBox D6;
        private System.Windows.Forms.PictureBox D7;
        private System.Windows.Forms.PictureBox D8;
        private System.Windows.Forms.PictureBox D9;
        private System.Windows.Forms.PictureBox D10;
        private System.Windows.Forms.PictureBox E2;
        private System.Windows.Forms.PictureBox E3;
        private System.Windows.Forms.PictureBox E4;
        private System.Windows.Forms.PictureBox E5;
        private System.Windows.Forms.PictureBox E6;
        private System.Windows.Forms.PictureBox E7;
        private System.Windows.Forms.PictureBox E8;
        private System.Windows.Forms.PictureBox E9;
        private System.Windows.Forms.PictureBox E10;
        private System.Windows.Forms.PictureBox F2;
        private System.Windows.Forms.PictureBox F3;
        private System.Windows.Forms.PictureBox F4;
        private System.Windows.Forms.PictureBox F5;
        private System.Windows.Forms.PictureBox F6;
        private System.Windows.Forms.PictureBox F7;
        private System.Windows.Forms.PictureBox F8;
        private System.Windows.Forms.PictureBox F9;
        private System.Windows.Forms.PictureBox F10;
        private System.Windows.Forms.PictureBox G2;
        private System.Windows.Forms.PictureBox G3;
        private System.Windows.Forms.PictureBox G4;
        private System.Windows.Forms.PictureBox G5;
        private System.Windows.Forms.PictureBox G6;
        private System.Windows.Forms.PictureBox G7;
        private System.Windows.Forms.PictureBox G8;
        private System.Windows.Forms.PictureBox G9;
        private System.Windows.Forms.PictureBox G10;
        private System.Windows.Forms.PictureBox H2;
        private System.Windows.Forms.PictureBox H3;
        private System.Windows.Forms.PictureBox H4;
        private System.Windows.Forms.PictureBox H5;
        private System.Windows.Forms.PictureBox H6;
        private System.Windows.Forms.PictureBox H7;
        private System.Windows.Forms.PictureBox H8;
        private System.Windows.Forms.PictureBox H9;
        private System.Windows.Forms.PictureBox H10;
        private System.Windows.Forms.PictureBox I2;
        private System.Windows.Forms.PictureBox I3;
        private System.Windows.Forms.PictureBox I4;
        private System.Windows.Forms.PictureBox I5;
        private System.Windows.Forms.PictureBox I6;
        private System.Windows.Forms.PictureBox I7;
        private System.Windows.Forms.PictureBox I8;
        private System.Windows.Forms.PictureBox I9;
        private System.Windows.Forms.PictureBox I10;
        private System.Windows.Forms.PictureBox J2;
        private System.Windows.Forms.PictureBox J3;
        private System.Windows.Forms.PictureBox J4;
        private System.Windows.Forms.PictureBox J5;
        private System.Windows.Forms.PictureBox J6;
        private System.Windows.Forms.PictureBox J7;
        private System.Windows.Forms.PictureBox J8;
        private System.Windows.Forms.PictureBox J9;
        private System.Windows.Forms.PictureBox J10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox shipSize5;
        private System.Windows.Forms.PictureBox shipSize4;
        private System.Windows.Forms.PictureBox shipSize3_1;
        private System.Windows.Forms.PictureBox shipSize2;
        private System.Windows.Forms.PictureBox shipSize3_2;
        private System.Windows.Forms.Button btnRotate;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnRandom;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblShipName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblDiffy;
        private System.Windows.Forms.Label lblDifficulty;
    }
}