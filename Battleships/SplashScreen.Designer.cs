﻿namespace Battleships
{
    partial class SplashScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPlayVsAi = new System.Windows.Forms.Button();
            this.btnPlayerVsPlayerLocal = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnPlayerVsPlayerOnline = new System.Windows.Forms.Button();
            this.btn4PlayerOnline = new System.Windows.Forms.Button();
            this.picQuit = new System.Windows.Forms.PictureBox();
            this.pic_SplashScreen = new System.Windows.Forms.PictureBox();
            this.picVolume = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picQuit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_SplashScreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVolume)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPlayVsAi
            // 
            this.btnPlayVsAi.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlayVsAi.Location = new System.Drawing.Point(20, 600);
            this.btnPlayVsAi.Name = "btnPlayVsAi";
            this.btnPlayVsAi.Size = new System.Drawing.Size(160, 50);
            this.btnPlayVsAi.TabIndex = 1;
            this.btnPlayVsAi.Text = "Player VS AI";
            this.btnPlayVsAi.UseVisualStyleBackColor = true;
            this.btnPlayVsAi.Click += new System.EventHandler(this.btn_PlayVsAi_Click);
            // 
            // btnPlayerVsPlayerLocal
            // 
            this.btnPlayerVsPlayerLocal.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlayerVsPlayerLocal.Location = new System.Drawing.Point(200, 600);
            this.btnPlayerVsPlayerLocal.Name = "btnPlayerVsPlayerLocal";
            this.btnPlayerVsPlayerLocal.Size = new System.Drawing.Size(160, 50);
            this.btnPlayerVsPlayerLocal.TabIndex = 4;
            this.btnPlayerVsPlayerLocal.Text = "2 Player Local";
            this.btnPlayerVsPlayerLocal.UseVisualStyleBackColor = true;
            this.btnPlayerVsPlayerLocal.Click += new System.EventHandler(this.btn_PlayerVsPlayerLocal_Click);
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(900, 562);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(50, 23);
            this.btnTest.TabIndex = 5;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Visible = false;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnPlayerVsPlayerOnline
            // 
            this.btnPlayerVsPlayerOnline.Enabled = false;
            this.btnPlayerVsPlayerOnline.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlayerVsPlayerOnline.Location = new System.Drawing.Point(380, 600);
            this.btnPlayerVsPlayerOnline.Name = "btnPlayerVsPlayerOnline";
            this.btnPlayerVsPlayerOnline.Size = new System.Drawing.Size(160, 50);
            this.btnPlayerVsPlayerOnline.TabIndex = 6;
            this.btnPlayerVsPlayerOnline.Text = "COMING SOON";
            this.btnPlayerVsPlayerOnline.UseVisualStyleBackColor = true;
            this.btnPlayerVsPlayerOnline.Click += new System.EventHandler(this.btnPlayerVsPlayerOnline_Click);
            // 
            // btn4PlayerOnline
            // 
            this.btn4PlayerOnline.Enabled = false;
            this.btn4PlayerOnline.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4PlayerOnline.Location = new System.Drawing.Point(560, 600);
            this.btn4PlayerOnline.Name = "btn4PlayerOnline";
            this.btn4PlayerOnline.Size = new System.Drawing.Size(160, 50);
            this.btn4PlayerOnline.TabIndex = 7;
            this.btn4PlayerOnline.Text = "COMING SOON";
            this.btn4PlayerOnline.UseVisualStyleBackColor = true;
            // 
            // picQuit
            // 
            this.picQuit.Image = global::Battleships.Properties.Resources.Quit;
            this.picQuit.Location = new System.Drawing.Point(900, 600);
            this.picQuit.Name = "picQuit";
            this.picQuit.Size = new System.Drawing.Size(50, 50);
            this.picQuit.TabIndex = 2;
            this.picQuit.TabStop = false;
            this.picQuit.Click += new System.EventHandler(this.picQuit_Click_1);
            this.picQuit.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picQuit_MouseClick_1);
            this.picQuit.MouseEnter += new System.EventHandler(this.picQuit_MouseEnter);
            this.picQuit.MouseLeave += new System.EventHandler(this.picQuit_MouseLeave);
            // 
            // pic_SplashScreen
            // 
            this.pic_SplashScreen.Image = global::Battleships.Properties.Resources.SplashScreen;
            this.pic_SplashScreen.Location = new System.Drawing.Point(-2, -2);
            this.pic_SplashScreen.Name = "pic_SplashScreen";
            this.pic_SplashScreen.Size = new System.Drawing.Size(975, 675);
            this.pic_SplashScreen.TabIndex = 0;
            this.pic_SplashScreen.TabStop = false;
            // 
            // picVolume
            // 
            this.picVolume.Image = global::Battleships.Properties.Resources.VolumeOn;
            this.picVolume.Location = new System.Drawing.Point(830, 600);
            this.picVolume.Name = "picVolume";
            this.picVolume.Size = new System.Drawing.Size(50, 50);
            this.picVolume.TabIndex = 8;
            this.picVolume.TabStop = false;
            this.picVolume.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picVolume_MouseDown);
            this.picVolume.MouseEnter += new System.EventHandler(this.picVolume_MouseEnter);
            this.picVolume.MouseLeave += new System.EventHandler(this.picVolume_MouseLeave);
            this.picVolume.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picVolume_MouseUp);
            // 
            // SplashScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 672);
            this.ControlBox = false;
            this.Controls.Add(this.picVolume);
            this.Controls.Add(this.btn4PlayerOnline);
            this.Controls.Add(this.btnPlayerVsPlayerOnline);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.btnPlayerVsPlayerLocal);
            this.Controls.Add(this.picQuit);
            this.Controls.Add(this.btnPlayVsAi);
            this.Controls.Add(this.pic_SplashScreen);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SplashScreen";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Anh\'s Battleships";
            this.Load += new System.EventHandler(this.SplashScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picQuit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_SplashScreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVolume)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pic_SplashScreen;
        private System.Windows.Forms.Button btnPlayVsAi;
        private System.Windows.Forms.PictureBox picQuit;
        private System.Windows.Forms.Button btnPlayerVsPlayerLocal;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Button btnPlayerVsPlayerOnline;
        private System.Windows.Forms.Button btn4PlayerOnline;
        private System.Windows.Forms.PictureBox picVolume;
    }
}