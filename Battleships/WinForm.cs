﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.Windows.Forms;

namespace Battleships
{
    public partial class WinForm : Form
    {
        public WinForm(String pPlayerName,Boolean disconnection, Boolean loss, Boolean mute)
        {
            //populates the Label when a player has sunk all of the other players ships
            InitializeComponent();
            SoundPlayer player = new SoundPlayer();
            player.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + "TaDa.wav";
            picBackground.Image = Properties.Resources.GoldBackground;

            if (loss == true)
            {
                picBackground.Image = Properties.Resources.LossScreen;

                if (disconnection == true)
                {
                    lblDisplay.Text = "Defeated! You have disconnected.";
                }

                else
                {
                    lblDisplay.Text = "Defeated! Your opponent, " + pPlayerName + ", has annihilated your entire fleet!";
                }
            }

            else if (disconnection == true)
            {
                lblDisplay.Text = "Congratulations " + pPlayerName + ", your opponent has disconnected!";
            }

            else
            {
                lblDisplay.Text = "Congratulations " + pPlayerName + ", you have annihilated the entire enemy fleet!";
            }
            
            if (mute == false)
            {
                player.Play();
            }
        }

        //Closes the form
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
