﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships
{
    public partial class ChallengePopup : Form
    {
        public ChallengePopup(string strPOppententname)
        {
            InitializeComponent();
            lblText.Text = strPOppententname + " has challenged you!";
        }

        public bool boolIsAccept;
        int intPlayerID;
        GameHandler GameH = new GameHandler();

        private void btnAccept_Click(object sender, EventArgs e)
        {
            boolIsAccept = true;
            GameH.PostIDintoEventPlayer(intPlayerID);
            DialogResult = DialogResult.OK;
        }

        private void btnDecline_Click(object sender, EventArgs e)
        {
            boolIsAccept = false;
            DialogResult = DialogResult.OK;
        }

        public void SetID(int intPPlayer)
        {
            intPlayerID = intPPlayer;
        }
    }
}
