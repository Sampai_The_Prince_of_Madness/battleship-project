﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships
{
    public class Game
    {
        public Dictionary<string, int> gameBoard1 = new Dictionary<string, int>();
        public Dictionary<string, int> gameBoard2 = new Dictionary<string, int>();
        List<string> strListHitNMissCoords1 = new List<string>();
        List<string> strListHitNMissCoords2 = new List<string>();
        List<string> strListInvalidCoord = new List<string>();
        List<string> strListAlreadyFiredUpon = new List<string>();

        public Game()
        {

        }

        // Pass argument is the String position (A1) // Do not pass index value
        public int registerHit(string position, int intNumOfThePlayer)
        {
            //Based on which player is chosen, it add the shot to the list of shots fired and
            //then returns if their is a ship in that position
            if (intNumOfThePlayer == 1)
            {
                strListHitNMissCoords2.Add(position);
                return gameBoard2[position];
            }

            else
            {
                strListHitNMissCoords1.Add(position);
                return gameBoard1[position];
            }          
        }

        //Fully populates the gameboard with blank coordinates
        public void PopulateGameBoard(int intNumOfThePlayer)
        {
            string strStringHolder;
            //cn stands for the "number" that it represents on the board
            for (int cn = 1; cn <= 10; cn++)
            {
                strStringHolder = "";
                //cn stands for the "letter" that it represents on the board
                for (int cl = 1; cl <= 10; cl++)
                {
                    strStringHolder = Number2String(cl, true) + cn.ToString();

                    if (intNumOfThePlayer == 1)
                    {
                        gameBoard1.Add(strStringHolder, 0);
                    }

                    else
                    {
                        gameBoard2.Add(strStringHolder, 0);
                    }   
                }
            }
        }

        //Generates a List string of coordinates for the ship size inputted
        public List<string> RandomPlacementGenerator(int intShipSize)
        {
            List<string> strListRandCood = new List<string>();
            List<string> strListUnverifedCoords = new List<string>();
            int Seed = (int)DateTime.Now.Ticks;
            Random rnd = new Random(Seed);

            int intValid = 0;
            int intNumber = rnd.Next(1, 11);
            int intLetter = rnd.Next(65, 75);
            int intShipOrientation = rnd.Next(1, 3);
            int intDirection = -1;
            int intRndNum = intNumber;
            int intRndLetter = intLetter;
            string strCoordHolder;
            bool boolValid = false;
            bool boolShipOrientation;


            //While the ship does not have a valid string of coordinates 
            while (boolValid == false)
            {
                strListUnverifedCoords = new List<string>();

                intValid = 0;
                intNumber = rnd.Next(1, 11);
                intRndNum = intNumber;
                intLetter = rnd.Next(65, 75);
                intShipOrientation = rnd.Next(1, 3);
                intRndLetter = intLetter;
                intDirection = -1;

                //Puts together randomly 
                strCoordHolder = ((char)intLetter).ToString() + intNumber.ToString();
                strListUnverifedCoords.Add(strCoordHolder);

                //Randoms between 1 & 2, if 1 sets orinentation to true
                if (rnd.Next(1, 3) == 1)
                {
                    boolShipOrientation = true;
                }

                //Otherwise it's set to false
                else
                {
                    boolShipOrientation = false;
                }

                strListUnverifedCoords = ReturnShipDisplay(strCoordHolder, intShipSize, boolShipOrientation);

                //goes through each unverifed coordinate and makes sure they don't already exist
                for (int c = 0; c < strListInvalidCoord.Count; c++)
                {
                    for (int b = 0; b < strListUnverifedCoords.Count; b++)
                    {
                        if (strListInvalidCoord[c] == strListUnverifedCoords[b])
                        {
                            intValid++;
                            boolValid = false;
                        }
                    }
                }

                if(intValid == 0)
                {
                    boolValid = true;
                }
            }

            //If the list checks out, they are added to the list of invaild coordinates and then returned for further use
            for(int c = 0; c < strListUnverifedCoords.Count; c++)
            {
                strListInvalidCoord.Add(strListUnverifedCoords[c]);
            }
            strListRandCood = strListUnverifedCoords;

            return strListRandCood;
        }

        //Generates a random coordinate to fire upon
        public string RandomAiFire(Boolean notFirstHit, Boolean hitShip, Boolean shipStillAlive, string startHold, string shipName, string startingCoord, string positionHold, int orientation, string aiSwitch, string difficulty)
        {
            //This will generate a position on the board for the AI to fire upon
            string position = "";
            bool Spin = false;
            int strLetter = 0;
            int compareLetter = 0;
            int strNumber = 0;
            int compareNumber = 0;
            Random rnd = new Random();

            if (difficulty == "Easy")
            {
                //fires randomly regardless of if it hits a ship
                position = RandomFire(Spin, position);
            }
            else
            {
                //sets coordinate for the current ai turn
                if (startingCoord != null)
                {
                    strLetter = Int32.Parse(string.Concat(positionHold[0], positionHold[1]));
                    if (positionHold.Length > 3)
                    {
                        //if number is 10
                        strNumber = 10;
                   }
                    else
                    {
                        //if Number is not 10
                        strNumber = Int32.Parse(positionHold[2].ToString());
                    }

                    //calculates orientation for next shot based on the initial hit of a ship
                    if (shipStillAlive == true)
                    {
                        //if hasn't missed the ship yet
                        if (aiSwitch == null)
                        {
                            compareLetter = Int32.Parse(string.Concat(startingCoord[0], startingCoord[1]));
                            if (startingCoord.Length > 3)
                            {
                                //if number is 10
                                compareNumber = 10;
                            }
                            else
                            {
                                //if Number is not 10
                                compareNumber = Int32.Parse(startingCoord[2].ToString());
                            }
                        }
                        //has missed the ship after a successful hit streak
                        else
                        {
                            compareLetter = Int32.Parse(string.Concat(aiSwitch[0], aiSwitch[1]));
                            if (aiSwitch.Length > 3)
                            {
                                //if number is 10
                                compareNumber = 10;
                            }
                            else
                            {
                                //if Number is not 10
                                compareNumber = Int32.Parse(aiSwitch[2].ToString());
                            }
                        }

                        //compares the strLeter/strNumber to the compareLetter/compareNumber to work out the current orientation 
                        if (strLetter < compareLetter)
                        {
                            //North
                            orientation = 1;

                            if (aiSwitch != null)
                            {
                                //Switches direction
                                orientation = 3;
                            }
                        }
                        else if (strLetter > compareLetter)
                        {
                            //South
                            orientation = 3;

                            if (aiSwitch != null)
                            {
                                //Switches direction
                                orientation = 1;
                            }
                        }
                        else if (strNumber > compareNumber)
                        {
                            //East
                            orientation = 2;

                            if (aiSwitch != null)
                            {
                                //Switches direction
                                orientation = 4;
                            }
                        }
                        else if (strNumber < compareNumber)
                        {
                            //West
                            orientation = 4;

                            if (aiSwitch != null)
                            {
                                //Switches direction
                                orientation = 2;
                            }
                        }
                        else
                        {
                            //if previous "hit" is the same as the first "hit" of the ship
                            orientation = 0;
                        }
                    }
                }

                if (hitShip == false && shipStillAlive == false)
                {
                    //Randomly fire untill a ship is hit or after a ship has been sunk and no other ships have been hit
                    position = RandomFire(Spin, position);
                }

                else if (hitShip == false && shipStillAlive == true)
                {
                    //If the previous shot was a miss after a ship has been hit but NOT sunk
                    position = AiOrientationSwitch(positionHold, position, strLetter, strNumber, startingCoord, orientation);
                }

                else if (hitShip == true)
                {
                    //If the previous shot was a hit and the ship hasn;t sunk
                    Spin = false;

                    while (Spin == false)
                    {
                        if (orientation == 0)
                        {
                            //selects a direction to fire upon after the first hit of a ship
                            orientation = rnd.Next(1, 5);
                        }
                        if (orientation == 1)
                        {
                            //North
                            strLetter = strLetter - 1;
                            position = ((char)strLetter).ToString() + strNumber.ToString();

                            if (strListAlreadyFiredUpon.Contains(position) && notFirstHit == true || strLetter < 65 && notFirstHit == true)
                            {
                                //If the ship has been hit multiple times in a row and the next position is either already hit or its off the board
                                position = AiOrientationSwitch(positionHold, position, strLetter, strNumber, startingCoord, orientation);
                                Spin = true;
                            }
                            else if (strListAlreadyFiredUpon.Contains(position) && notFirstHit == false || strLetter < 65 && notFirstHit == false)
                            {
                                //if the ship has been hit once and the position slected is filled or off the board, the direction will be reselected
                                Spin = false;
                                strLetter = strLetter + 1;
                                orientation = 0;
                            }
                            else
                            {
                                //Next position is clear and will be fired upon
                                Spin = true;
                                strListAlreadyFiredUpon.Add(position);
                            }
                        }
                        else if (orientation == 2)
                        {
                            //East
                            strNumber = strNumber + 1;
                            position = ((char)strLetter).ToString() + strNumber.ToString();
                            if (strListAlreadyFiredUpon.Contains(position) && notFirstHit == true || strNumber > 10 && notFirstHit == true)
                            {
                                //If the ship has been hit multiple times in a row and the next position is either already hit or its off the board
                                position = AiOrientationSwitch(positionHold, position, strLetter, strNumber, startingCoord, orientation);
                                Spin = true;
                            }
                            else if (strListAlreadyFiredUpon.Contains(position) && notFirstHit == false || strNumber > 10 && notFirstHit == false)
                            {
                                //if the ship has been hit once and the position slected is filled or off the board, the direction will be reselected
                                Spin = false;
                                strNumber = strNumber - 1;
                                orientation = 0;
                            }
                            else
                            {
                                //Next position is clear and will be fired upon
                                Spin = true;
                                strListAlreadyFiredUpon.Add(position);
                            }
                        }                        
                        else if (orientation == 3)
                        {
                            //South
                            strLetter = strLetter + 1;
                            position = ((char)strLetter).ToString() + strNumber.ToString();

                            if (strListAlreadyFiredUpon.Contains(position) && notFirstHit == true || strLetter > 74 && notFirstHit == true)
                            {
                                //If the ship has been hit multiple times in a row and the next position is either already hit or its off the board
                                position = AiOrientationSwitch(positionHold, position, strLetter, strNumber, startingCoord, orientation);
                                Spin = true;
                            }
                            else if (strListAlreadyFiredUpon.Contains(position) && notFirstHit == false || strLetter > 74 && notFirstHit == false)
                            {
                                //if the ship has been hit once and the position slected is filled or off the board, the direction will be reselected
                                Spin = false;
                                strLetter = strLetter - 1;
                                orientation = 0;
                            }
                            else
                            {
                                //Next position is clear and will be fired upon
                                Spin = true;
                                strListAlreadyFiredUpon.Add(position);
                            }
                        }                        //West
                        else if (orientation == 4)
                        {
                            //West
                            strNumber = strNumber - 1;
                            position = ((char)strLetter).ToString() + strNumber.ToString();
                            if (strListAlreadyFiredUpon.Contains(position) && notFirstHit == true || strNumber < 1 && notFirstHit == true)
                            {
                                //If the ship has been hit multiple times in a row and the next position is either already hit or its off the board
                                position = AiOrientationSwitch(positionHold, position, strLetter, strNumber, startingCoord, orientation);
                                Spin = true;
                            }
                            else if (strListAlreadyFiredUpon.Contains(position) && notFirstHit == false || strNumber < 1 && notFirstHit == false)
                            {
                                //if the ship has been hit once and the position slected is filled or off the board, the direction will be reselected
                                Spin = false;
                                strNumber = strNumber + 1;
                                orientation = 0;
                            }
                            else
                            {
                                //Next position is clear and will be fired upon
                                Spin = true;
                                strListAlreadyFiredUpon.Add(position);
                            }
                        }
                    }
                }
            }
            //Passes the position so that it can be used throughout the Game Class
            return position;
        }

        public string AiOrientationSwitch(string positionHold, string position, int strLetter, int strNumber, string startingCoord, int orientation)
        {
            Random rnd = new Random();
            
            //Sets the next shot coordinates back to the coordinates of the first hit of the ship
            strLetter = Int32.Parse(string.Concat(startingCoord[0], startingCoord[1]));
            if (startingCoord.Length > 3)
            {
                //if number is 10
                strNumber = 10;
            }
            else
            {
                //if Number is not 10
                strNumber = Int32.Parse(startingCoord[2].ToString());
            }

            if (orientation == 0)
            {
                //Selects a directon if none was made
                orientation = rnd.Next(1, 5);
            }

            if (orientation == 1)
            {
                //If North is filled, switch to South
                strLetter = strLetter + 1;
                position = ((char)strLetter).ToString() + strNumber.ToString();
                orientation = 3;

                if (strListAlreadyFiredUpon.Contains(position) || strLetter > 74)
                {
                    //If South is filled, randomly choose between East and West
                    strLetter = strLetter - 1;

                    orientation = rnd.Next(1,3);

                    if (orientation == 1)
                    {
                        //East was selected
                        orientation = 2;
                        strNumber = strNumber + 1;
                        position = ((char)strLetter).ToString() + strNumber.ToString();
                        if (strListAlreadyFiredUpon.Contains(position) || strNumber > 10)
                        {
                            //If East is filled, switch to West
                            strNumber = strNumber - 2;
                            orientation = 4;
                            position = ((char)strLetter).ToString() + strNumber.ToString();
                            strListAlreadyFiredUpon.Add(position);
                        }
                        else
                        {
                            //Add East position to list
                            strListAlreadyFiredUpon.Add(position);
                        }
                    }
                    else if (orientation == 2)
                    {
                        //West was selected
                        orientation = 4;
                        strNumber = strNumber - 1;
                        position = ((char)strLetter).ToString() + strNumber.ToString();
                        if (strListAlreadyFiredUpon.Contains(position) || strNumber < 1)
                        {
                            //If West is filled, switch to East
                            strNumber = strNumber + 2;
                            orientation = 2;
                            position = ((char)strLetter).ToString() + strNumber.ToString();
                            strListAlreadyFiredUpon.Add(position);
                        }
                        else
                        {
                            //Add West position to list
                            strListAlreadyFiredUpon.Add(position);
                        }
                    }
                }
                else
                {
                    //Add South position
                    strListAlreadyFiredUpon.Add(position);
                }
            }
            else if (orientation == 2)
            {
                //If East is filled, switch to West
                strNumber = strNumber - 1;
                position = ((char)strLetter).ToString() + strNumber.ToString();
                orientation = 4;

                if (strListAlreadyFiredUpon.Contains(position) || strNumber < 1)
                {
                    //If West is filled, randomly select between North and South
                    strNumber = strNumber + 1;

                    orientation = rnd.Next(1, 3);

                    if (orientation == 1)
                    {
                        //North is selected
                        strLetter = strLetter - 1;
                        position = ((char)strLetter).ToString() + strNumber.ToString();
                        if (strListAlreadyFiredUpon.Contains(position) || strLetter < 65)
                        {
                            //if North is filled, switch to South
                            strLetter = strLetter + 2;
                            orientation = 3;
                            position = ((char)strLetter).ToString() + strNumber.ToString();
                            strListAlreadyFiredUpon.Add(position);
                        }
                        else
                        {
                            //Add North position to list
                            strListAlreadyFiredUpon.Add(position);
                        }
                    }
                    else if (orientation == 2)
                    {
                        //South is selected
                        orientation = 3;
                        strLetter = strLetter + 1;
                        position = ((char)strLetter).ToString() + strNumber.ToString();
                        if (strListAlreadyFiredUpon.Contains(position) || strLetter > 74)
                        {
                            //If South is filled, switch to North
                            strLetter = strLetter - 2;
                            orientation = 1;
                            position = ((char)strLetter).ToString() + strNumber.ToString();
                            strListAlreadyFiredUpon.Add(position);
                        }
                        else
                        {
                            //Add South position to list
                            strListAlreadyFiredUpon.Add(position);
                        }
                    }
                }
                else
                {
                    //Add West position to list
                    strListAlreadyFiredUpon.Add(position);
                }
            }
            else if (orientation == 3)
            {
                //if South is filled, switch to North
                strLetter = strLetter - 1;
                position = ((char)strLetter).ToString() + strNumber.ToString();
                orientation = 1;

                if (strListAlreadyFiredUpon.Contains(position) || strLetter < 65)
                {
                    //If North is filled, randomly select between East and West
                    strLetter = strLetter + 1;

                    orientation = rnd.Next(1, 3);

                    if (orientation == 1)
                    {
                        //East is selected
                        orientation = 2;
                        strNumber = strNumber + 1;
                        position = ((char)strLetter).ToString() + strNumber.ToString();
                        if (strListAlreadyFiredUpon.Contains(position) || strNumber > 10)
                        {
                            //If east is filled, switch to West
                            strNumber = strNumber - 2;
                            orientation = 4;
                            position = ((char)strLetter).ToString() + strNumber.ToString();
                            strListAlreadyFiredUpon.Add(position);
                        }
                        else
                        {
                            //Add East position to a list
                            strListAlreadyFiredUpon.Add(position);
                        }
                    }
                    else if (orientation == 2)
                    {
                        //West is selectd
                        orientation = 4;
                        strNumber = strNumber + 1;
                        position = ((char)strLetter).ToString() + strNumber.ToString();
                        if (strListAlreadyFiredUpon.Contains(position) || strNumber < 1)
                        {
                            //If West is filled, switch to East
                            strNumber = strNumber - 2;
                            orientation = 4;
                            position = ((char)strLetter).ToString() + strNumber.ToString();
                            strListAlreadyFiredUpon.Add(position);
                        }
                        else
                        {
                            //Add West podition to list
                            strListAlreadyFiredUpon.Add(position);
                        }
                    }
                }
                else
                {
                    //Add north position to list
                    strListAlreadyFiredUpon.Add(position);
                }
            }
            else if (orientation == 4)
            {
                //if West is filled, switch to East
                strNumber = strNumber + 1;
                position = ((char)strLetter).ToString() + strNumber.ToString();
                orientation = 2;

                if (strListAlreadyFiredUpon.Contains(position) || strNumber > 10)
                {
                    //If East is filled, randomly select between North and South
                    strNumber = strNumber - 1;

                    orientation = rnd.Next(1, 3);

                    if (orientation == 1)
                    {
                        //North is selected
                        strLetter = strLetter - 1;
                        position = ((char)strLetter).ToString() + strNumber.ToString();

                        if (strListAlreadyFiredUpon.Contains(position) || strLetter < 65)
                        {
                            //If North is filled, switch to South
                            strLetter = strLetter + 2;
                            orientation = 3;
                            position = ((char)strLetter).ToString() + strNumber.ToString();
                            strListAlreadyFiredUpon.Add(position);
                        }
                        else
                        {
                            //Add North position to a list
                            strListAlreadyFiredUpon.Add(position);
                        }
                    }
                    else if (orientation == 2)
                    {
                        //South is selected
                        orientation = 3;
                        strLetter = strLetter + 1;
                        position = ((char)strLetter).ToString() + strNumber.ToString();
                        if (strListAlreadyFiredUpon.Contains(position) || strLetter > 74)
                        {
                            //If South is filled, switch to North
                            strLetter = strLetter - 2;
                            orientation = 1;
                            position = ((char)strLetter).ToString() + strNumber.ToString();
                            strListAlreadyFiredUpon.Add(position);
                        }
                        else
                        {
                            //Add South position to list
                            strListAlreadyFiredUpon.Add(position);
                        }
                    }
                }
                else
                {
                    //Add East position to list
                    strListAlreadyFiredUpon.Add(position);
                }
            }
            //Return the position
            return position;
        }

        public string RandomFire(bool Spin, string position)
        {
            Random rnd = new Random();

            //This is the loop that will keep running until a valid position has been selected.
            while (Spin == false)
            {
                int intNumber = rnd.Next(1, 11);
                int intLetter = rnd.Next(65, 75);

                position = ((char)intLetter).ToString() + intNumber.ToString();

                //If the position selected has already been fired upon, choose another
                if (strListAlreadyFiredUpon.Contains(position))
                {
                    Spin = false;
                }
                else
                {
                    Spin = true;
                    strListAlreadyFiredUpon.Add(position);
                }
            }

            return position;
        }

        //Clears the list that holds the posisitons that have been taken up on the board
        public void ClearTakenList()
        {
            strListInvalidCoord = new List<string>();
        }
        
        //Returns a List string of coordinates of where the ship should be visually displayed
        public List<string> ReturnShipDisplay(string strIntilCoord, int intShipLength, bool boolOrintation)
        {
            List<string> strShipDisplay = new List<string>();

            strShipDisplay.Add(strIntilCoord);
            char charLetter;
            string strLetter = "";
            int intLetter = 0;
            int intDirection = -1;
            int intNumbers = 0;

            //Vertical
            if (boolOrintation == true)
            {
                for (int c = 2; c <= intShipLength; c++)
                {
                    //Seperating the coordinates into letter and number, but also checking if the number is a 10
                    if (strIntilCoord.Length == 3)
                    {
                        strLetter = string.Concat(strIntilCoord[1], strIntilCoord[2]);
                        intNumbers = Int32.Parse(strLetter);
                    }

                    else
                    {
                        intNumbers = Int32.Parse(strIntilCoord[1].ToString());
                    }

                    strLetter = strIntilCoord[0].ToString();
                    charLetter = strIntilCoord[0];

                    //Source: https://stackoverflow.com/questions/3665757/how-to-convert-char-to-int
                    //Turning the letter we took from the coordinates into an int so we can more easily manipluate it
                    intLetter = strIntilCoord[0];

                    //Flips the direction of which position will be selected
                    intDirection = intDirection * -1;

                    //If the letter would be less then A(65), it adds it to the other end of the ship
                    if (intLetter + intDirection < 65)
                    {
                        intDirection = intDirection * -1;
                        intDirection++;
                        intLetter = intLetter + intDirection;
                    }

                    //If the letter would be greater then J(74), it adds it to the other end of the ship
                    else if (intLetter + intDirection > 74)
                    {
                        intDirection = intDirection * -1;
                        intLetter = intLetter + intDirection;
                    }

                    // Otherwise adds it to the direction it wishes
                    else
                    {
                        intLetter = intLetter + intDirection;
                    }

                    //If the direction negative, it means that the program has at least attempted to add to both sides, meaning it can move to the
                    //next row
                    if (intDirection < 0)
                    {
                        intDirection--;
                    }

                    //Turning the number "letter" back into a string letter
                    strLetter = ((char)intLetter).ToString();

                    //Combining the letter and numbers to get a new set coordinates
                    strShipDisplay.Add(strLetter + intNumbers.ToString());
                }
            }

            //Horizontal
            else
            {
                for (int c = 2; c <= intShipLength; c++)
                {
                    //Seperating the coordinates into letter and number, but also checking if the number is a 10
                    if (strIntilCoord.Length == 3)
                    {
                        strLetter = string.Concat(strIntilCoord[1], strIntilCoord[2]);
                        intNumbers = Int32.Parse(strLetter);
                    }

                    else
                    {
                        intNumbers = Int32.Parse(strIntilCoord[1].ToString());
                    }

                    strLetter = strIntilCoord[0].ToString();

                    //Flips the direction of which position will be selected
                    intDirection = intDirection * -1;

                    //If the letter would be less then "1", it adds it to the other end of the ship
                    if (intNumbers + intDirection < 1)
                    {
                        intDirection = intDirection * -1;
                        intDirection++;
                        intNumbers = intNumbers + intDirection;
                    }

                    //If the letter would be greater then "10", it adds it to the other end of the ship
                    else if (intNumbers + intDirection > 10)
                    {
                        intDirection = intDirection * -1;
                        intNumbers = intNumbers + intDirection;
                    }

                    // Otherwise adds it to the direction it wishes
                    else
                    {
                        intNumbers = intNumbers + intDirection;
                    }

                    //If the direction negative, it means that the program has at least attempted to add to both sides, meaning it can move to the
                    //next column
                    if (intDirection < 0)
                    {
                        intDirection = intDirection * -1;
                        intDirection++;
                        intDirection = intDirection * -1;
                    }

                    //Combining the letter and numbers to get a new set coordinates
                    strShipDisplay.Add(strLetter + intNumbers.ToString());
                }
            }

            //strShipDisplay.Add("Fill Later");

            return (strShipDisplay);
        }

        //Needs to read from the list of ships each of their ID's, which positions
        //they take up (they're in the form of a list of strings) and then using the 
        //positions change the VALUE, using the positions as the KEY in the dictionary 
        //of the dictionary.
        public void PlaceShipsOnBoard(List<Ship> listShips4Board, int intNumOfThePlayer)
        {       
            List<string> listStrCoordHold = new List<string>();

            //Counts through ships to be placed
            for (int a = 0; a < listShips4Board.Count; a++)
            {
                //Holds the coordinates of the currently selected ship
                listStrCoordHold = listShips4Board[a].strShipPos;

                //Counts through the number of coordinates set from above
                for (int b = 0; b < listStrCoordHold.Count; b++)
                {
                    //Based on which player is selected adds the ships ID into the gameboard
                    if (intNumOfThePlayer == 1)
                    {
                        gameBoard1[listStrCoordHold[b]] = listShips4Board[a].intShipId;
                    }

                    else
                    {
                        gameBoard2[listStrCoordHold[b]] = listShips4Board[a].intShipId;
                    } 
                }
            }
        }

        //Clears all lists in game
        public void ClearLists()
        {
            gameBoard1 = new Dictionary<string, int>();
            gameBoard2 = new Dictionary<string, int>();
            strListHitNMissCoords1 = new List<string>();
            strListHitNMissCoords2 = new List<string>();
            strListInvalidCoord = new List<string>();
            strListAlreadyFiredUpon = new List<string>();
        }

        //Returns the Hit and Miss lists based on when player number is inputed 
        public List<string> ReturnHitNMissList(int intNumOfPlayer)
        {
            if(intNumOfPlayer == 1)
            {
                return strListHitNMissCoords2;
            }

            else
            {
                return strListHitNMissCoords1;
            }
        }

        //Source: https://social.msdn.microsoft.com/Forums/vstudio/en-US/78e75d1a-0795-4bdb-8a62-ae6faa909986/convert-number-to-alphabet?forum=csharpgeneral
        //Converts a number to it's letter counterpart
        private String Number2String(int number, bool isCaps)
        {
            Char c = (Char)((isCaps ? 65 : 97) + (number - 1));
            return c.ToString();
        }
    }
}
