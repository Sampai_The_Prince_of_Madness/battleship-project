﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships
{
    public class Player
    {
        // Player's name.
        public string strName { get; set; }

        // Locations of the players' ships.
        //public List<Ship> ShipSet { get; set; }

        // bool to see revealed or unrevieled.
        public List<string> RevealedCells { get; set; }

        // Hits count.
        public int Hits { get; set; }

        // Misses count.
        public int Misses { get; set; }

        // Hit ratio.
        public double HitRatio { get; set; }

        // Ships cells count.
        public int ShipCells { get; set; }

        // Ships left count.
        public int ShipsLeft { get; set; }

        // Is this player an AI.
        public bool isAI { get; set; }

        public int ID { get; set; }

        public Player(string pName, bool pIsAI)
        {
            Hits = 0;
            Misses = 0;
            HitRatio = 0;
            ShipCells = 17;
            ShipsLeft = 5;
            isAI = pIsAI;
            strName = pName;
            //ShipSet = new List<Ship>();
        }

        public void SetID(int intPID)
        {
            ID = intPID;
        }
    }
}