﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using BattleshipAPI.Controllers;

namespace Battleships
{
    public class GameHandler
    {             
        static Game game = new Game();
        GameController apiGameController = new GameController();

        // This checks the board for whether a ship can be placed
        public List<string> CanThereBeShip(string strIntilCoord, int intShipLength, bool boolOrintation)
        {
            return game.ReturnShipDisplay(strIntilCoord, intShipLength, boolOrintation);
        }

        // This is the random function to randomize the ship placements
        public List<string> Random(int intShipSize)
        {
            return game.RandomPlacementGenerator(intShipSize);
        }

        // This is the random function to randomize the AI's shots
        public string RandomAI(Boolean notFirstHit, Boolean hitShip, Boolean shipStillAlive, string sHold, string shipName, string startCoord, string pHold, int orientation, string aiSwitch, string difficulty)
        {
            return game.RandomAiFire(notFirstHit, hitShip, shipStillAlive, sHold, shipName, startCoord, pHold, orientation, aiSwitch, difficulty);
        }

        // This allows the program to create the gameboard 
        public void PopulateGB(int intNumOfThePlayer)
        {
            game.PopulateGameBoard(intNumOfThePlayer);
        }

        // This clears the spot taken list
        public void ClearTakenList()
        {
            game.ClearTakenList();
        }

        // This allows the program to fire 
        public int Hit(string position, int intNumOfThePlayer)
        {
            return game.registerHit(position, intNumOfThePlayer);
        }

        // This allows the program to place ships
        public void PlaceShip(List<Ship> ListShips4Board, int intNumOfThePlayer)
        {
            game.PlaceShipsOnBoard(ListShips4Board, intNumOfThePlayer);
        }

        // Tells the game to clear its lists
        public void ClearLists()
        {
            game.ClearLists();
        }

        // Asks the game to return its hit and miss list
        public List<string> ReturnHitNMiss(int intNumOfPlayer)
        {
            return game.ReturnHitNMissList(intNumOfPlayer);
        }

        //testing purposes
        public int returnGameboard(string position)
        {
            return game.gameBoard1[position];
        }

        public void CloseConnection()
        {
            apiGameController.CloseConnection();
        }

        //------------//
        //Event Quiers//
        //------------//

        //This is a test for the connection to the server and grabbing
        public List<string> TestListReturn()
        {
            List<string> listTest = new List<string>();

            listTest = apiGameController.GetPlayerNameTest();

            return listTest;
        }

        //Trying to check to see if the turn packages have been uploaded
        public string GetCoordinates(int intWhichPlayer)
        {
            string strCoords = "";

            strCoords = apiGameController.GetCoordinates(intWhichPlayer);

            return strCoords;
        }

        public List<Ship> GetPlayerShips(int intPlayer)
        {
            List<string> strListTemp = new List<string>();
            List<Ship> shipList = new List<Ship>();
            Ship shipTemp = new Ship(0, strListTemp, 0, "");
            string strShips = "";
            string strNameTemp = "";
            string strCoordTemp = "";
            string strIDTemp = "";
            string strSizeTemp = "";
            int intIDTemp = 0;
            int intSizeTemp = 0;
            bool boolIsID = false;
            bool boolIsCoord = false;
            bool boolIsSize = false;
            bool boolIsName = false;
            bool boolIsNextCood = false;

            strShips = apiGameController.GetCoordinates(intPlayer);

            for (int a = 0; a < strShips.Length; a++)
            {
                if(strShips == "(")
                {
                    strListTemp = new List<string>();
                    shipTemp = new Ship(0, strListTemp, 0, "");

                    boolIsID = true;
                    boolIsCoord = false;
                    boolIsNextCood = false;
                    boolIsSize = false;
                    boolIsName = false;
                }

                else if (strShips == ")")
                {
                    boolIsID = false;
                    boolIsCoord = false;
                    boolIsNextCood = false;
                    boolIsSize = false;
                    boolIsName = false;

                    intIDTemp = Int32.Parse(strIDTemp);
                    intSizeTemp = Int32.Parse(strSizeTemp);

                    shipTemp = new Ship(intIDTemp, strListTemp, intSizeTemp, strNameTemp);
                    shipList.Add(shipTemp);
                }

                else if (strShips == "!")
                {
                    boolIsID = false;
                    boolIsCoord = true;
                    boolIsNextCood = false;
                    boolIsSize = false;
                    boolIsName = false;
                }

                else if (strShips == ",")
                {
                    boolIsNextCood = true;
                }

                else if (strShips == "$")
                {
                    boolIsID = false;
                    boolIsCoord = false;
                    boolIsNextCood = false;
                    boolIsSize = true;
                    boolIsName = false;
                }

                else if (strShips == "#")
                {
                    boolIsID = false;
                    boolIsCoord = false;
                    boolIsNextCood = false;
                    boolIsSize = false;
                    boolIsName = true;
                }

                else if (boolIsID == true)
                {
                    strIDTemp = strIDTemp + strShips[a];
                }

                else if (boolIsCoord == true)
                {
                    if (boolIsNextCood == true)
                    {
                        boolIsNextCood = false;
                        strListTemp.Add(strCoordTemp);
                        strCoordTemp = "";
                    }

                    else
                    {
                        strCoordTemp = strCoordTemp + strShips[a];
                    }
                }

                else if (boolIsSize == true)
                {
                    strSizeTemp = strSizeTemp + strShips[a];
                }

                else if (boolIsName == true)
                {
                    strNameTemp = strNameTemp + strShips[a];
                }
            }

            return shipList;
        }

        //Trying to check to see if the game has been won
        public bool GetGameStatusByPlayer(int intPlayerID)
        {
            bool boolGameStatis = false;

            boolGameStatis = apiGameController.GetGameStatisByPlayerID(intPlayerID);

            return boolGameStatis;
        }

        public void PostPlayerIDIntoGame(int intPlayerID)
        {
            apiGameController.PostPlayerIDIntoGame(intPlayerID);
            apiGameController.PutInGameStatus(intPlayerID, false);
        }

        //Trying to insert turn packages
        public void PutCoordinates(string strPlayer, string strCoordinates)
        {
            apiGameController.PutCoordinates(strPlayer, strCoordinates);
        }

        //Trying to delete turn packages
        public void DeleteCoordinates(int intPlayerID)
        {
            apiGameController.DeleteCoordinates(intPlayerID);
        }

        public void DeleteEventGameByPlayer(int intPlayerID)
        {
            apiGameController.DeleteEventGameByPlayer(intPlayerID);
        }

        public void RemoveChallenge(int intPlayerID)
        {
            apiGameController.RemoveChallenge(intPlayerID);
        }

        //Trying to increment Round/Turn
        public void PutTurnIncrement()
        {
            apiGameController.PutTurnIncrement();
        }

        //Trying to update GameWon
        public void PutGameStatus(bool boolStatis, string strPlayer)
        {
            apiGameController.PutGameStatis(boolStatis, strPlayer);
        }

        //-------------//
        //Player Quiers//
        //-------------//

        //Trying to grab player name by ID
        public string GetPlayerByID(int intIDFinder)
        {
            string strName = "";

            strName = apiGameController.GetPlayerByID(intIDFinder);

            return strName;
        }

        //Trying to grab different ship IDs for a player
        public List<int> GetPlayerShipIDs(int intIDFinder)
        {
            List<int> intListShipIDs = new List<int>();

            intListShipIDs = apiGameController.GetPlayerShipIDs(intIDFinder);

            return intListShipIDs;
        }

        public int GetPlayerIDByChallenge(int intPlayerNum)
        {
            int intPlayerID = 0;

            intPlayerID = apiGameController.GetPlayerIDByChallenge(intPlayerNum);

            return intPlayerID;
        }

        //Trying to check if a player is online
        public bool GetPlayerStatus(int intIDFinder)
        {
            bool boolStatus = false;

            boolStatus = apiGameController.GetPlayerStatusByID(intIDFinder);

            return boolStatus;
        }

        //Trying to get player id and name by being online
        public Dictionary<int, string> GetPlayerByOnline()
        {
            Dictionary<int, string> dicPlayerList = new Dictionary<int, string>();

            List<int> intList = apiGameController.GetPlayerIDByOnline();

            for (int c = 0; c < intList.Count; c++)
            {
                dicPlayerList.Add(intList[c], apiGameController.GetPlayerByID(intList[c]));
            }
   
            return dicPlayerList;
        }

        //Getting player id by date
        public int GetPlayerIDByDate(string strName)
        {
            int intID = 0;

            intID = apiGameController.GetPlayerIDByDate(strName);

            return intID;
        }

        //Getting player id by being offline
        public List<int> GetPlayerIDByOffline()
        {
            List<int> intList = new List<int>();

            intList = apiGameController.GetPlayerIDByOffline();

            return intList;
        }

        public bool GetInGame(int intPlayerID)
        {
            bool boolInGame = false;

            boolInGame = apiGameController.GetInGameStatus(intPlayerID);

            return boolInGame;
        }

        //Trying to insert a new player
        public void PostPlayer(string strPlayerName)
        {
            apiGameController.PostPlayer(strPlayerName);
        }

        //Trying to update player status
        public void PutPlayerStatis(int intPlayerID, bool boolStatus)
        {
            apiGameController.PutPlayerStatusOnline(intPlayerID, boolStatus);
        }

        public void PutInGameStatus(int intPlayerID, bool boolStatus)
        {
            apiGameController.PutInGameStatus(intPlayerID, boolStatus);
        }

        public void PutPlayerIDInChallenge(int intPlayerTargetID, int intPlayerSenderID)
        {
            apiGameController.PutPlayerIDInChallenge(intPlayerTargetID, intPlayerSenderID);
        }

        public void PostIDintoEventPlayer(int intPlayerID)
        {
            apiGameController.PostPlayerIDinPlayer(intPlayerID);
        }

        //Trying to delete a player
        public void DeletePlayer(int intPlayerID)
        {
            apiGameController.DeletePlayer(intPlayerID);
        }
    }
}

